//
//  Player.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation

struct Player {
    
    public private(set) var uid: String
    
    public private(set) var name: String
    
    public private(set) var values: [DiceType: Int]
    
    public private(set) var topSummary: Int? = nil
    
    public private(set) var topBonus: Int? = nil
    
    public private(set) var bottomSummary: Int? = nil
    
    public private(set) var summary: Int? = nil
    
    public private(set) var rounds: Int = 0
    
    init(uid: String, name: String, values: [DiceType: Int]) {
        self.uid = uid
        self.name = name
        self.values = values
        
        for type in DiceType.top {
            if let v = values[type] {
                topSummary = topSummary == nil ? v : topSummary! + v
                rounds = rounds + 1
            }
        }
        if topSummary != nil && topSummary! >= 63 {
            topBonus = 35
        }
        
        for type in DiceType.bottom {
            if var v = values[type] {
                if type == .jokers {
                    v = v * 100
                } else {
                    rounds = rounds + 1
                }
                bottomSummary = bottomSummary == nil ? v : bottomSummary! + v
            }
        }
        
        if topSummary != nil || bottomSummary != nil {
            let ts = topSummary ?? 0
            let tb = topBonus ?? 0
            let bs = bottomSummary ?? 0
            summary = ts + tb + bs
        }
    }
}
