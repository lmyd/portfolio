//
//  Entry.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation

struct Entry {
    
    public private(set) var definition: DiceDefinition
    public private(set) var value: Int?

    init(definition: DiceDefinition, value: Int? = nil) {
        self.definition = definition
        self.value = value
    }
    
}


