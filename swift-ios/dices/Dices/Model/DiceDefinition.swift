//
//  Structs.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation


struct DiceDefinition {

    public private(set) var type: DiceType
    public private(set) var title: String
    public private(set) var description: String
    public private(set) var info: String
    
    init(type: DiceType, title: String, description: String, info: String) {
        self.type = type
        self.title = title
        self.description = description
        self.info = info
    }
}


