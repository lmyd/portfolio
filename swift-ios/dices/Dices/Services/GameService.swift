//
//  GameService.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation
import Firebase

class GameService: DatabaseService {
    
    static let instance = GameService()
    
    static let definition: [[DiceDefinition]] = [
        [
            DiceDefinition(type: .ones, title: "Ones", description: "the sum of ones", info: ""),
            DiceDefinition(type: .doubles, title: "Doubles", description: "the sum of doubles", info: ""),
            DiceDefinition(type: .threes, title: "Threes", description: "the sum of threes", info: ""),
            DiceDefinition(type: .fours, title: "Fours", description: "the sum of fours", info: ""),
            DiceDefinition(type: .fives, title: "Fives", description: "the sum of fives", info: ""),
            DiceDefinition(type: .sixes, title: "Sixes", description: "the sum of sixes", info: "")
        ],[
            DiceDefinition(type: .three, title: "Three", description: "three that same", info: ""),
            DiceDefinition(type: .carriage, title: "Carriage", description: "four that same", info: ""),
            DiceDefinition(type: .full, title: "Full", description: "two plus three that same", info: ""),
            DiceDefinition(type: .straightSmall, title: "Small straight", description: "four in order", info: ""),
            DiceDefinition(type: .straight, title: "Straight", description: "five in order", info: ""),
            DiceDefinition(type: .poker, title: "Poker", description: "five that same", info: ""),
            DiceDefinition(type: .chance, title: "Chance", description: "sum of fives", info: ""),
            DiceDefinition(type: .jokers, title: "Jokers", description: "count of pokers", info: "")
        ],
    ]
    
    static let rounds: Int = 13

    public private(set) var gameId: String? = "wIMAY4w67jWhmtfmgQ6c"
    
    public private(set) var players = [Player]()
    
    public private(set) var currentPlayer: Player?
    
    private var playerListner: ListenerRegistration!

    func initNewGame(numberOfPlayers count: Int, completition: CompletitionHandler) {
        finishGame()
        let game = db.collection(REF_GAMES).addDocument(data: [
            F_USER_ID: AuthService.instance.uid() as Any,
            F_PLAYER_COUNT: count,
            F_CREATED_AT: FieldValue.serverTimestamp()
        ])
        gameId = game.documentID
        for i in  1...count {
            game.collection(REF_PLAYERS).addDocument(data: [
                F_NAME: "Player \(i)",
                F_CREATED_AT: FieldValue.serverTimestamp()
            ])
        }
        completition(true)
    }
    
    func finishGame() {
        gameId = nil
        players.removeAll()
        currentPlayer = nil
    }
    
    func startGame(completition: @escaping CompletitionHandler){
        playerListner = db.collection(REF_GAMES)
            .document(GameService.instance.gameId!)
            .collection(REF_PLAYERS)
            .order(by: F_CREATED_AT, descending: false)
            .addSnapshotListener { (snapshot, error) in
                guard let snapshot = snapshot else {
                    debugPrint("Error fetching players: \(String(describing: error))")
                    return
                }
                self.players.removeAll()
                for document in snapshot.documents {
                    let data = document.data()
                    var values = [DiceType: Int]()
                    
                    for section in GameService.definition {
                        for definition in section {
                            values[definition.type] = data[definition.type.rawValue] as? Int
                        }
                    }
                    
                    self.players.append(
                        Player(
                            uid: document.documentID,
                            name: data[F_NAME] as? String ?? "Anonymous",
                            values: values
                        )
                    )
                }
                
                completition(true)
        }
    }
    
    func stopGame(){
        playerListner.remove()
    }
    
    func setCurrentPlayer(index: Int) -> Bool {
        if index >= 0 && index < players.count {
            currentPlayer = players[index]
            return true
        }
        return false
    }
    
    func updateCurrentPlayer(forName name: String) {
        guard let player = currentPlayer else { return }
        db.collection(REF_GAMES)
            .document(GameService.instance.gameId!)
            .collection(REF_PLAYERS)
            .document(player.uid)
            .updateData([F_NAME: name])
    }

    func updateCurrentPlayer(forEntry type: DiceType, withValue value: Int, andJoker joker: Bool, onSegments segments: [DiceBones: Int], completition: @escaping CompletitionHandler) {
        guard let player = currentPlayer else { return }
        let playerRef = db.collection(REF_GAMES).document(GameService.instance.gameId!).collection(REF_PLAYERS).document(player.uid)
        
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            let playerDoc: DocumentSnapshot
            do {
                try playerDoc = transaction.getDocument(playerRef)
            } catch let fetchError as NSError {
                debugPrint("fetch error: \(fetchError.localizedDescription)")
                return nil
            }
            
            var dataToSave: [String: Any] = [:]
            dataToSave[type.rawValue] = value
            
            if joker {
                if playerDoc.data()![DiceType.poker.rawValue] != nil {
                    let savedJokers = playerDoc.data()![DiceType.jokers.rawValue] as? Int ?? 0
                    dataToSave[DiceType.jokers.rawValue] = savedJokers + 1
                }
            }
            
            transaction.updateData(dataToSave, forDocument: playerRef)
            
            var log: [String: Any] = [
                F_NAME: type.rawValue,
                F_CREATED_AT: FieldValue.serverTimestamp()
            ]
            for segment in segments {
                log[segment.key.rawValue] = segment.value
            }
            
            let logRef = playerRef.collection(REF_LOG).document()
            
            transaction.setData(log, forDocument: logRef)
            
            return nil
        }) { (object, error) in
            if let err = error {
                debugPrint("transaction failed: \(err.localizedDescription)")
            } else {
                completition(true)
            }
        }        
    }
}
