//
//  ScoreService.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 20/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation

class ScoreService {
    
    static let instance = ScoreService()
    
    func score(forType type: DiceType, andBones bones: [DiceBones: Int]) -> Int {
        
        let numbers = Array(bones.values)
        
        if numbers.count != 5 || numbers.contains(0) {
            return 0
        }

        switch type {
        case .ones:
            return topSectionScore(for: 1, on: bones)
        case .doubles:
            return topSectionScore(for: 2, on: bones)
        case .threes:
            return topSectionScore(for: 3, on: bones)
        case .fours:
            return topSectionScore(for: 4, on: bones)
        case .fives:
            return topSectionScore(for: 5, on: bones)
        case .sixes:
            return topSectionScore(for: 6, on: bones)
        case .three:
            return isThatSame(for: 3, on: bones) ? numbers.reduce(0, +) : 0
        case .carriage:
            return isThatSame(for: 4, on: bones) ? numbers.reduce(0, +) : 0
        case .full:
            return isFull(on: bones) ? 25 : 0
        case .straightSmall:
            return isSmallStraight(on: bones) ? 30 : 0
        case .straight:
            return isStraight(on: bones) ? 40 : 0
        case .poker:
            return isThatSame(for: 5, on: bones) ?  50 : 0
        case .chance:
            return numbers.reduce(0, +)
        default:
            return 0
        }
    }
    
    func topSectionScore(for number: Int, on bones: [DiceBones: Int] ) -> Int {
        let numbers = Array(bones.values)
        var score: Int = 0
        for i in numbers {
            if i == number {
                score = score + i
            }
        }
        return score
    }
    
    func isThatSame(for count: Int, on bones: [DiceBones: Int]) -> Bool {
        let numbers = Array(bones.values)
        for i in 1...6 {
            let c = numbers.filter{$0 == i}.count
            if c >= count {
                return true
            }
        }
        return false
    }
    
    func isFull(on bones: [DiceBones: Int]) -> Bool {
        let numbers = Array(bones.values)
        var smallerPart = false
        var biggerPart = false
        for i in 1...6 {
            let count = numbers.filter{$0 == i}.count
            if count == 2 {
                smallerPart = true
            }
            if count == 3 {
                biggerPart = true
            }
            if count == 5 {
                smallerPart = true
                biggerPart = true
            }
        }
        return smallerPart && biggerPart
    }
    
    func isSmallStraight(on bones: [DiceBones: Int]) -> Bool {
        let numbers = Array(bones.values)
        if (numbers.contains(1) && numbers.contains(2) && numbers.contains(3) && numbers.contains(4))
            || (numbers.contains(2) && numbers.contains(3) && numbers.contains(4) && numbers.contains(5))
            || (numbers.contains(3) && numbers.contains(4) && numbers.contains(5) && numbers.contains(6)
            || isThatSame(for: 5, on: bones)) {
            return true
        }
        return false
    }

    func isStraight(on bones: [DiceBones: Int]) -> Bool {
        let numbers = Array(bones.values)
        if (numbers.contains(1) && numbers.contains(2) && numbers.contains(3) && numbers.contains(4) && numbers.contains(5))
            || (numbers.contains(2) && numbers.contains(3) && numbers.contains(4) && numbers.contains(5) && numbers.contains(6)
            || isThatSame(for: 5, on: bones)) {
            return true
        }
        return false
    }
}
