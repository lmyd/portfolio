//
//  DatabaseService.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation
import Firebase

class DatabaseService {
    
    let db = Firestore.firestore()
    
    init() {
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    
}
//        // old:
//        let date: Date = documentSnapshot.get("created_at") as! Date
//        // new:
//        let timestamp: Timestamp = documentSnapshot.get("created_at") as! Timestamp
//        let date: Date = timestamp.dateValue()

