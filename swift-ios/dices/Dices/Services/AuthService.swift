//
//  AuthService.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 18/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit
import Firebase

class AuthService {
    
    static let instance = AuthService()

    let auth = Auth.auth()

//    func addStateDidChangeListener(view: UIViewController) {
//        auth.addStateDidChangeListener({ (auth, user) in
//            if user == nil {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let authVC = storyboard.instantiateViewController(withIdentifier: VC_ID_AUTH)
//                view.present(authVC, animated: true, completion: nil)
//            }
//        })
//    }

    func login(email: String, password: String, completition: @escaping CompletitionHandler){
        auth.signIn(withEmail: email, password: password) { (user, error) in
            if let err = error {
                debugPrint(err.localizedDescription)
                completition(false)
            } else {
                completition(true)
            }
        }
    }
    
    func logout(completition: @escaping CompletitionHandler) {
        do {
            try auth.signOut()
        } catch let signoutError as NSError {
            completition(false)
            debugPrint("Error signing out: \(signoutError)")
        }
        completition(true)
    }

    func signin(email: String, password: String, completition: @escaping CompletitionHandler){
        self.auth.createUser(withEmail: email, password: password) { (user, error) in
            if let err = error {
                debugPrint(err.localizedDescription)
                completition(false)
            } else {
                self.login(email: email, password: password, completition: { (success) in
                    if success {
                        completition(true)
                    } else {
                        completition(false)
                    }
                })
            }
        }
    }
    
    func uid() -> String? {
        return auth.currentUser?.uid ?? nil
    }
    
}

