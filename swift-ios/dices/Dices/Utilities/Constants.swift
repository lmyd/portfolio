//
//  Constants.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 17/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

let BTN_CORNER_RADIUS: CGFloat = 10

// notifications
let NOTIF_USER_DATA_DID_CHANGE = Notification.Name("notifUserDataChanged")

// SEGUES
let SEGUE_HOME = "loginSegue"
let SEGUE_USER_LOGIN = "loginUserSegue"
let SEGUE_USER_CREATE = "createUserSegue"
let SEGUE_GAME = "gameSegue"
let SEGUE_GAME_PLAYER = "gamePlayerSegue"

//--old--

let SEGUE_GAME_ENTRY = "gameEntrySegue"
let SEGUE_GAME_SUMMARY = "gameSummarySegue"

// view controllers id
let VC_CONFIRMATION = "confirmationVC"
let VC_START_GAME = "startGameVC"
let CELL_GAME_PLAYER = "gamePlayerCell"


//--old--
let VC_ID_GAME_PLAYER = "gamePlayerVC"
let CELL_ID_HEADER = "headerCell"
let CELL_ID_ENTRY = "entryCell"
let CELL_ID_FOOTER = "footerCell"
let CELL_ID_SUMMARY = "summaryCell"

//collections
let REF_GAMES = "games"
let REF_PLAYERS = "players"
let REF_LOG = "logs"


//firebase fields
let F_CREATED_AT = "createdAt"
let F_USER_ID = "userId"
let F_NAME = "name"
let F_PLAYER_COUNT = "playerCount"

