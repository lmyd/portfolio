//
//  Functions.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 17/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit


typealias CompletitionHandler = (_ Success: Bool) -> ()

func appAlert(forViewController view: UIViewController, forTitle title: String? = nil, forMessage msg: String? = nil, forDismissTitle close: String? = nil) {
    let alertController = UIAlertController(title: title ?? "Error", message: msg, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: close ?? "Dismiss", style: .default, handler: nil))
    view.present(alertController, animated: true, completion: nil)
}
