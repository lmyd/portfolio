//
//  Enums.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 18/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import Foundation

enum DiceSection: String {
    case top = "Top section"
    case bottom = "Bottom section"
    case total = "Total"
    
    static let all = [top, bottom]
}

enum DiceType: String {
    case ones = "entry_ones"
    case doubles = "entry_doubles"
    case threes = "entry_threes"
    case fours = "entry_fours"
    case fives = "entry_fives"
    case sixes = "entry_sixes"
    case three = "entry_three"
    case carriage = "entry_carriage"
    case full = "entry_full"
    case straightSmall = "entry_small_straight"
    case straight = "entry_straight"
    case poker = "entry_poker"
    case chance = "entry_chance"
    case jokers = "entry_jokers"
    
    static let all = [ones, doubles, threes, fours, fives, sixes, three, carriage, full, straightSmall, straight, poker, chance, jokers]
    static let top = [ones, doubles, threes, fours, fives, sixes]
    static let bottom = [three, carriage, full, straightSmall, straight, poker, chance, jokers]
}

enum DiceBones: String {
    case first = "first"
    case second = "second"
    case third = "third"
    case fourth = "fourth"
    case fifth = "fifth"
}
