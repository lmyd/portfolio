//
//  HomeVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 07/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var startGameBtn: AppButton!
    @IBOutlet weak var continueGameBtn: AppButton!
    @IBOutlet weak var historyBtn: AppButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataDidChange()
        NotificationCenter.default.addObserver(self, selector: #selector(userDataDidChange), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if GameService.instance.gameId != nil {
            continueGameBtn.isHidden = false
        } else {
            continueGameBtn.isHidden = true
        }
    }
    
    @objc func userDataDidChange(){
        if(AuthService.instance.auth.currentUser != nil){ //isLogged
            navigationItem.leftBarButtonItem = getBarBtn(forImage: "user", andAction: #selector(user))
            navigationItem.rightBarButtonItem = getBarBtn(forImage: "logout", andAction: #selector(logout))
            historyBtn.isHidden = false
        }else{
            navigationItem.leftBarButtonItem = nil
            navigationItem.rightBarButtonItem = getBarBtn(forImage: "login", andAction: #selector(login))
            historyBtn.isHidden = true
        }
    }
    
    @objc func login() {
        performSegue(withIdentifier: SEGUE_USER_LOGIN, sender: self)
    }
    
    @objc func logout() {
        AuthService.instance.logout(completition: { (success) in
            let confirmation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: VC_CONFIRMATION) as! ConfirmationVC
            if success {
                confirmation.setupView(secondLine: "logged out")
            } else {
                confirmation.setupView(firstLine: "Failure", secondLine: "user not logged out", infoColor: #colorLiteral(red: 0.5807225108, green: 0.066734083, blue: 0, alpha: 1))
            }
            self.present(confirmation, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    confirmation.dismiss(animated: true, completion: nil)
                    if success {
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                    }
                })
            }
        })
    }

    @objc func user() {
        print("USER")
    }
    
    @IBAction func startGameBtnPressed(_ sender: Any) {
        let start = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: VC_START_GAME) as! StartGameVC
        start.modalPresentationStyle = .overCurrentContext
        start.modalTransitionStyle = .crossDissolve
        start.sender = self
        present(start, animated: true, completion: nil)
    }
    
    @IBAction func continueGameBtnPressed(_ sender: Any) {
        if GameService.instance.gameId != nil {
            performSegue(withIdentifier: SEGUE_GAME, sender: self)
        }
    }
    
    @IBAction func historyBtnPressed(_ sender: Any) {
    }
}
