//
//  confirmationVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 12/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class ConfirmationVC: UIViewController {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var firstLineTxt: UILabel!
    @IBOutlet weak var secondLineTxt: UILabel!
    
    private var firstLine: String = "Successfully"
    private var secondLine: String = ""
    private var infoColor: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.addLayerShadow()
        firstLineTxt.text = firstLine
        secondLineTxt.text = secondLine
        
        if let color = infoColor {
            img.image = img.image!.withRenderingMode(.alwaysTemplate)
            img.tintColor = color
            firstLineTxt.textColor = color
            secondLineTxt.textColor = color
        }
    }
    
    func setupView(firstLine: String? = nil, secondLine: String? = nil, infoColor: UIColor? = nil){
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
        if let line = firstLine {
            self.firstLine = line
        }
        if let line = secondLine {
            self.secondLine = line
        }
        self.infoColor = infoColor
    }
    
}
