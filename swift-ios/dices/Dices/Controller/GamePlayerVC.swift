//
//  GamePlayerVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 17/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class GamePlayerVC: UIViewController {


    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = getBarBackBtn()
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = GameService.instance.currentPlayer!.name
    }
    
    
    /*
    func numberOfSections(in tableView: UITableView) -> Int {
        return GameService.definition.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        let player = GameService.instance.currentPlayer!
        let footerCell = tableView.dequeueReusableCell(withIdentifier: CELL_ID_FOOTER) as! FooterCell
        if section == 0 {
            footerCell.configureCell(forTop: true, scores: player.topSummary, bonus: player.topBonus)
        } else {
            footerCell.configureCell(forTop: false, scores: player.bottomSummary, bonus: player.summary)
        }
        
        footerView.addSubview(footerCell)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GameService.definition[section].count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID_ENTRY, for: indexPath) as? EntryCell else { return UITableViewCell() }
        
        let definition = GameService.definition[indexPath.section][indexPath.row]
        
        let entry = Entry(definition: definition, value: GameService.instance.currentPlayer!.values[definition.type])
        
        cell.configureCell(entry: entry)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let definition = GameService.definition[indexPath.section][indexPath.row]
        
        let entry = Entry(definition: definition, value: GameService.instance.currentPlayer!.values[definition.type])

        if entry.value == nil && entry.definition.type != .jokers {
            performSegue(withIdentifier: SEGUE_GAME_ENTRY, sender: entry)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_GAME_ENTRY {
            if let destinationVC = segue.destination as? GameEntryVC {
                if let entry = sender as? Entry {
                    destinationVC.entry = entry
                }
            }
        }
    }
    
    @IBAction func editBtnPressed(_ sender: Any) {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Edit player", message: "Change name of player", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
            //getting the input values from user
            let newName = alertController.textFields![0].text ?? ""
            
            if newName != "" && newName.count <= 10 {
                GameService.instance.updateCurrentPlayer(forName: newName)
                self.navigationItem.title = newName
            } else {
                appAlert(forViewController: self, forMessage: "Wrong name of player.\nMax 10 digits.")
            }
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.text = GameService.instance.currentPlayer!.name
            textField.textAlignment = .center
        }
      
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        present(alertController, animated: true, completion: nil)
    }
    */
}

