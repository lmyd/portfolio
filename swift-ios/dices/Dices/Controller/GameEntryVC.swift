//
//  GameEntryVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class GameEntryVC: UIViewController {

    @IBOutlet weak var firstSegment: UISegmentedControl!
    @IBOutlet weak var secondSegment: UISegmentedControl!
    @IBOutlet weak var thirdSegment: UISegmentedControl!
    @IBOutlet weak var fourthSegment: UISegmentedControl!
    @IBOutlet weak var fifthSegment: UISegmentedControl!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    var entry: Entry!
    var segments: [DiceBones: UISegmentedControl]!
    var score:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = entry.definition.title
        scoreLbl.text = "\(score) pts"
        saveBtn.layer.cornerRadius = BTN_CORNER_RADIUS
        segments = [
            .first: firstSegment,
            .second: secondSegment,
            .third: thirdSegment,
            .fourth: fourthSegment,
            .fifth: fifthSegment
        ]
    }

    func values() -> [DiceBones: Int] {
        var response = [DiceBones: Int]()
        for segment in segments {
            response[segment.key] = segment.value.selectedSegmentIndex + 1
        }
        return response
    }
    
    @IBAction func segmentPressed(_ sender: Any) {
        
        let segments = values()
        
        score = ScoreService.instance.score(forType: entry.definition.type, andBones: segments)
        
        scoreLbl.text = "\(score) pts"
    }

    
    @IBAction func savePressed(_ sender: Any) {

        let segments = values()

        if segments.values.contains(0) {
            return
        }

        var canUpdate = true
        var joker = false

        if ScoreService.instance.isThatSame(for: 5, on: segments) {
            let poker = GameService.instance.currentPlayer!.values[.poker]
            let tmp: [DiceType] = [.three, .carriage, .full, .straightSmall, .straight, .chance]
            if tmp.contains(entry.definition.type) {
                let topKey = DiceType.top[firstSegment.selectedSegmentIndex]
                let top = GameService.instance.currentPlayer!.values[topKey]
                if poker == nil || top == nil {
                    canUpdate = false
                    appAlert(forViewController: self, forTitle: "Error", forMessage: "First you must fill top or poker for this bones.")
                }
            }
            if poker != nil {
                joker = true
            }
        }
        if canUpdate {
            GameService.instance.updateCurrentPlayer(forEntry: entry.definition.type, withValue: score, andJoker: joker, onSegments: segments) { (success) in
                if success {
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                }
            }
        }
    }
}
