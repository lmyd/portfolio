//
//  GameVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 26/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class GameVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.delegate = self
        collection.dataSource = self
        navigationItem.leftBarButtonItem = getBarBackBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GameService.instance.startGame { (isFinish) in
            self.collection.reloadData()
            var isFinish = true
            for player in GameService.instance.players {
                if player.rounds < GameService.rounds {
                    isFinish = false
                }
            }
            if isFinish {
                GameService.instance.stopGame()
                self.performSegue(withIdentifier: SEGUE_GAME_SUMMARY, sender: self)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        GameService.instance.stopGame()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GameService.instance.players.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collection.dequeueReusableCell(withReuseIdentifier: CELL_GAME_PLAYER, for: indexPath) as? GamePlayerCell else { return UICollectionViewCell() }
        cell.configureCell(player: GameService.instance.players[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if GameService.instance.setCurrentPlayer(index: indexPath.row) {
            performSegue(withIdentifier: SEGUE_GAME_PLAYER, sender: nil)
        }
    }
    
}
