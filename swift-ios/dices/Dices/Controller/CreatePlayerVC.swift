//
//  CreatePlayerVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 12/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class CreatePlayerVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTxt: AppTextField!
    @IBOutlet weak var passwordTxt: AppTextField!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = getBarBackBtn()
        emailTxt.delegate = self
        emailTxt.tag = 0
        passwordTxt.delegate = self
        passwordTxt.tag = 1
    }
    
    @IBAction func createBtnPressed(_ sender: Any) {
        
        guard let email = emailTxt.text, !email.isEmpty,
            let password = passwordTxt.text, !password.isEmpty else { return }
        
        navigationController!.navigationBar.layer.zPosition = -1;
        spinner.startAnimating()
        blackView.isHidden = false
        
        AuthService.instance.signin(email: email, password: password) { (success) in
            self.blackView.isHidden = true
            self.spinner.stopAnimating()
            self.navigationController!.navigationBar.layer.zPosition = 0;
            let confirmation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: VC_CONFIRMATION) as! ConfirmationVC
            if success {
                confirmation.setupView(secondLine: "created")
            } else {
                confirmation.setupView(firstLine: "Failure", secondLine: "user not created", infoColor: #colorLiteral(red: 0.5807225108, green: 0.066734083, blue: 0, alpha: 1))
            }
            self.present(confirmation, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    confirmation.dismiss(animated: true, completion: nil)
                    if success {
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                        self.navigationController!.popToRootViewController(animated: true)
                    }
                })
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
}
