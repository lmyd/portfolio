//
//  GameSummaryVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 25/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class GameSummaryVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var players = [Player]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Finish", style: .plain, target: self, action: #selector(backBtnTouched))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        players.removeAll()
        players = GameService.instance.players.sorted(by: { (first, second) -> Bool in
            return first.summary! > second.summary!
        })
        tableView.reloadData()
    }
    
    @objc func backBtnTouched(){
        GameService.instance.finishGame()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID_SUMMARY, for: indexPath) as? SummaryCell else { return UITableViewCell() }
        
        cell.configureCell(place: indexPath.row+1, player: players[indexPath.row])
        
        return cell
    }
}
