//
//  StartGameVC.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 16/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class StartGameVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var picker: UIPickerView!
    
    public var sender: UIViewController?
    
    private var pickerData = [
        "one player",
        "two players",
        "three players",
        "four players",
        "five players",
        "six players",
        "seven players",
        "eight players",
        "nine players",
        "ten players"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.addLayerShadow()
        picker.delegate = self
        picker.dataSource = self
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerData[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return attributedString
    }

    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startBtnPressed(_ sender: Any) {
        let players = picker.selectedRow(inComponent: 0)+1
        GameService.instance.initNewGame(numberOfPlayers: players) { (success) in
            if success {
                self.dismiss(animated: true, completion: {
                    self.sender!.performSegue(withIdentifier: SEGUE_GAME, sender: nil)
                })
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
