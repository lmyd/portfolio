//
//  UIViewControllerExt.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 10/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func getBarBtn(forImage name: String, andAction action: Selector) -> UIBarButtonItem {
        let img = UIImage(named: name)!.resizeImage(targetSize: CGSize(width: 30, height: 30))
        let btn = UIBarButtonItem(image: img, style: .plain, target: self, action: action)
        btn.tintColor = UIColor.white
        return btn
    }
    
    func getBarBackBtn() -> UIBarButtonItem {
        let img = UIImage(named: "back")!.resizeImage(targetSize: CGSize(width: 30, height: 30))
        let btn = UIBarButtonItem(image: img, style: .plain, target: navigationController, action: #selector(UINavigationController.popViewController(animated:)))
        btn.tintColor = UIColor.white
        return btn

    }

}
