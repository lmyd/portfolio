//
//  AppButton.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 07/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

@IBDesignable
class AppButton: UIButton {

    //this init fires usually called, when storyboards UI objects created:
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    //This method is called during programmatic initialisation
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    //required method to present changes in IB
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupViews()
    }

    func setupViews() {
        //your common setup goes here
        frame.size.height = CGFloat(50)
        layer.cornerRadius = 20
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 1
        
        titleLabel!.font = UIFont(name: "Avenir Next-medium", size: 18)
        
        setTitleColor(UIColor.white, for: .normal)
        setTitleColor(UIColor.red, for: .highlighted)
    }
    
    override var isHighlighted: Bool {
        didSet {
            layer.shadowOpacity = isHighlighted ? 0 : 0.3
        }
    }
}
