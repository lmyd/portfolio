//
//  AppTextField.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 10/02/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class AppTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    func setupViews() {
        frame.size.height = CGFloat(50)
        layer.cornerRadius = 20
        layer.borderWidth = 0
        backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
        font = UIFont(name: "Avenir Next-medium", size: 18)
        textColor = UIColor.white
        textAlignment = .center
        attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}
