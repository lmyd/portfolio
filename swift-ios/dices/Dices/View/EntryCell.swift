//
//  EntryCell.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 19/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class EntryCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    
    
    func configureCell(entry: Entry) {
        titleLbl.text = entry.definition.title
        subtitleLbl.text = entry.definition.description
        if let val = entry.value {
            if entry.definition.type == .jokers {
                valueLbl.text = "\(val * 100) pts"
            } else {
                valueLbl.text = "\(val) pts"
            }
        } else {
            valueLbl.text = ""
        }
    }

}
