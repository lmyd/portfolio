//
//  GamePlayerCell.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 26/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class GamePlayerCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameTxt: UILabel!
    @IBOutlet weak var roundTxt: UILabel!
    @IBOutlet weak var pointsTxt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 20
        self.addLayerShadow()
    }
    
    func configureCell(player: Player) {
        nameTxt.text = player.name
        roundTxt.text = "round \(player.rounds) of 13"
        pointsTxt.text = "\(player.summary ?? 0) pts"
    }

    override var isHighlighted: Bool {
        didSet {
            layer.shadowOpacity = isHighlighted ? 0 : 0.3
        }
    }
    
}
