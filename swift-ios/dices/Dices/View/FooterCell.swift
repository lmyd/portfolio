//
//  FooterCell.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 21/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class FooterCell: UITableViewCell {

    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var bonusLbl: UILabel!
 
    func configureCell(forTop: Bool,  scores: Int? = nil, bonus: Int? = nil){
        
        if let s = scores {
            scoreLbl.isHidden = false
            scoreLbl.text = "\(s) pts"
        } else {
            scoreLbl.isHidden = true
        }
        
        if let b = bonus {
            bonusLbl.isHidden = false
            if forTop {
                bonusLbl.text = "+\(b) pts"
                bonusLbl.textColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            } else {
                bonusLbl.text = "\(b) pts"
                bonusLbl.textColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
            }
        } else {
            bonusLbl.isHidden = true
        }
        
    }
    
}
