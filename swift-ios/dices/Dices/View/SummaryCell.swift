//
//  SummaryCell.swift
//  Dices
//
//  Created by Lukasz Mydlarski on 26/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    @IBOutlet weak var placeView: UIView!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var placeImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeView.layer.cornerRadius = placeView.bounds.height / 2
    }

    func configureCell(place: Int, player: Player){
        placeImg.isHidden = true
        placeLbl.isHidden = true
        switch place {
        case 1:
            placeImg.image = UIImage(named: "firstPlace")!
            placeImg.isHidden = false
            break
        case 2:
            placeImg.image = UIImage(named: "secondPlace")!
            placeImg.isHidden = false
            break
        case 3:
            placeImg.image = UIImage(named: "thirdPlace")!
            placeImg.isHidden = false
            break
        default:
            placeLbl.text = "\(place)"
            placeLbl.isHidden = false
        }
        nameLbl.text = player.name
        if let sum = player.summary {
            scoreLbl.text = "\(sum) pts"
        } else {
            scoreLbl.text = ""
        }
    }
}
