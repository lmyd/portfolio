//
//  DicesTests.swift
//  DicesTests
//
//  Created by Lukasz Mydlarski on 17/01/2019.
//  Copyright © 2019 Lukasz Mydlarski. All rights reserved.
//

import XCTest
@testable import Dices

class DicesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEntryScore() {
        assert(ScoreService.instance.score(forType: .ones, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 1]) == 3)
        assert(ScoreService.instance.score(forType: .doubles, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 1]) == 2)
        assert(ScoreService.instance.score(forType: .threes, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 1]) == 3)
        assert(ScoreService.instance.score(forType: .fours, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 1]) == 0)
        assert(ScoreService.instance.score(forType: .fives, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 5]) == 5)
        assert(ScoreService.instance.score(forType: .doubles, andBones: [.first: 2, .second: 1, .third: 3, .fourth: 1, .fifth: 1]) == 2)
        
        assert(ScoreService.instance.score(forType: .three, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 5, .fifth: 2]) == 10)
        assert(ScoreService.instance.score(forType: .three, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 1, .fifth: 5]) == 9)
        assert(ScoreService.instance.score(forType: .three, andBones: [.first: 1, .second: 2, .third: 5, .fourth: 5, .fifth: 2]) == 0)

        assert(ScoreService.instance.score(forType: .carriage, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 5, .fifth: 2]) == 0)
        assert(ScoreService.instance.score(forType: .carriage, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 1, .fifth: 5]) == 9)
        assert(ScoreService.instance.score(forType: .carriage, andBones: [.first: 2, .second: 2, .third: 2, .fourth: 2, .fifth: 2]) == 10)

        assert(ScoreService.instance.score(forType: .full, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 5, .fifth: 2]) == 0)
        assert(ScoreService.instance.score(forType: .full, andBones: [.first: 1, .second: 1, .third: 3, .fourth: 2, .fifth: 5]) == 0)
        assert(ScoreService.instance.score(forType: .full, andBones: [.first: 2, .second: 2, .third: 2, .fourth: 4, .fifth: 4]) == 25)
        
        assert(ScoreService.instance.score(forType: .straightSmall, andBones: [.first: 1, .second: 2, .third: 3, .fourth: 4, .fifth: 1]) == 30)
        assert(ScoreService.instance.score(forType: .straightSmall, andBones: [.first: 2, .second: 3, .third: 4, .fourth: 5, .fifth: 1]) == 30)
        assert(ScoreService.instance.score(forType: .straightSmall, andBones: [.first: 3, .second: 4, .third: 5, .fourth: 6, .fifth: 1]) == 30)
        assert(ScoreService.instance.score(forType: .straightSmall, andBones: [.first: 2, .second: 2, .third: 2, .fourth: 4, .fifth: 1]) == 0)

        assert(ScoreService.instance.score(forType: .straight, andBones: [.first: 1, .second: 2, .third: 3, .fourth: 4, .fifth: 5]) == 40)
        assert(ScoreService.instance.score(forType: .straight, andBones: [.first: 2, .second: 3, .third: 4, .fourth: 5, .fifth: 6]) == 40)
        assert(ScoreService.instance.score(forType: .straight, andBones: [.first: 2, .second: 2, .third: 2, .fourth: 4, .fifth: 4]) == 0)

        assert(ScoreService.instance.score(forType: .poker, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 5, .fifth: 2]) == 0)
        assert(ScoreService.instance.score(forType: .poker, andBones: [.first: 1, .second: 1, .third: 1, .fourth: 1, .fifth: 1]) == 50)
        
        assert(ScoreService.instance.score(forType: .chance, andBones: [.first: 1, .second: 2, .third: 1, .fourth: 5, .fifth: 2]) == 11)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
