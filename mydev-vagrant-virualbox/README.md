# MyDev #

My development environment on Vagrant & VirtualBox

## How to install? ##

* Install [VirtualBox](https://www.virtualbox.org)
* Install [Vagrant](https://www.vagrantup.com)
* Install [Git for Windows](https://git-for-windows.github.io)
* Install [PHP](http://php.net)
* Install [Composer](https://getcomposer.org)
* Clone MyDev repo **git clone git@bitbucket.org:lmyd/mydev.git** to c:/mydev
* Run **composer install** in c:/mydev
* Run **vagrant up** in c:/mydev - you'll start virtual machine
* Run **vagrant ssh** in c:/mydev - you'll loged on virtual machine
* Upgrade composer by run **composer self-update --update-keys** and put keys from [Composer Public Keys / Signatures](https://composer.github.io/pubkeys.html)

## Hosts entries: ##

You should add entries to C:\Windows\System32\drivers\etc\hosts

* 192.168.100.100    mydev
* 192.168.100.100    myapi.mydev
* 192.168.100.100    makiety.myapi.mydev
* 192.168.100.100    api.myapi.mydev
* 192.168.100.100    fundacjapolsat.mydev
* 192.168.100.100    rywalizator.mydev

## Configuration steps required to work with git: ##

* git config --global user.name "John Doe"
* git config --global user.email johndoe@example.com
* git config --global push.default current
* git config --global branch.autoSetupRebase always