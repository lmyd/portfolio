#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the MyDev machine is provisioned.

#remeber !!! script is running from root account not vagrant

ssh-keygen -R bitbucket.org -f /home/vagrant/.ssh/known_hosts

ssh-keyscan -H bitbucket.org >> /home/vagrant/.ssh/known_hosts

rm -rfv /root/.ssh

cp -R /home/vagrant/.ssh /root/.ssh

sudo chown -R vagrant:vagrant /home/vagrant/.ssh

sudo chmod 0644 /home/vagrant/.ssh/*

sudo chmod 0600 /home/vagrant/.ssh/id_rsa

if [ ! -d "/home/vagrant/wwwroot/phptools" ]; then
  git clone git@bitbucket.org:lmyd/phptools.git /home/vagrant/wwwroot/phptools
fi

if [ ! -d "/home/vagrant/wwwroot/dbquerybuilder" ]; then
  git clone git@bitbucket.org:lmyd/dbquerybuilder.git /home/vagrant/wwwroot/dbquerybuilder
fi

if [ ! -d "/home/vagrant/wwwroot/apiengine" ]; then
  git clone git@bitbucket.org:lmyd/apiengine.git /home/vagrant/wwwroot/apiengine
fi

if [ ! -d "/home/vagrant/wwwroot/myapi" ]; then
  git clone git@bitbucket.org:lmyd/myapi.git /home/vagrant/wwwroot/myapi
fi

if [ ! -d "/home/vagrant/wwwroot/fundacjapolsat" ]; then
  git clone git@bitbucket.org:lmyd/fundacjapolsat.git /home/vagrant/wwwroot/fundacjapolsat
fi

if [ ! -d "/home/vagrant/wwwroot/rywalizator" ]; then
  git clone git@bitbucket.org:lmyd/rywalizator.git /home/vagrant/wwwroot/rywalizator
fi


