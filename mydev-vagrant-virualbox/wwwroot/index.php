<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MyDev by Lukasz Mydlarski</title>
</head>
<body>

<h1>MyDev on 192.168.100.100</h1>
<p>My development environment on Vagrant & VirtualBox</p>

<h2>List of applications</h2>

<ul>
    <li><a href="http://mydev">http://mydev</a></li>
    <li><a href="http://myapi.mydev">http://myapi.mydev</a></li>
    <li><a href="http://myapi.mydev">http://myapi.mydev</a></li>
    <li><a href="http://makiety.myapi.mydev">http://makiety.myapi.mydev</a></li>
    <li><a href="http://api.myapi.mydev">http://api.myapi.mydev</a></li>
</ul>

</body>
</html>
