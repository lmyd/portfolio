<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Models\Choice as ChoiceModel;

/**
 * Unit test on ChoiceModel class
 * 
 * @package Tests\Unit
 * @author lmydlarski@gmail.com 
 */
class ChoiceModelTest extends TestCase
{
    /**
     * A unit test to create model and feed him.
     *
     * @return void
     */
    public function testCreateAndFeedObject()
    {
        $question = 'I am goooood!';
        $model = new ChoiceModel($question);

        $this->assertInstanceOf('App\Http\Models\Choice', $model);
        $this->assertEquals($question, $model->getText(), 'Setter & getter of text, are not equal');
    }
}
