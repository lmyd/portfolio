<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Models\Question as QuestionModel;

/**
 * Unit test on QuestionModel class
 * 
 * @package Tests\Unit
 * @author lmydlarski@gmail.com 
 */
class QuestionModelTest extends TestCase
{
    /**
     * A unit test to create model and feed him.
     *
     * @return void
     */
    public function testCreateAndFeedObject()
    {
        $question = 'How u feel today?';
        $createdAt = '2009-11-22 11:23:09';
        $choices = [
            'I`m very angry :(',
            'I am gooood :)'
        ];
        $model = new QuestionModel($question, $createdAt, $choices);

        $this->assertInstanceOf('App\Http\Models\Question', $model);
        $this->assertEquals($question, $model->getText(), 'Setter & getter of text, are not equal');
        $this->assertEquals($createdAt, $model->getCreatedAt(), 'Setter & getter date, are not equal');
        $this->assertEquals(count($choices), count($model->getChoices()));
    }
}
