<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Storages\Json as JsonStorage;
use App\Http\Storages\Csv as CsvStorage;
use App\Http\Storages\StorageFactory;

/**
 * Unit test on Storeges system
 * 
 * @package Tests\Unit
 * @author lmydlarski@gmail.com 
 */
class StoragesTest extends TestCase
{
    /**
     * A unit test for JsonStorage.
     *
     * @return void
     */
    public function testJsonInstances()
    {
        $storage = JsonStorage::instance();
        $this->assertInstanceOf('App\Http\Storages\Json', $storage);
        $storage = StorageFactory::instance('json');
        $this->assertInstanceOf('App\Http\Storages\Json', $storage);
    }

    /**
     * A unit test for JsonStorage.
     * @todo implement providers
     *
     * @return void
     */
    public function testCsvInstances()
    {
        $storage = CsvStorage::instance();
        $this->assertInstanceOf('App\Http\Storages\Csv', $storage);
        $storage = StorageFactory::instance('csv');
        $this->assertInstanceOf('App\Http\Storages\Csv', $storage);
    }
}
