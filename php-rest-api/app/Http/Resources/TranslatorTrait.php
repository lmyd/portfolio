<?php

namespace App\Http\Resources;

use Stichoza\GoogleTranslate\GoogleTranslate;

/**
 * Translator trait used in resources
 * 
 * @package App\Http\Resources
 * @author lmydlarski@gmail.com 
 */
trait TranslatorTrait
{
    /**
     * @var GoogleTranslate engine to translate
     */
    private static $translator = null;

    public function translate($text, $lang)
    {
        //lazy load of translator
        if (self::$translator === null) {
            if ($lang === null) {
                self::$translator = false;
            } else {
                self::$translator = new GoogleTranslate($lang);
            }
        }
        return self::$translator ? self::$translator->translate($text) : $text;
    }
}
