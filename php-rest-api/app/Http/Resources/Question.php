<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Request object of single question
 * 
 * @package App\Http\Resources
 * @author lmydlarski@gmail.com 
 */
class Question extends JsonResource
{
    use TranslatorTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text' => $this->translate($this->getText(), $request->query('lang')),
            'createdAt' => $this->getCreatedAt(),
            'choices' => Choice::collection(collect($this->getChoices())),
        ];
    }
}
