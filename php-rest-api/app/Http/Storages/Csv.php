<?php

namespace App\Http\Storages;

use Illuminate\Support\Facades\Storage;

/**
 * Storage engine from csv file resources
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 */
class Csv extends StorageAbstract implements StorageInterface
{

    use SingletonTrait;

    protected $columnNames = [];

    /**
     * @inheritdoc
     */
    public function loadData(): void
    {
        $rawData = array_map('str_getcsv', file(Storage::disk('local')->path('questions.csv')));
        $this->columnNames = $rawData[0];
        $data = array_slice($rawData, 1);
        foreach ($data as $question) {
            $choices = [];
            for ($i = 2; $i < count($question); $i++) {
                $choices[] = $question[$i];
            }
            $this->addQuestion($question[0], $choices, $question[1]);
        }
    }

    /**
     * @inheritdoc
     */
    public function saveData(): void
    {
        $file = fopen(Storage::disk('local')->path('questions.csv'), 'w');

        fputcsv($file, $this->columnNames);
        foreach ($this->questions as $question) {
            $row = [
                $question->getText(),
                $question->getCreatedAt()
            ];
            $choices = $question->getChoices();
            foreach ($choices as $choice) {
                $row[] = $choice->getText();
            }
            fputcsv($file, $row);
        }
        fclose($file);
    }
}
