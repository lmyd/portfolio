<?php

namespace App\Http\Storages;

/**
 * Factory of engines with lazy loader of each single instance engine
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 * @final
 */
final class StorageFactory
{
    const ENGINES = ['json', 'csv'];
    
    private static $engines = [];
    
    public static function instance(String $engine = null)
    {
        if (!in_array($engine, self::ENGINES)) {
            $engine = config('app.defaultStorage'); //configuration in .env file
        }

        if (!in_array($engine, self::$engines)) {
            switch ($engine) {
                case self::ENGINES[1]:
                    self::$engines[$engine] = Csv::instance();
                    break;
                default:
                    self::$engines[$engine] = Json::instance();
            }
        }

        return self::$engines[$engine];
    }
}
