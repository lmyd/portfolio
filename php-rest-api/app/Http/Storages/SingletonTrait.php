<?php

namespace App\Http\Storages;

/**
 * Singleton trait 
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 */
trait SingletonTrait
{
    /**
     * @var Storage Single instance external resources
     */
    private static $instance;

    /**
     * @inheritdoc
     */
    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return new self();
    }
}
