<?php

namespace App\Http\Storages;

use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\json_encode;

/**
 * Storage engine from json file resources
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 */
class Json extends StorageAbstract implements StorageInterface
{

    use SingletonTrait;

    /**
     * @inheritdoc
     */
    public function loadData(): void
    {
        $rawData = Storage::disk('local')->get('questions.json');
        $data = json_decode($rawData, true);

        foreach ($data as $question) {
            $choices = [];
            foreach ($question['choices'] as $choice) {
                $choices[] = $choice['text'];
            }
            $this->addQuestion($question['text'], $choices, $question['createdAt']);
        }
    }

    /**
     * @inheritdoc
     */
    public function saveData(): void
    {
        $questions = [];

        foreach ($this->questions as $question) {
            $row = [
                'text' => $question->getText(),
                'createdAt' => $question->getCreatedAt(),
                'choices' => []
            ];
            foreach ($question->getChoices() as $choice) {
                $row['choices'][] = [
                    'text' => $choice->getText()
                ];
            }
            $questions[] = $row;
        }

        Storage::disk('local')->put('questions.json', json_encode($questions));

    }
}