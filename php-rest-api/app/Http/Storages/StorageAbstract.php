<?php

namespace App\Http\Storages;

use App\Http\Models\Question;
use Illuminate\Support\Collection;

/**
 * Storage engine as Singleton 
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 * @abstract
 */
abstract class StorageAbstract
{

    /**
     * @var Array collection of question models
     */
    protected $questions = [];

    /**
     * Constructor of object when we load data from externals resources
     */
    protected function __construct()
    {
        $this->loadData();
    }

    /**
     * @inheritdoc
     */
    public function addQuestion(String $text, array $choices, String $createdAt = null): Question
    {
        $question = new Question($text, ($createdAt ?? date('Y-m-d H:i:s')), $choices);
        $this->questions[] = $question;

        return $question;
    }

    /**
     * @inheritdoc
     */
    public function all(): Collection
    {
        return collect($this->questions);
    }

    /**
     * @abstract method to load data in aours models
     */
    abstract public function loadData(): void;

}
