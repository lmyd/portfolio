<?php

namespace App\Http\Storages;

use Illuminate\Support\Collection;
use App\Http\Models\Question;

/**
 * Storage interface 
 * 
 * @package App\Http\Storages
 * @author lmydlarski@gmail.com 
 * @abstract
 */
interface StorageInterface
{
    /**
     * Each class need to be Singleton
     */
    public static function instance();

    /**
     * We must can add new question
     */
    public function addQuestion(String $text, array $choices, String $createdAt = null): Question;

    /**
     * We must return collection of questions
     */
    public function all(): Collection;

    /**
     * We must save data after added new question
     */
    public function saveData(): void;
}
