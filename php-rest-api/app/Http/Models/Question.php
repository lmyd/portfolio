<?php

namespace App\Http\Models;

/**
 * Model object of question from external resources 
 * 
 * @package App\Http\Models
 * @author lmydlarski@gmail.com 
 */
class Question
{
    /**
     * @var string Question text
     */
    private $text;

    /**
     * @var string Creation date of the question
     * @todo Convert to data object ex. Carbon
     */
    private $createdAt;

    /**
     * @var Choice[] Array of choices associated to the question
     */
    private $choices = [];

    /**
     * Constructor of object
     * 
     * @property String $text new value of text variable
     * @property String $createdAt new value of date variable
     * @property Array $choices array of string answers for question
     */
    public function __construct(String $text, String $createdAt, Array $choices = [])
    {
        $this->setText($text)
             ->setCreatedAt($createdAt);

        foreach ($choices as $choice) {
            $this->appendChoice(new Choice($choice));
        }
    }

    /**
     * Setter of text variable
     * 
     * @property String $text new value of variable
     * @return Question
     */
    public function setText(String $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Getter of text variable
     * 
     * @return String
     */
    public function getText(): String
    {
        return $this->text;
    }

    /**
     * Setter of createdAt variable
     * 
     * @property String $createdAt new value of variable
     * @return Question
     */
    public function setCreatedAt(String $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Getter of createdAt variable
     * 
     * @return String
     */
    public function getCreatedAt(): String
    {
        return $this->createdAt;
    }

    /**
     * Adding new choice to question
     * 
     * @property Choice $choice new choice object
     * @return Question
     */
    public function appendChoice(Choice $choice): self
    {
        $this->choices[] = $choice;
        return $this;
    }

    /**
     * Getter of choices array
     * 
     * @return Choice[]
     */
    public function getChoices(): Array
    {
        return $this->choices;
    }

}
