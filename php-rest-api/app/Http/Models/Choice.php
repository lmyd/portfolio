<?php

namespace App\Http\Models;

/**
 * Model object of question choice from external resources 
 * 
 * @package App\Http\Models
 * @author lmydlarski@gmail.com 
 */
class Choice
{
    /**
     * @var string Choice text
     */
    private $text;

    /**
     * Constructor of object
     * 
     * @property String $text new value of text variable
     */
    public function __construct(String $text)
    {
        $this->setText($text);
    }

    /**
     * Setter of text variable
     * 
     * @property String $text new value of variable
     * @return Choice
     */
    public function setText(String $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Getter of text variable
     * 
     * @return String
     */
    public function getText(): String
    {
        return $this->text;
    }
}
