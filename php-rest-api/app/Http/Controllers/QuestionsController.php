<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Question;
use App\Http\Storages\StorageFactory;
use App\Exceptions\ApiRequestException;

class QuestionsController extends Controller
{
    /**
     * Return collection of questions
     * 
     * @property Request $request
     * @throws ApiRequestException
     */
    public function index(Request $request)
    {
        try {
            if (!$request->query('lang')) {
                throw new ApiRequestException('Parametr lang is empty.');
            }
        } catch (ApiRequestException $e) {
            return response()->json($e->toArray(), $e->getCode());
        }
        return Question::collection(StorageFactory::instance()->all());
    }

    /**
     * Store new question
     * 
     * @property Request $request
     * @throws ApiRequestException
     */
    public function store(Request $request)
    {
        $input = $request->input();
        try {
            if (!isset($input['text'])) {
                throw new ApiRequestException('Parametr text is empty.');
            }
            if (!isset($input['createdAt'])) {
                throw new ApiRequestException('Parametr createdAt is empty.');
            }
            if (!isset($input['choices']) && !count($input['choices'])) {
                throw new ApiRequestException('Parametr choices is empty.');
            }
            $choices = [];
            foreach ($input['choices'] as $choice) {
                if (!isset($choice['text'])) {
                    throw new ApiRequestException('Parametr choices is empty.');
                };
                $choices[] = $choice['text'];
            }
        } catch (ApiRequestException $e) {
            return response()->json($e->toArray(), $e->getCode());
        }

        $storage = StorageFactory::instance();

        $question = $storage->addQuestion($input['text'], $choices, $input['createdAt']);

        $storage->saveData();

        return new Question($question);

    }

}
