<?php

namespace App\Exceptions;

use Exception;

/**
 * Exception when wrong http request
 * 
 * @package App\Exceptions
 * @author lmydlarski@gmail.com 
 */
class ApiRequestException extends Exception 
{
    /**
     * overide constructor
     */
    public function __construct(String $message)
    {
        parent::__construct($message, 500);
    }

    /**
     * Array for json response
     */
    public function toArray(): Array
    {
        return [
            'status' => 'error',
            'message' => $this->getMessage()
        ];
    }

}






