The goal of the exercise is to create a REST API that will handle two different resources :
multiple choice questions 
choices related to a question

Only specific endpoints are required as part of this exercise and are described in the open-api.yaml file attached to this email.

The data is provided in a JSON and in a CSV file (both files are attached to this email). In order to remain flexible, your system must be able to handle both data sources (only one at a time and preferably configurable in the system) and be easily extendable to more in the future. For the purpose of this exercise, the data must remain in the JSON or CSV file and those files must serve as your database (do not import those data in an external database such as MySQL).

As described in the Open API file, one of the endpoints ([GET]/questions) provides a query parameter lang that allows the API client to request a translated version of the questions text field and associated choices text field. In order to provide the translation, please use Google Translate API (the library “stichoza/google-translate-php” is available on Packagist to ease the implementation).
