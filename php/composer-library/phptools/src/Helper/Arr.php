<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 */
namespace LMYD\PhpTools\Helper;

/**
 * Class Arr
 * Helper for arrays
 *
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\PhpTools\Helper
 */
class Arr
{
    /**
     * Check if key exist in array
     *
     * @param array      $array
     * @param string|int $key
     *
     * @return bool
     */
    public static function exist(array &$array, $key)
    {
        return array_key_exists($key, $array);
    }

    /**
     * Return value from given array
     *
     * @param array      $array
     * @param string|int $key
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public static function get(array &$array, $key, $default = null)
    {
        return self::exist($array, $key) ? $array[$key] : $default;
    }

    /**
     * Return min value from given array,
     *
     * @param array $array
     *
     * @return int|float
     */
    public static function min(array &$array)
    {
        $min = current($array);

        foreach ($array as $value) {
            $min = $value < $min ? $value : $min;
        }

        return $min;
    }

    /**
     * Return max value from given array,
     *
     * @param array $array
     *
     * @return int|float
     */
    public static function max(array &$array)
    {
        $max = current($array);

        foreach ($array as $value) {
            $max = $value > $max ? $value : $max;
        }

        return $max;
    }

    /**
     * Return shortest value from given array,
     *
     * @param array $array
     *
     * @return string
     */
    public static function shortest(array &$array)
    {
        $shortest = current($array);

        foreach ($array as $value) {
            $shortest = strlen($value) < strlen($shortest) ? $value : $shortest;
        }

        return $shortest;
    }

    /**
     * Return longest value from given array,
     *
     * @param array $array
     *
     * @return string
     */
    public static function longest(array &$array)
    {
        $longest = current($array);

        foreach ($array as $value) {
            $longest = strlen($value) > strlen($longest) ? $value : $longest;
        }

        return $longest;
    }

    /**
     * Return trimmed array from both side
     *
     * @param array $array
     * @param mixed $toValue
     *
     * @return array
     */
    public static function trim(array &$array, $toValue = null)
    {
        $array = self::ltrim($array, $toValue);

        return self::rtrim($array, $toValue);
    }

    /**
     * Return trimmed array from left side
     *
     * @param array $array
     * @param mixed $toValue
     *
     * @return array
     */
    public static function ltrim(array &$array, $toValue = null)
    {
        foreach ($array as $key => $entry) {

            if ($toValue !== null) {
                if ($entry === $toValue) {
                    break;
                }
            } elseif (!empty($entry)) {
                break;
            }

            unset($array[$key]);
        }

        return $array;
    }

    /**
     * Return trimmed array from right side
     *
     * @param array $array
     * @param mixed $toValue
     *
     * @return array
     */
    public static function rtrim(array &$array, $toValue = null)
    {
        $array = self::ltrim(array_reverse($array), $toValue);

        return array_reverse($array);
    }
}
