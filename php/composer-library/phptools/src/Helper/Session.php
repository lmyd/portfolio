<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 */
namespace LMYD\PhpTools\Helper;

/**
 * Class Session
 * Helper for php session
 *
 * @singleton
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\PhpTools\Helper
 */
class Session
{
    /**
     * @var array of instaces
     */
    private static $instances = [];

    /**
     * @var string name of instance
     */
    private $name;

    /**
     * private Session constructor.
     *
     * @param string $name of instance
     */
    private function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Instance getter
     *
     * @param string $name
     *
     * @return Session
     */
    public static function instance($name = 'default')
    {
        if (!Arr::exist(self::$instances, $name)) {
            self::$instances[$name] = new self($name);
        }

        return self::$instances[$name];
    }

    /**
     * Getter of instance name
     *
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Getter of key for specific session key
     *
     * @param $key
     *
     * @return string
     */
    private function key($key)
    {
        return $this->name() . '-' . $key;
    }

    /**
     * Getter of session value, if not found will return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $key = $this->key($key);

        return Globals::SESSION($key, $default);
    }

    /**
     * Setter value for specific session
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return Session
     */
    public function set($key, $value)
    {
        $key            = $this->key($key);
        $_SESSION[$key] = $value;

        return $this;
    }

    /**
     * Single getter for value. Return value or default value and unset variable from session.
     *
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function flush($key, $default = null)
    {
        $key   = $this->key($key);
        $value = $this->get($key, $default);
        if (Arr::exist($_SESSION, $key)) {
            unset($_SESSION[$key]);
        }

        return $value;
    }
}
