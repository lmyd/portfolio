<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 */
namespace LMYD\PhpTools\Helper;

/**
 * Class Fn
 * Helper for others function
 *
 * @singleton
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\PhpTools\Helper
 */
class Fn
{
    /** @var string new line code */
    const NL = "/n";

    /**
     * Return newValue when oldValue !== ifNot.
     *
     * @param mixed $oldValue
     * @param mixed $ifNot
     * @param mixed $newValue
     *
     * @return mixed
     */
    public static function returnIfNot($oldValue, $ifNot, $newValue)
    {
        if ($oldValue === $ifNot) {
            return $oldValue;
        }

        return $newValue;
    }

    /**
     * Dump variable
     *
     * @param mixed $value
     * @param bool  $withHtml
     *
     * @return mixed
     */
    public static function debug(&$value, $withHtml = false)
    {
        if ($withHtml) {
            echo '<PRE>';
        }

        var_dump($value);

        if ($withHtml) {
            echo '</PRE>';
        }
    }

}
