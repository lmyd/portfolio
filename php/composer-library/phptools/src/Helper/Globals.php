<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 */
namespace LMYD\PhpTools\Helper;

/**
 * Class Globals
 * Helper for global variables
 *
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\PhpTools\Helper
 */
class Globals
{
    /** The filename of the currently executing script, relative to the document root */
    const SERVER_PHP_SELF = 'PHP_SELF';

    /** What revision of the CGI specification the server is using; i.e. 'CGI/1.1'. */
    const SERVER_GATEWAY_INTERFACE = 'GATEWAY_INTERFACE';

    /** The IP address of the server under which the current script is executing. */
    const SERVER_ADDR = 'SERVER_ADDR';

    /** The name of the server host under which the current script is executing. If the script is running on a virtual host, this will be the value defined for that virtual host. */
    const SERVER_NAME = 'SERVER_NAME';

    /** Server identification string, given in the headers when responding to requests . */
    const SERVER_SOFTWARE = 'SERVER_SOFTWARE';

    /** Name and revision of the information protocol via which the page was requested; i . e . 'HTTP/1.0'; */
    const SERVER_PROTOCOL = 'SERVER_PROTOCOL';

    /** Which request method was used to access the page; i . e . 'GET', 'HEAD', 'POST', 'PUT' . */
    const SERVER_REQUEST_METHOD = 'REQUEST_METHOD';

    /** The timestamp of the start of the request . Available since PHP 5.1.0 . */
    const SERVER_REQUEST_TIME = 'REQUEST_TIME';

    /** The timestamp of the start of the request, with microsecond precision . Available since PHP 5.4.0 . */
    const SERVER_REQUEST_TIME_FLOAT = 'REQUEST_TIME_FLOAT';

    /** The query string, if any, via which the page was accessed . */
    const SERVER_QUERY_STRING = 'QUERY_STRING';

    /** The document root directory under which the current script is executing, as defined in the server's configuration file. */
    const SERVER_DOCUMENT_ROOT = 'DOCUMENT_ROOT';

    /** Contents of the Accept: header from the current request, if there is one. */
    const SERVER_HTTP_ACCEPT = 'HTTP_ACCEPT';

    /** Contents of the Accept-Charset: header from the current request, if there is one. Example: 'iso - 8859 - 1,*,utf - 8'. */
    const SERVER_HTTP_ACCEPT_CHARSET = 'HTTP_ACCEPT_CHARSET';

    /** Contents of the Accept-Encoding: header from the current request, if there is one. Example: 'gzip'. */
    const SERVER_HTTP_ACCEPT_ENCODING = 'HTTP_ACCEPT_ENCODING';

    /** Contents of the Accept-Language: header from the current request, if there is one. Example: 'en'. */
    const SERVER_HTTP_ACCEPT_LANGUAGE = 'HTTP_ACCEPT_LANGUAGE';

    /** Contents of the Connection: header from the current request, if there is one. Example: 'Keep - Alive'. */
    const SERVER_HTTP_CONNECTION = 'HTTP_CONNECTION';

    /** Contents of the Host: header from the current request, if there is one. */
    const SERVER_HTTP_HOST = 'HTTP_HOST';

    /** The address of the page (if any) which referred the user agent to the current page. This is set by the user agent. Not all user agents will set this, and some provide the ability to modify HTTP_REFERER as a feature. In short, it cannot really be trusted. */
    const SERVER_HTTP_REFERER = 'HTTP_REFERER';

    /** Contents of the User-Agent: header from the current request, if there is one. This is a string denoting the user agent being which is accessing the page. A typical example is: Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586). Among other things, you can use this value with get_browser() to tailor your page's output to the capabilities of the user agent . */
    const SERVER_HTTP_USER_AGENT = 'HTTP_USER_AGENT';

    /** Set to a non - empty value if the script was queried through the HTTPS protocol . */
    const SERVER_HTTPS = 'HTTPS';

    /** The IP address from which the user is viewing the current page . */
    const SERVER_REMOTE_ADDR = 'REMOTE_ADDR';

    /** The Host name from which the user is viewing the current page . The reverse dns lookup is based off the REMOTE_ADDR of the user . */
    const SERVER_REMOTE_HOST = 'REMOTE_HOST';

    /** The port being used on the user's machine to communicate with the web server . */
    const SERVER_REMOTE_PORT = 'REMOTE_PORT';

    /** The authenticated user . */
    const SERVER_REMOTE_USER = 'REMOTE_USER';

    /** The authenticated user if the request is internally redirected . */
    const SERVER_REDIRECT_REMOTE_USER = 'REDIRECT_REMOTE_USER';

    /** The absolute pathname of the currently executing script . */
    const SERVER_SCRIPT_FILENAME = 'SCRIPT_FILENAME';

    /** The value given to the SERVER_ADMIN(for Apache) directive in the web server configuration file . If the script is running on a virtual host, this will be the value defined for that virtual host . */
    const SERVER_ADMIN = 'SERVER_ADMIN';

    /** The port on the server machine being used by the web server for communication . For default setups, this will be '80'; using SSL, for instance, will change this to whatever your defined secure HTTP port is . */
    const SERVER_PORT = 'SERVER_PORT';

    /** String containing the server version and virtual host name which are added to server - generated pages, if enabled . */
    const SERVER_SIGNATURE = 'SERVER_SIGNATURE';

    /** Filesystem - (not document root -) based path to the current script, after the server has done any virtual - to - real mapping . */
    const SERVER_PATH_TRANSLATED = 'PATH_TRANSLATED';

    /** Contains the current script's path. This is useful for pages which need to point to themselves. The __FILE__ constant contains the full path and filename of the current (i.e. included) file. */
    const SERVER_SCRIPT_NAME = 'SCRIPT_NAME';

    /** The URI which was given in order to access this page; for instance, ' / index . html'. */
    const SERVER_REQUEST_URI = 'REQUEST_URI';

    /** When doing Digest HTTP authentication this variable is set to the 'Authorization' header sent by the client (which you should then use to make the appropriate validation). */
    const SERVER_PHP_AUTH_DIGEST = 'PHP_AUTH_DIGEST';

    /** When doing HTTP authentication this variable is set to the username provided by the user. */
    const SERVER_PHP_AUTH_USER = 'PHP_AUTH_USER';

    /** When doing HTTP authentication this variable is set to the password provided by the user. */
    const SERVER_PHP_AUTH_PW = 'PHP_AUTH_PW';

    /** When doing HTTP authentication this variable is set to the authentication type. */
    const SERVER_AUTH_TYPE = 'AUTH_TYPE';

    /** Contains any client-provided pathname information trailing the actual script filename but preceding the query string, if available. For instance, if the current script was accessed via the URL http://www.example.com/php/path_info.php/some/stuff?foo=bar, then $_SERVER['PATH_INFO'] would contain /some/stuff. */
    const SERVER_PATH_INFO = 'PATH_INFO';

    /** Original version of 'PATH_INFO' before processed by PHP. */
    const SERVER_ORIG_PATH_INFO = 'ORIG_PATH_INFO';

    /**
     * Gets value from $_GET, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function GET($key, $default = null)
    {
        return Arr::get($_GET, $key, $default);
    }

    /**
     * Gets value from $_POST, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function POST($key, $default = null)
    {
        return Arr::get($_POST, $key, $default);
    }

    /**
     * Gets value from $_SERVER, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function SERVER($key, $default = null)
    {
        return Arr::get($_SERVER, $key, $default);
    }

    /**
     * Gets value from $_FILES, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function FILES($key, $default = null)
    {
        return Arr::get($_FILES, $key, $default);
    }

    /**
     * Gets value from $_SESSION, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function SESSION($key, $default = null)
    {
        return Arr::get($_SESSION, $key, $default);
    }

    /**
     * Gets value from $_COOKIE, or return default value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function COOKIE($key, $default = null)
    {
        return Arr::get($_COOKIE, $key, $default);
    }

}
