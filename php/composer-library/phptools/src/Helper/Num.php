<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 */
namespace LMYD\PhpTools\Helper;

/**
 * Class Num
 * Helper for numeric functions
 *
 * @singleton
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\PhpTools\Helper
 */
class Num
{

    /**
     * Return true if num is odd.
     *
     * @param int $num
     *
     * @return bool
     */
    public static function isOdd($num)
    {
        if (is_float($num)) {
            return false;
        }

        return $num % 2 !== 0;
    }

    /**
     * Return true if num is even.
     *
     * @param int $num
     *
     * @return bool
     */
    public static function isEven($num)
    {
        if (is_float($num)) {
            return false;
        }

        return $num % 2 === 0;
    }

}
