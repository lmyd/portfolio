<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
namespace LMYD\Test\PhpTools\Helper;

use LMYD\PhpTools\Helper\Session;

/**
 * Test class SessionTest
 *
 * @coversDefaultClass LMYD\PhpTools\Helper\Session
 *
 * @author             Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c)     2016 Lukasz Mydlarski
 * @package            LMYD\Test\PhpTools\Helper
 */
class SessionTest extends \PHPUnit_Framework_TestCase
{
    /** @var Session */
    private $defaultInstance;

    /** @var Session */
    private $defaultInstance2;

    /** @var Session */
    private $secondInstance;

    /** @var Session */
    private $secondInstance2;

    /**
     * Prepare session for instances
     *
     * @return void
     */
    private function prepareInstances()
    {
        $this->defaultInstance  = Session::instance();
        $this->defaultInstance2 = Session::instance();
        $this->secondInstance   = Session::instance('second');
        $this->secondInstance2  = Session::instance('second');
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::instance
     * @covers ::name
     */
    public function shouldMakeAndGetInstances()
    {
        $this->prepareInstances();

        $this->assertInstanceOf(Session::class, $this->defaultInstance);
        $this->assertInstanceOf(Session::class, $this->defaultInstance2);
        $this->assertInstanceOf(Session::class, $this->secondInstance);
        $this->assertInstanceOf(Session::class, $this->secondInstance2);

        $this->assertEquals('default', $this->defaultInstance->name());
        $this->assertEquals('default', $this->defaultInstance2->name());
        $this->assertEquals('second', $this->secondInstance->name());
        $this->assertEquals('second', $this->secondInstance2->name());
    }

    /**
     * @test
     *
     * @covers ::key
     * @covers ::set
     * @covers ::get
     */
    public function shouldSetAndGetValueFromInstance()
    {
        $this->prepareInstances();

        $ins1 = $this->defaultInstance->set('firstValue', 'raz');
        $ins2 = $this->secondInstance->set('firstValue', 'raz1');

        $this->assertInstanceOf(Session::class, $ins1);

        $this->assertInstanceOf(Session::class, $ins2);

        $this->assertEquals('raz', $this->defaultInstance->get('firstValue'));

        $this->assertEquals('raz', $this->defaultInstance2->get('firstValue'));

        $this->assertEquals('raz', $this->defaultInstance->get('firstValue', 'dwa'));

        $this->assertEquals('dwa', $this->defaultInstance->get('secondValue', 'dwa'));

        $this->assertEquals(null, $this->defaultInstance->get('secondValue'));

        $this->assertEquals('raz1', $this->secondInstance->get('firstValue'));
    }

    /**
     * @test
     *
     * @covers ::key
     * @covers ::set
     * @covers ::flush
     */
    public function shouldSetAndFlushValueFromInstance()
    {
        $this->prepareInstances();

        $this->defaultInstance->set('flushValue', 'err msg');

        $this->assertEquals('err msg', $this->defaultInstance->flush('flushValue'));
        $this->assertEquals(null, $this->defaultInstance->flush('flushValue'));
        $this->assertEquals('raz', $this->defaultInstance->flush('flushValue', 'raz'));
    }
}
