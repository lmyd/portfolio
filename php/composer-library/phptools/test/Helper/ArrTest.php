<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
namespace LMYD\Test\PhpTools\Helper;

use LMYD\PhpTools\Helper\Arr;

/**
 * Test class ArrTest
 *
 * @coversDefaultClass LMYD\PhpTools\Helper\Arr
 *
 * @author         Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c) 2016 Lukasz Mydlarski
 * @package        LMYD\Test\PhpTools\Helper
 */
class ArrTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     *
     * @dataProvider getExistProvider
     *
     * @covers ::exist
     *
     * @param array      $arr
     * @param string|int $key
     * @param bool       $response
     */
    public function shouldExist($arr, $key, $response)
    {
        $checked = Arr::exist($arr, $key);

        if ($response) {
            $this->assertTrue($checked);
        } else {
            $this->assertFalse($checked);
        }

    }

    /**
     * Data provider for test shouldExist()
     *
     * @return array
     */
    public function getExistProvider()
    {
        return [
            [[1, 2, 3], 1, true],
            [[1, 2, 3], 3, false],
            [['a', 'b', 'c'], 1, true],
            [['a', 'b', 'c'], 3, false],
            [['first' => 1, 'second' => 2, 'third' => 3], 'third', true],
            [['first' => 1, 'second' => 2, 'third' => 3], 'last', false],
            [['first' => 1, 'second' => 2, 'third' => 3], 1, false],
            [['first' => 'a', 'second' => 'b', 'third' => 'c'], 'second', true],
            [['first' => 'a', 'second' => 'b', 'third' => 'c'], 'b', false],
            [['first' => 'a', 'second' => 'b', 'third' => 'c'], 2, false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getProvider
     *
     * @covers ::get
     *
     * @param array      $arr
     * @param string|int $key
     * @param mixed      $neededValue
     * @param mixed      $default
     */
    public function shouldGet($arr, $key, $neededValue, $default)
    {
        if ($default === null) {
            $value = Arr::get($arr, $key);
        } else {
            $value = Arr::get($arr, $key, $default);
        }

        $this->assertEquals($neededValue, $value);
    }

    /**
     * Data provider for test shouldGet()
     *
     * @return array
     */
    public function getProvider()
    {
        return [
            [[1, 2, 3], 1, 2, null],
            [[1, 2, 3], 1, 2, 3],
            [[1, 2, 3], 3, null, null],
            [[1, 2, 3], 3, 10, 10],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getMinProvider
     *
     * @covers ::min
     *
     * @param array $arr
     * @param mixed $neededValue
     */
    public function shouldGetMin($arr, $neededValue)
    {
        $value = Arr::min($arr);

        $this->assertEquals($neededValue, $value);
    }

    /**
     * Data provider for test shouldGetMin()
     *
     * @return array
     */
    public function getMinProvider()
    {
        return [
            [[1, 2, 3], 1],
            [[2, 1, 3, -10], -10],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getMaxProvider
     *
     * @covers ::max
     *
     * @param array $arr
     * @param mixed $neededValue
     */
    public function shouldGetMax($arr, $neededValue)
    {
        $value = Arr::max($arr);

        $this->assertEquals($neededValue, $value);
    }

    /**
     * Data provider for test shouldGetMax()
     *
     * @return array
     */
    public function getMaxProvider()
    {
        return [
            [[1, 2, 3], 3],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getShortestProvider
     *
     * @covers ::shortest
     *
     * @param array $arr
     * @param mixed $neededValue
     */
    public function shouldGetShortest($arr, $neededValue)
    {
        $value = Arr::shortest($arr);

        $this->assertEquals($neededValue, $value);
    }

    /**
     * Data provider for test shouldGetShortest()
     *
     * @return array
     */
    public function getShortestProvider()
    {
        return [
            [[1, 2, 3], 1],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getLongestProvider
     *
     * @covers ::longest
     *
     * @param array $arr
     * @param mixed $neededValue
     */
    public function shouldGetLongest($arr, $neededValue)
    {
        $value = Arr::longest($arr);

        $this->assertEquals($neededValue, $value);
    }

    /**
     * Data provider for test shouldGetLongest()
     *
     * @return array
     */
    public function getLongestProvider()
    {
        return [
            [[1, 2, 3], 1],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getLtrimProvider
     *
     * @covers ::ltrim
     *
     * @param array $arr
     * @param mixed $toValue
     * @param array $neededArr
     */
    public function shouldLtrim($arr, $toValue, $neededArr)
    {
        $arr = Arr::ltrim($arr, $toValue);

        $this->assertEquals($neededArr, $arr);
    }

    /**
     * Data provider for test shouldLtrim()
     *
     * @return array
     */
    public function getLtrimProvider()
    {
        return [
            [[1, 2, 3], null, [1, 2, 3]],
            [[null, null, 2, 3, null], null, [2 => 2, 3 => 3, 4 => null]],
            [[1, 2, 3], 2, [1=>2, 2=>3]],
            [[null, null, 2, 3, null], 3, [3 => 3, 4 => null]],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getRtrimProvider
     *
     * @covers ::rtrim
     *
     * @param array $arr
     * @param mixed $toValue
     * @param array $neededArr
     */
    public function shouldRtrim($arr, $toValue, $neededArr)
    {
        $arr = Arr::rtrim($arr, $toValue);

        $this->assertEquals($neededArr, $arr);
    }

    /**
     * Data provider for test shouldRtrim()
     *
     * @return array
     */
    public function getRtrimProvider()
    {
        return [
            [[1, 2, 3], null, [1, 2, 3]],
            [[null, null, 2, 3, null, null], null, [null, null, 2, 3]],
            [[1, 2, 3], 2, [1, 2]],
            [[null, null, 2, 3, null, null], 2, [null, null, 2]],
        ];
    }

    /**
     * @test
     *
     * @dataProvider getTrimProvider
     *
     * @covers ::trim
     *
     * @param array $arr
     * @param mixed $toValue
     * @param array $neededArr
     */
    public function shouldTrim($arr, $toValue, $neededArr)
    {
        $arr = Arr::trim($arr, $toValue);

        $this->assertEquals($neededArr, $arr);
    }

    /**
     * Data provider for test shouldTrim()
     *
     * @return array
     */
    public function getTrimProvider()
    {
        return [
            [[1, 2, 3], null, [1, 2, 3]],
            [[null, null, 2, 3, null, null], null, [2, 3]],
            [[1, 2, 3, 2, 4, 5], 2, [2, 3, 2]],
            [[null, null, 2, 3, null, null], 2, [2]],
        ];
    }
}
