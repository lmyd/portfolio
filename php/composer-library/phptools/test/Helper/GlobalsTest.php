<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
namespace LMYD\Test\PhpTools\Helper;

use LMYD\PhpTools\Helper\Globals;

/**
 * Test class GlobalsTest
 *
 * @coversDefaultClass LMYD\PhpTools\Helper\Globals
 *
 * @author             Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c)     2016 Lukasz Mydlarski
 * @package            LMYD\Test\PhpTools\Helper
 */
class GlobalsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Prepare globals arrays
     *
     * @return void
     */
    private function prepareGlobals()
    {
        $_GET['getValue']                = 'valueGet';
        $_POST['postValue']              = 'valuePost';
        $_FILES['filesValue']            = 'valueFiles';
        $_SESSION['sessionValue']        = 'valueSession';
        $_COOKIE['cookieValue']          = 'valueCookie';
        $_SERVER['PHP_SELF']             = '1';
        $_SERVER['GATEWAY_INTERFACE']    = '2';
        $_SERVER['SERVER_ADDR']          = '3';
        $_SERVER['SERVER_NAME']          = '4';
        $_SERVER['SERVER_SOFTWARE']      = '5';
        $_SERVER['SERVER_PROTOCOL']      = '6';
        $_SERVER['REQUEST_METHOD']       = '7';
        $_SERVER['REQUEST_TIME']         = '8';
        $_SERVER['REQUEST_TIME_FLOAT']   = '9';
        $_SERVER['QUERY_STRING']         = '10';
        $_SERVER['DOCUMENT_ROOT']        = '11';
        $_SERVER['HTTP_ACCEPT']          = '12';
        $_SERVER['HTTP_ACCEPT_CHARSET']  = '13';
        $_SERVER['HTTP_ACCEPT_ENCODING'] = '14';
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = '15';
        $_SERVER['HTTP_CONNECTION']      = '16';
        $_SERVER['HTTP_HOST']            = '17';
        $_SERVER['HTTP_REFERER']         = '18';
        $_SERVER['HTTP_USER_AGENT']      = '19';
        $_SERVER['HTTPS']                = '20';
        $_SERVER['REMOTE_ADDR']          = '21';
        $_SERVER['REMOTE_HOST']          = '22';
        $_SERVER['REMOTE_PORT']          = '23';
        $_SERVER['REMOTE_USER']          = '24';
        $_SERVER['REDIRECT_REMOTE_USER'] = '25';
        $_SERVER['SCRIPT_FILENAME']      = '26';
        $_SERVER['SERVER_ADMIN']         = '27';
        $_SERVER['SERVER_PORT']          = '28';
        $_SERVER['SERVER_SIGNATURE']     = '29';
        $_SERVER['PATH_TRANSLATED']      = '30';
        $_SERVER['SCRIPT_NAME']          = '31';
        $_SERVER['REQUEST_URI']          = '32';
        $_SERVER['PHP_AUTH_DIGEST']      = '33';
        $_SERVER['PHP_AUTH_USER']        = '34';
        $_SERVER['PHP_AUTH_PW']          = '35';
        $_SERVER['AUTH_TYPE']            = '36';
        $_SERVER['PATH_INFO']            = '37';
        $_SERVER['ORIG_PATH_INFO']       = '38';
    }

    /**
     * @test
     *
     * @covers ::GET
     * @covers ::POST
     * @covers ::FILES
     * @covers ::SESSION
     * @covers ::COOKIE
     *
     */
    public function shouldGetGlobals()
    {
        $this->prepareGlobals();

        $this->assertEquals('valueGet', Globals::GET('getValue'));
        $this->assertEquals($_GET['getValue'], Globals::GET('getValue'));

        $this->assertEquals('valuePost', Globals::POST('postValue'));
        $this->assertEquals($_POST['postValue'], Globals::POST('postValue'));

        $this->assertEquals('valueFiles', Globals::FILES('filesValue'));
        $this->assertEquals($_FILES['filesValue'], Globals::FILES('filesValue'));

        $this->assertEquals('valueSession', Globals::SESSION('sessionValue'));
        $this->assertEquals($_SESSION['sessionValue'], Globals::SESSION('sessionValue'));

        $this->assertEquals('valueCookie', Globals::COOKIE('cookieValue'));
        $this->assertEquals($_COOKIE['cookieValue'], Globals::COOKIE('cookieValue'));
    }

    /**
     * @test
     *
     * @covers ::SERVER
     */
    public function shouldGetServerGlobals()
    {
        $this->prepareGlobals();

        $this->assertEquals($_SERVER['PHP_SELF'], Globals::SERVER(Globals::SERVER_PHP_SELF));
        $this->assertEquals($_SERVER['GATEWAY_INTERFACE'], Globals::SERVER(Globals::SERVER_GATEWAY_INTERFACE));
        $this->assertEquals($_SERVER['SERVER_ADDR'], Globals::SERVER(Globals::SERVER_ADDR));
        $this->assertEquals($_SERVER['SERVER_NAME'], Globals::SERVER(Globals::SERVER_NAME));
        $this->assertEquals($_SERVER['SERVER_SOFTWARE'], Globals::SERVER(Globals::SERVER_SOFTWARE));
        $this->assertEquals($_SERVER['SERVER_PROTOCOL'], Globals::SERVER(Globals::SERVER_PROTOCOL));
        $this->assertEquals($_SERVER['REQUEST_METHOD'], Globals::SERVER(Globals::SERVER_REQUEST_METHOD));
        $this->assertEquals($_SERVER['REQUEST_TIME'], Globals::SERVER(Globals::SERVER_REQUEST_TIME));
        $this->assertEquals($_SERVER['REQUEST_TIME_FLOAT'], Globals::SERVER(Globals::SERVER_REQUEST_TIME_FLOAT));
        $this->assertEquals($_SERVER['QUERY_STRING'], Globals::SERVER(Globals::SERVER_QUERY_STRING));
        $this->assertEquals($_SERVER['DOCUMENT_ROOT'], Globals::SERVER(Globals::SERVER_DOCUMENT_ROOT));
        $this->assertEquals($_SERVER['HTTP_ACCEPT'], Globals::SERVER(Globals::SERVER_HTTP_ACCEPT));
        $this->assertEquals($_SERVER['HTTP_ACCEPT_CHARSET'], Globals::SERVER(Globals::SERVER_HTTP_ACCEPT_CHARSET));
        $this->assertEquals($_SERVER['HTTP_ACCEPT_ENCODING'], Globals::SERVER(Globals::SERVER_HTTP_ACCEPT_ENCODING));
        $this->assertEquals($_SERVER['HTTP_ACCEPT_LANGUAGE'], Globals::SERVER(Globals::SERVER_HTTP_ACCEPT_LANGUAGE));
        $this->assertEquals($_SERVER['HTTP_CONNECTION'], Globals::SERVER(Globals::SERVER_HTTP_CONNECTION));
        $this->assertEquals($_SERVER['HTTP_HOST'], Globals::SERVER(Globals::SERVER_HTTP_HOST));
        $this->assertEquals($_SERVER['HTTP_REFERER'], Globals::SERVER(Globals::SERVER_HTTP_REFERER));
        $this->assertEquals($_SERVER['HTTP_USER_AGENT'], Globals::SERVER(Globals::SERVER_HTTP_USER_AGENT));
        $this->assertEquals($_SERVER['HTTPS'], Globals::SERVER(Globals::SERVER_HTTPS));
        $this->assertEquals($_SERVER['REMOTE_ADDR'], Globals::SERVER(Globals::SERVER_REMOTE_ADDR));
        $this->assertEquals($_SERVER['REMOTE_HOST'], Globals::SERVER(Globals::SERVER_REMOTE_HOST));
        $this->assertEquals($_SERVER['REMOTE_PORT'], Globals::SERVER(Globals::SERVER_REMOTE_PORT));
        $this->assertEquals($_SERVER['REMOTE_USER'], Globals::SERVER(Globals::SERVER_REMOTE_USER));
        $this->assertEquals($_SERVER['REDIRECT_REMOTE_USER'], Globals::SERVER(Globals::SERVER_REDIRECT_REMOTE_USER));
        $this->assertEquals($_SERVER['SCRIPT_FILENAME'], Globals::SERVER(Globals::SERVER_SCRIPT_FILENAME));
        $this->assertEquals($_SERVER['SERVER_ADMIN'], Globals::SERVER(Globals::SERVER_ADMIN));
        $this->assertEquals($_SERVER['SERVER_PORT'], Globals::SERVER(Globals::SERVER_PORT));
        $this->assertEquals($_SERVER['SERVER_SIGNATURE'], Globals::SERVER(Globals::SERVER_SIGNATURE));
        $this->assertEquals($_SERVER['PATH_TRANSLATED'], Globals::SERVER(Globals::SERVER_PATH_TRANSLATED));
        $this->assertEquals($_SERVER['SCRIPT_NAME'], Globals::SERVER(Globals::SERVER_SCRIPT_NAME));
        $this->assertEquals($_SERVER['REQUEST_URI'], Globals::SERVER(Globals::SERVER_REQUEST_URI));
        $this->assertEquals($_SERVER['PHP_AUTH_DIGEST'], Globals::SERVER(Globals::SERVER_PHP_AUTH_DIGEST));
        $this->assertEquals($_SERVER['PHP_AUTH_USER'], Globals::SERVER(Globals::SERVER_PHP_AUTH_USER));
        $this->assertEquals($_SERVER['PHP_AUTH_PW'], Globals::SERVER(Globals::SERVER_PHP_AUTH_PW));
        $this->assertEquals($_SERVER['AUTH_TYPE'], Globals::SERVER(Globals::SERVER_AUTH_TYPE));
        $this->assertEquals($_SERVER['PATH_INFO'], Globals::SERVER(Globals::SERVER_PATH_INFO));
        $this->assertEquals($_SERVER['ORIG_PATH_INFO'], Globals::SERVER(Globals::SERVER_ORIG_PATH_INFO));
    }

}
