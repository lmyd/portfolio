<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
namespace LMYD\Test\PhpTools\Helper;

use LMYD\PhpTools\Helper\Fn;

/**
 * Test class FnTest
 *
 * @coversDefaultClass LMYD\PhpTools\Helper\Fn
 *
 * @author             Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c)     2016 Lukasz Mydlarski
 * @package            LMYD\Test\PhpTools\Helper
 */
class FnTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     *
     * @dataProvider getReturnIfNotProvider
     *
     * @covers ::returnIfNot
     *
     * @param mixed $oldValue
     * @param mixed $ifNot
     * @param mixed $newValue
     * @param mixed $expectedValue
     */
    public function shouldReturnIfNot($oldValue, $ifNot, $newValue, $expectedValue)
    {
        $value = Fn::returnIfNot($oldValue, $ifNot, $newValue);
        $this->assertEquals($expectedValue, $value);
    }

    /**
     * Data provider for test shouldReturnIfNot()
     *
     * @return array
     */
    public function getReturnIfNotProvider()
    {
        return [
            [1,1,2,1],
            [1,2,2,2],
            [1,3,4,4],
        ];
    }
}
