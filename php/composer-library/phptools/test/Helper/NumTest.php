<?php
/**
 * Copyright 2016 Lukasz Mydlarski <lmydlarski@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
namespace LMYD\Test\PhpTools\Helper;

use LMYD\PhpTools\Helper\Num;

/**
 * Test class NumTest
 *
 * @coversDefaultClass LMYD\PhpTools\Helper\Num
 *
 * @author             Lukasz Mydlarski <lmydlarski@gmail.com>
 * @copyright  (c)     2016 Lukasz Mydlarski
 * @package            LMYD\Test\PhpTools\Helper
 */
class NumTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     *
     * @dataProvider oddEvenProvider
     *
     * @covers ::isOdd
     * @covers ::isEven
     *
     * @param int  $num
     * @param bool $isEven
     * @param bool $isOdd
     */
    public function shouldOddEven($num, $isEven, $isOdd)
    {
        $even = Num::isEven($num);
        $odd = Num::isOdd($num);

        $this->assertSame($isEven, $even);
        $this->assertSame($isOdd, $odd);
    }

    public function oddEvenProvider()
    {
        return [
            [1, false, true],
            [2, true, false],
            [3, false, true],
            [4, true, false],
            [5, false, true],
            [6, true, false],
            [7, false, true],

            [1.1, false, false],
            [3.1, false, false],
        ];
    }
}
