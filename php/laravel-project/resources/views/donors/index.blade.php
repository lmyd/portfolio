@extends('layout')

@section('title', __('donors.title'))

@section('description', __('donors.description'))

@section('content')

   <div class="columns is-multiline">
      @foreach($donors as $donor)
         <div class="column is-4">
            @include('donors.partials.card', $donor)
         </div>
      @endforeach
   </div>

@endsection