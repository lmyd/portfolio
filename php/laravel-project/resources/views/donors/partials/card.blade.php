<div class="card">
   <div class="card-content">
      <div class="media">
         <div class="media-left">
            <figure class="image is-48x48">
               <img src="{{ $img }}">
            </figure>
         </div>
         <div class="media-content">
            <p class="title is-4">{{ $title }}</p>
            @isset($subtitle)
            <p class="subtitle is-6">{!! $subtitle !!}</p>
            @endisset
         </div>
      </div>
      <div class="content">
          @include('partials.tags4card')
      </div>
   </div>
   @auth
      @if(isset($action['status']) && $action['status'] == config('actions.status.wait') && Auth::user()->getHash() == $hash)
         <join-action url="{{ route('join2action', ['slug' => $action['slug']]) }}">{{ __('donors.i-join') }}</join-action>
      @endif
   @endauth
</div>