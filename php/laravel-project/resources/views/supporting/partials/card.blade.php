<div class="card">
   <div class="card-image">
      <div class="fb-video" data-href="{{ $video }}" data-allowfullscreen="true"></div>
   </div>
   <div class="card-content">
      <div class="media">
         <div class="media-content">
            <p class="title is-4">{{ $title }}</p>
            <p class="subtitle is-6">{{ $subtitle }}</p>
         </div>
      </div>
      <div class="content">{{ $description }}</div>
   </div>
</div>