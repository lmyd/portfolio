@extends('layout')

@section('title', __('supporting.title'))

@section('description', __('supporting.description'))

@section('content')

   <div class="columns is-multiline">
      @foreach($supporting as $person)
         <div class="column is-3">
            @include('supporting.partials.card', $person)
         </div>
      @endforeach
   </div>

@endsection