<nav class="tabs is-boxed">
    <div class="container">
        <ul>
            <li class="{{route('dashboard') == url()->current() ? 'is-active':''}}">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="{{route('participants') == url()->current() ? 'is-active':''}}">
                <a href="{{ route('participants') }}">Uczestnicy</a>
            </li>
            <li class="{{route('my-competitions') == url()->current() ? 'is-active':''}}">
                <a href="{{ route('my-competitions') }}">Rywalizacje</a>
            </li>
            <li class="{{route('hits') == url()->current() ? 'is-active':''}}">
                <a href="{{ route('hits') }}">Zgłoszenia</a>
            </li>
            <li class="{{route('struggles') == url()->current() ? 'is-active':''}}">
                <a href="{{ route('struggles') }}">Zmagania</a>
            </li>
        </ul>
    </div>
</nav>