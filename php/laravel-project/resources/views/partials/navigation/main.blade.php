
<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ route('main') }}">
                <img src="/img/logo-{{ app()->getLocale() }}.png" alt="{{ __('app.name') }}">
            </a>
            <a class="navbar-item is-hidden-desktop" href="https://www.facebook.com/Judo-Sport-Walki-z-Rakiem-958212147647495">
                <span class="icon has-text-link"><i class="fa fa-3x fa-facebook-square"></i></span>
            </a>
            <button class="button navbar-burger my-navbar-burger" @click="toogleNavbarBurger = !toogleNavbarBurger">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
        <div class="navbar-menu" :class="{'is-active': toogleNavbarBurger}">
            <div class="navbar-start">
                <a href="{{ route('actions') }}" class="navbar-item {{route('actions') == url()->current() ? 'is-active':''}}">{{ __('menu.actions') }}</a>
                <a href="{{ route('proteges') }}" class="navbar-item {{route('proteges') == url()->current() ? 'is-active':''}}">{{ __('menu.proteges') }}</a>
                <a href="{{ route('donors') }}" class="navbar-item {{route('donors') == url()->current() ? 'is-active':''}}">{{ __('menu.donors') }}</a>
                <a href="{{ route('supporting') }}" class="navbar-item {{route('supporting') == url()->current() ? 'is-active':''}}">{{ __('menu.supporting') }}</a>
                <a href="{{ route('sponsors') }}" class="navbar-item {{route('sponsors') == url()->current() ? 'is-active':''}}">{{ __('menu.sponsors') }}</a>
            </div>
            <div class="navbar-end">

                <div class="field is-grouped">
                <a class="navbar-item is-hidden-desktop-only">
                    <payu action="{{ route('payuSend') }}" type="small">{{ csrf_field() }}</payu>
                </a>

                <a class="navbar-item is-hidden-desktop-only" href="https://www.facebook.com/Judo-Sport-Walki-z-Rakiem-958212147647495">
                    <span class="icon has-text-link"><i class="fa fa-2x fa-facebook-square"></i></span>
                </a>
                </div>
                {{--
                <div class="navbar-item">
                    <div class="field is-grouped">
                        <a href="{{ route('main', ['locale' => 'pl']) }}" class="navbar-item">
                            <span class="flag-icon flag-icon-pl"></span>
                        </a>
                        <a href="{{ route('main', ['locale' => 'en']) }}" class="navbar-item">
                            <span class="flag-icon flag-icon-gb"></span>
                        </a>

                    </div>
                </div>

                @guest
                    <div class="navbar-item">
                        <a href="{{ route('login') }}" class="button ">
                            <span class="icon"><i class="fa fa-sign-in"></i></span>
                            <span>{{ __('menu.login') }}</span>
                        </a>
                    </div>
                @else
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">{{ __('menu.my-profile') }}</a>
                        <div class="navbar-dropdown">
                            <a href="{{ route('dashboard') }}" class="navbar-item">{{ __('menu.dashboard') }}</a>
                            <a href="{{ route('editProfile') }}" class="navbar-item">{{ __('menu.edit-profile') }}</a>
                            <a href="{{ route('regulations') }}" class="navbar-item">{{ __('menu.regulations') }}</a>
                            <hr class="navbar-divider">
                            <div class="navbar-item">
                                <logout-form action="{{ route('logout') }}" label="{{ __('menu.logout') }}">{{ csrf_field() }}</logout-form>
                            </div>

                        </div>
                    </div>
                @endguest
                --}}
            </div>
        </div>
    </div>
</nav>