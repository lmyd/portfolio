
@if($status = session(\App\Http\Controllers\Controller::STATUS_SUCCESS, false))
    <notification class="is-success">{{ $status }}</notification>
@endif

@if($status = session(\App\Http\Controllers\Controller::STATUS_INFO, false))
    <notification class="is-info">{{ $status }}</notification>
@endif

@if($status = session(\App\Http\Controllers\Controller::STATUS_DANGER, false))
    <notification class="is-danger">{{ $status }}</notification>
@endif
