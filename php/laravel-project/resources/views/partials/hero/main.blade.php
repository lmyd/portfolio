<?php
    $sponsors = app()->make(\App\Services\SponsorsService::class)->getListArray(null, true);
    /** @var \App\Services\StatsService $stats */
    $stats = app()->make(\App\Services\StatsService::class);
?>
<section class="hero is-dark is-bold">
    <div class="hero-body my-hero-body">
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column is-three-quarters">
                    <h1 class="title">@yield('title')</h1>
                    <h2 class="subtitle">@yield('description')</h2>
                    <nav class="level is-mobile">
                        <div class="level-item has-text-centered">
                            <div>
                                <p class="heading is-hidden-mobile">Akcji</p>
                                <p class="subtitle">
                                    <span class="icon is-dark has-text-info"><i class="fa fa-ambulance"></i></span>
                                    <span>{{ number_format($stats->getActionsCount()) }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered">
                            <div>
                                <p class="heading is-hidden-mobile">Podopiecznych</p>
                                <p class="subtitle">
                                    <span class="icon is-dark has-text-success"><i class="fa fa-heartbeat"></i></span>
                                    <span>{{ number_format($stats->getProtegesCount()) }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered">
                            <div>
                                <p class="heading is-hidden-mobile">Donatorów</p>
                                <p class="subtitle">
                                    <span class="icon is-dark has-text-warning"><i class="fa fa-users"></i></span>
                                    <span>{{ number_format($stats->getDonorsCount()) }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered">
                            <div>
                                <p class="heading is-hidden-mobile">Zebranych litrów</p>
                                <p class="subtitle">
                                    <span class="icon is-dark has-text-danger"><i class="fa fa-tint"></i></span>
                                    <span>{{ $stats->getBloodCount() }}</span>
                                </p>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="column is-hidden-mobile is-paddingless">
                    <div class="carousel">
                        <div class="carousel-container">
                            <div class="carousel-content carousel-animate carousel-animate-slide">
                                @foreach($sponsors as $sponsor)
                                    <div class="carousel-item" @click="goto('{{ $sponsor['www'] }}')">
                                        <img class="is-background" style="object-fit:fill" src="{{ $sponsor['img'] }}" title="{{ $sponsor['title'] }}">
                                    </div>
                                @endforeach
                            </div>
                            <div class="carousel-nav-left is-hidden">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                            </div>
                            <div class="carousel-nav-right is-hidden" ref="sponsorsCarouselNext">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-hidden-mobile is-paddingless">
                    <figure class="image is-128x128 is-pulled-right">
                        <img src="/img/logo-hero.png" alt="{{ __('app.motto') }}">
                    </figure>
                </div>
            </div>
        </div>
    </div>

    {{--
    @auth
    <div class="hero-foot">
        <div class="container">
            @include('partials.navigation.user')
        </div>
    </div>
    @endauth
    --}}

</section>



