<section class="hero is-primary is-bold">
    <div class="hero-body">
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column">
                    <h1 class="title">Interesująca aplikacja?</h1>
                    <h2 class="subtitle">Zarejestruj się bez żadnych opłat i w kilka chwil, zostań rywalizatorem!</h2>
                </div>
                <div class="column">
                    <a class="button is-info is-fullwidth is-large" href="#">
                        <span class="icon"><i class="fa fa-id-card"></i></span>
                        <span>Rejestracja</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>