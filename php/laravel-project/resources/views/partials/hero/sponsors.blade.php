<?php $sponsors = app()->make(\App\Services\SponsorsService::class)->getListArray(5); ?>
<section class="hero is-primary is-bold">
    <div class="hero-body my-hero-body">
        <div class="container">
            <div class="columns is-multiline is-vcentered">
                <div class="column">
                    <h1 class="title">{{ __('app.hero-sponsors-title') }}</h1>
                    <h2 class="subtitle">{{ __('app.hero-sponsors-description') }}</h2>
                </div>
                @foreach($sponsors as $sponsor)
                <div class="column">
                    <a href="{{ $sponsor['www'] }}" target="_blank">
                        <figure class="image is-128x128">
                            <img src="{{ $sponsor['img'] }}" title="{{ $sponsor['title'] }}">
                        </figure>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>