<section class="hero is-warning is-bold">
    <div class="hero-body">
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column">
                    <h2 class="subtitle">{{ __('app.hero-promotion') }}</h2>
                </div>
                <div class="column">
                    <a href="#" class="button is-fullwidth is-info">
                        <span class="icon"><i class="fa fa-facebook"></i></span>
                        <span>Facebook</span>
                    </a>
                </div>
                <div class="column">
                    <a href="#" class="button is-fullwidth bd-tw-button">
                        <span class="icon"><i class="fa fa-twitter"></i></span>
                        <span>Twitter</span>
                    </a>
                </div>
                <div class="column">
                    <a href="#" class="button is-fullwidth bd-google-button">
                        <span class="icon"><i class="fa fa-google-plus"></i></span>
                        <span>Google Plus</span>
                    </a>
                </div>
                <div class="column">
                    <a href="#" class="button is-fullwidth bd-lnin-button">
                        <span class="icon"><i class="fa fa-linkedin"></i></span>
                        <span>Linked In</span>
                    </a>
                </div>
                <div class="column">
                    <a href="#" class="button is-fullwidth bd-pint-button">
                        <span class="icon"><i class="fa fa-pinterest-p"></i></span>
                        <span>Pinterest</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
