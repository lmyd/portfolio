<footer class="footer">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div class="content">
                    <p>Copyright © 2015 - {{date('Y')}}</p>
                    <p>{{ __('app.rights-reserved') }}</p>
                </div>
            </div>

            <div class="column">
                <div class="content">
                    {{--
                    <p>{{ __('app.donation') }}</p>
                    <p><a href="#" class="button payu">{{ __('app.payu') }}&nbsp;<strong>PayU</strong></a></p>
                    <p>{{ __('app.bank') }}</p>
                    --}}
                    <p><a href="mailto:judosportwalkizrakiem@gmail.com">Fundacja JUDO - Sport walki z rakiem</a></p>

                    <!-- p>By móc prężnie działać <strong>Fundacja potrzebuje środków</strong> na swoją działalność, które możecie pomóc nam zbierać <strong>przekazując darowiznę lub 1% podatku</strong>, wspierając tym samym walkę naszych podopiecznych.</p -->

                    <p>By móc prężnie działać <strong>Fundacja potrzebuje środków</strong> na swoją działalność, które możecie pomóc nam zbierać <strong>przekazując darowiznę</strong> na numer konta:</p>

                    <p>NRB: <strong>97 1750 0012 0000 0000 3878 9627</strong></p>

                    <p>lub poprzez bezpieczny system płatności:</p>

                    <payu action="{{ route('payuSend') }}" type="normal">{{ csrf_field() }}</payu>

                    <p>Wesprzeć walkę naszych podopiecznych można także <strong>przekazując 1% podatku</strong> na Fundację.
                        <br>Cel szczegółowy <strong>SPORT WALKI Z RAKIEM 8925</strong>
                        <br>KRS: <strong>0000270261</strong></p>

                    <p></p>

                </div>
            </div>

            <div class="column">
                <div class="content has-text-right">
                    <p><i>{{ __('app.motto') }}</i></p>
                </div>
            </div>
        </div>
        <p class="has-text-centered">NIP: 524-284-74-58 / KRS: 0000709948 / ul. Skarbka z Gór 130 lok 6 / 03-287 Warszawa</p>
    </div>
</footer>