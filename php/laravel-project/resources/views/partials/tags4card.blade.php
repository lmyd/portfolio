@isset($tags)
<div class="field is-grouped is-grouped-multiline">
    @foreach($tags as $tag)
        <div class="control">
            <div class="tags has-addons tooltip" data-tooltip="{{ __($tag['tooltip']) }}">
                <span class="tag icon is-dark has-text-danger"><i class="fa {{ $tag['icon'] }}"></i></span>
                <span class="tag {{ $tag['bg'] }}">{{ $tag['value'] }}</span>
            </div>
        </div>
    @endforeach
</div>
@endisset
