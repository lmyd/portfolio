@extends('layout')

@section('title', __('regulations.title'))

@section('description', __('regulations.description'))

@section('content')

   @if(Auth::check() && !Auth::user()->isAcceptRegulations())
      <notification class="is-danger">{{ __('regulations.pls-accept-regulations') }}</notification>
   @endif

   <h3 class="title is-4 is-spaced has-text-centered">
      - &sect; 1 -<br>Tytuł paragrafu
   </h3>
   <div class="content">
      <strong>Pkt 1</strong>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat neque ut finibus blandit. Aliquam risus ipsum, dignissim sed mattis eu, posuere nec mi. Nulla convallis, ligula sed sagittis elementum, dolor massa tincidunt nunc, ac iaculis arcu nisl vel erat. Vestibulum cursus ut felis eget faucibus. Pellentesque a pulvinar dolor. Vestibulum laoreet, dui nec eleifend finibus, magna est lobortis urna, et bibendum sem nulla in ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut quis nisi lobortis, pretium sapien non, ultrices metus. Curabitur cursus ipsum condimentum tortor euismod dapibus. Praesent eleifend ornare purus, vel tempor dui efficitur quis. Aenean a gravida enim, et porttitor augue.</p>
      <strong>Pkt 2</strong>
      <p>Nulla non nunc interdum, congue lacus at, posuere turpis. Vestibulum et arcu et lectus placerat bibendum. Suspendisse at augue nisi. Donec nec dolor a ipsum feugiat gravida ut a nisl. Quisque scelerisque porta massa. Phasellus non accumsan lorem. Vivamus rutrum ipsum massa, ut luctus justo molestie sed. Aenean venenatis, metus sit amet faucibus posuere, justo diam ullamcorper dolor, a condimentum diam urna semper dui. Maecenas a interdum quam. Donec rutrum fermentum neque ut pulvinar. Curabitur a tempus turpis. Nam congue mauris in risus laoreet vehicula. Integer maximus quam sodales, finibus neque in, sagittis mi.</p>
   </div>
   <h3 class="title is-4 is-spaced has-text-centered">
      - &sect; 2 -<br>Tytuł paragrafu
   </h3>
   <div class="content">
      <strong>Pkt 1</strong>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat neque ut finibus blandit. Aliquam risus ipsum, dignissim sed mattis eu, posuere nec mi. Nulla convallis, ligula sed sagittis elementum, dolor massa tincidunt nunc, ac iaculis arcu nisl vel erat. Vestibulum cursus ut felis eget faucibus. Pellentesque a pulvinar dolor. Vestibulum laoreet, dui nec eleifend finibus, magna est lobortis urna, et bibendum sem nulla in ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut quis nisi lobortis, pretium sapien non, ultrices metus. Curabitur cursus ipsum condimentum tortor euismod dapibus. Praesent eleifend ornare purus, vel tempor dui efficitur quis. Aenean a gravida enim, et porttitor augue.</p>
      <strong>Pkt 2</strong>
      <p>Nulla non nunc interdum, congue lacus at, posuere turpis. Vestibulum et arcu et lectus placerat bibendum. Suspendisse at augue nisi. Donec nec dolor a ipsum feugiat gravida ut a nisl. Quisque scelerisque porta massa. Phasellus non accumsan lorem. Vivamus rutrum ipsum massa, ut luctus justo molestie sed. Aenean venenatis, metus sit amet faucibus posuere, justo diam ullamcorper dolor, a condimentum diam urna semper dui. Maecenas a interdum quam. Donec rutrum fermentum neque ut pulvinar. Curabitur a tempus turpis. Nam congue mauris in risus laoreet vehicula. Integer maximus quam sodales, finibus neque in, sagittis mi.</p>
   </div>
   <h3 class="title is-4 is-spaced has-text-centered">
      - &sect; 3 -<br>Tytuł paragrafu
   </h3>
   <div class="content">
      <strong>Pkt 1</strong>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat neque ut finibus blandit. Aliquam risus ipsum, dignissim sed mattis eu, posuere nec mi. Nulla convallis, ligula sed sagittis elementum, dolor massa tincidunt nunc, ac iaculis arcu nisl vel erat. Vestibulum cursus ut felis eget faucibus. Pellentesque a pulvinar dolor. Vestibulum laoreet, dui nec eleifend finibus, magna est lobortis urna, et bibendum sem nulla in ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut quis nisi lobortis, pretium sapien non, ultrices metus. Curabitur cursus ipsum condimentum tortor euismod dapibus. Praesent eleifend ornare purus, vel tempor dui efficitur quis. Aenean a gravida enim, et porttitor augue.</p>
      <strong>Pkt 2</strong>
      <p>Nulla non nunc interdum, congue lacus at, posuere turpis. Vestibulum et arcu et lectus placerat bibendum. Suspendisse at augue nisi. Donec nec dolor a ipsum feugiat gravida ut a nisl. Quisque scelerisque porta massa. Phasellus non accumsan lorem. Vivamus rutrum ipsum massa, ut luctus justo molestie sed. Aenean venenatis, metus sit amet faucibus posuere, justo diam ullamcorper dolor, a condimentum diam urna semper dui. Maecenas a interdum quam. Donec rutrum fermentum neque ut pulvinar. Curabitur a tempus turpis. Nam congue mauris in risus laoreet vehicula. Integer maximus quam sodales, finibus neque in, sagittis mi.</p>
   </div>

   @if(Auth::check() && !Auth::user()->isAcceptRegulations())

      <div class="columns">
         <div class="column has-text-centered">
            <logout-form action="{{ route('logout') }}" label="{{ __('regulations.no-accept') }}">{{ csrf_field() }}</logout-form>
         </div>
         <div class="column has-text-centered">
            <accept-regulations-form action="{{ route('acceptRegulations') }}" label="{{ __('regulations.yes-accept') }}">{{ csrf_field() }}</accept-regulations-form>
         </div>
      </div>

   @endif

@endsection