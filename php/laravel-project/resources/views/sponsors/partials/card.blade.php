<div class="card">
   <div class="card-image">
      <figure class="image is-1by1">
         <a href="{{$www}}" target="_blank"><img src="{{ $img }}"></a>
      </figure>
   </div>
   <div class="card-content">
      <div class="media">
         <div class="media-content">
            <p class="title is-4">{{ $title }}</p>
            <p class="subtitle"><a href="{{ $www }}" target="_blank">{{ $www }}</a></p>
         </div>
      </div>
      <div class="content">
         <p class="has-text-justified">{!! $description !!}</p>
      </div>
   </div>
</div>