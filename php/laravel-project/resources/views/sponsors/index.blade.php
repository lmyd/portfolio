@extends('layout')

@section('title', __('sponsors.title'))

@section('description', __('sponsors.description'))

@section('content')

   <div class="columns is-multiline">
      @foreach($sponsors as $sponsor)
         <div class="column is-4">
            @include('sponsors.partials.card', $sponsor)
         </div>
      @endforeach
   </div>

@endsection
