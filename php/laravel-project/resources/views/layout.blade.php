<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116658784-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116658784-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ __('app.name') }} :: @yield('title')</title>
    <meta name="description" content="{{ __('app.description') }}">
    @if(App::environment('local'))
    <link rel="stylesheet" href="/css/app.css?{{time()}}">
    @else
    <link rel="stylesheet" href="/css/app.css">
    @endif
</head>
<body>
<div id="app">

    @include('partials.navigation.main')

    @include('partials.hero.main')

    <div class="section">
        <div class="container">
            @include('partials.notification')
            @yield('content')
        </div>
    </div>

    @include('partials.hero.sponsors')

    {{--
    @include('partials.hero.social')
    --}}

    @include('partials.footer')

</div>
<script src="/js/datepicker.js"></script>
<script src="/js/carousel.js"></script>
@if(App::environment('local'))
<script src="/js/app.js?{{time()}}"></script>
@else
<script src="/js/app.js"></script>
@endif

</body>
</html>
