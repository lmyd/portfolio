@extends('layout')

@section('title', __('dashboard.title'))

@section('description', __('dashboard.description'))

@section('content')

    @if (session('statusOk'))
        <notification class="is-success">{{ __('dashboard.' . session('statusOk')) }}</notification>
    @endif

    @if (count($errors->all()))
        <notification class="is-danger">
            <ul>
            @foreach($errors->all() as $e)
                <li>{{ __('dashboard.' . $e) }}</li>
            @endforeach
            </ul>
        </notification>
    @endif

    <div class="columns">
        <div class="column">
            <h1 class="title">{{ __('dashboard.make-donation') }}</h1>
            <article class="message">
                <div class="message-body">
                    <donation-form action="{{ route('makeDonation') }}" v-bind="{{ json_encode($donationForm) }}">
                        {{ csrf_field() }}
                    </donation-form>
                </div>
            </article>
        </div>
        <div class="column">
            <h1 class="title">{{ __('dashboard.best-three') }}</h1>
            @foreach($bestDonors as $key => $bestDonor)
                <article class="message">
                    <?php $subtitle = $key < 3 ? __('dashboard.place-'. ($key + 1)) : __('dashboard.place') . ' ' . ($key + 1) ?>
                    @include('donors.partials.card', array_merge($bestDonor, ['subtitle' => '<fa-icon name="trophy" style="color:' . __('dashboard.place-color-' . ($key + 1)) . '"></fa-icon> ' . $subtitle]))
                </article>
            @endforeach
        </div>
    </div>

    @if(count($waitingActions))
        <h1 class="title">{{ __('dashboard.my-actions') }}</h1>

        <div class="columns is-multiline">
            @foreach($waitingActions as $action)
                <div class="column is-4">
                    @include('actions.partials.card', $action)
                </div>
            @endforeach
        </div>
    @endif

    <h1 class="title">{{ __('dashboard.my-donations') }}</h1>

    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th>{{ __('dashboard.given_day') }}</th>
            <th>{{ __('dashboard.protege') }}</th>
            <th>{{ __('dashboard.action') }}</th>
            <th>{{ __('dashboard.type') }}</th>
            <th class="has-text-right">{{ __('dashboard.amount') }}</th>
            <th class="has-text-right">{{ __('dashboard.blood') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($donations['data'] as $donation)
            <tr>
                <td>{{ $donation['given_day'] }}</td>
                <td><a href="{{ route('protege', ['slug' => $donation['protege']['slug']]) }}">{{ $donation['protege']['title'] }}</a></td>
                <td>
                    @isset($donation['action'])
                        <a href="{{ route('action', ['slug' => $donation['action']['slug']]) }}">{{ $donation['action']['title'] }}</a>
                    @endisset
                </td>
                <td>{{ $donation['type'] }}</td>
                <td class="has-text-right">{{ $donation['amount'] }}</td>
                <td class="has-text-right">{{ $donation['blood'] }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4"></th>
                <th class="has-text-right">{{ $donations['summary']['amount'] }}</th>
                <th class="has-text-right">{{ $donations['summary']['blood'] }}</th>
            </tr>
        </tfoot>
    </table>

@endsection