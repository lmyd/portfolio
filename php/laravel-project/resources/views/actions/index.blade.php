@extends('layout')

@section('title', __('actions.title'))

@section('description', __('actions.description'))

@section('content')

   <div class="columns is-multiline">
      @foreach($actions as $action)
      <div class="column is-4">
         @include('actions.partials.card', $action)
      </div>
      @endforeach
   </div>

@endsection