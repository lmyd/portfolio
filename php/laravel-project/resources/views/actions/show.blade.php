@extends('layout')

@section('title', __('actions.title'))

@section('description', __('actions.description'))

@section('content')

   <div class="columns">
      <div class="column is-4">
         @include('actions.partials.card', $action)
      </div>
      <div class="column">
         <figure class="image is-16by9">
            <img src="{{ $action['img'] }}">
         </figure>
      </div>
   </div>

   @if(count($supportings))
   <h1 class="title">{{ $action['status'] == config('actions.status.was') ? __('actions.was-support-us') : __('actions.support-us') }}</h1>

   <div class="columns is-multiline">
      @foreach($supportings as $person)
         <div class="column is-3">
            @include('supporting.partials.card', $person)
         </div>
      @endforeach
   </div>
   @endif

   @if(count($sponsors))
   <h1 class="title">{{ $action['status'] == config('actions.status.was') ? __('actions.was-sponsor-us') : __('actions.sponsor-us') }}</h1>

   <div class="columns is-multiline">
      @foreach($sponsors as $sponsor)
         <div class="column is-3">
            @include('sponsors.partials.card', $sponsor)
         </div>
      @endforeach
   </div>
   @endif

   @if(count($donors))
   <h1 class="title">{{ $action['status'] == config('actions.status.was') ? __('actions.was-donors-us') : ($action['status'] == config('actions.status.wait') ? __('actions.will-donors-us') : __('actions.donors-us')) }}</h1>

   <div class="columns is-multiline">
      @foreach($donors as $donor)
         <div class="column is-4">
            @include('donors.partials.card', $donor)
         </div>
      @endforeach
   </div>
   @endif




   <a href="{{ route('actions') }}" class="button">
      <span class="icon"><i class="fa fa-hand-o-left"></i></span>
      <span>{{ __('actions.back') }}</span>
   </a>
@endsection