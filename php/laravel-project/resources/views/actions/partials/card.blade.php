<div class="card">
   @if(!$singleActionPage)
   <div class="card-image" @click="goto('{{ route('action', ['slug' => $slug]) }}')">
      <figure class="image is-16by9">
         <img src="{{ $img }}">
      </figure>
   </div>
   @endif
   <div class="card-content">
      <div class="media">
         <div class="media-left">
            <figure class="image is-48x48">
               <img src="{{ $promotor }}">
            </figure>
         </div>
         <div class="media-content">
            <p class="title is-4">{{ $title }}</p>
            <p class="subtitle is-6">{{ $datetime }}</p>
         </div>
      </div>
      <div class="content">

         @isset($countdown)
            <countdown v-bind="{{ json_encode($countdown) }}"></countdown>
         @endisset

         @include('partials.tags4card')

         <ul>
            <li>
               <a href="{{ route('protege', ['slug' => $protege]) }}">
                  <span>{{ '@' . $protege }}</span>
                  <span class="icon"><i class="fa fa-hand-o-right"></i></span>
               </a>
            </li>
            <li>{{ $place }}</li>
            <li>{{ $address }}</li>
            <li>{{ $city }}</li>
         </ul>

         @if($singleActionPage)
         <p class="has-text-justified">{!! $description !!}</p>
         @endif

      </div>
   </div>

   @if(!$singleActionPage)
   <footer class="card-footer">
      <a href="{{ route('action', ['slug' => $slug]) }}" class="card-footer-item">
         <span>{{ __('actions.more') }}</span>
         <span class="icon"><i class="fa fa-hand-o-right"></i></span>
      </a>
   </footer>
   @endif

</div>