@extends('layout')

@section('title', 'Resetowanie hasła')

@section('description', 'Jeśli zapomniałeś hasła lub chcesz je zmienić z innych powodów.')

@section('content')

    @if (session('status'))
        <notification class="is-success">{{ session('status') }}</notification>
    @endif

    <div class="columns">
        <div class="column"></div>
        <div class="column is-one-third">
            <forgot-form action="{{ route('password.email') }}" email="{{ old('email') }}" error="{{ $errors->first('email') }}">
                {{ csrf_field() }}
            </forgot-form>
        </div>
        <div class="column"></div>
    </div>

@endsection

@section('contentOLD')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
