@extends('layout')

@section('title', __('auth.title'))

@section('description', __('auth.description'))

@section('content')

    <div class="columns is-multiline is-centered">
        <div class="column is-half">
            <article class="message">
                <div class="message-body">
                    <login-form
                            action="{{ route('login') }}"
                            email="{{ old('email') }}"
                            email-label="{{ __('auth.email') }}"
                            email-error="{{ $errors->first('email') }}"
                            password="{{ old('password') }}"
                            password-label="{{ __('auth.password') }}"
                            password-error="{{ $errors->first('password') }}"
                            label="{{ __('auth.login') }}"
                    >{{ csrf_field() }}</login-form>
                    <a href="{{ route('password.request') }}"><small>{{ __('auth.forgot') }}</small></a>
                </div>
            </article>
        </div>
    </div>

@endsection
