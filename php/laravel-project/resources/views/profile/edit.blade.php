@extends('layout')

@section('title', __('profile.title'))

@section('description', __('profile.description'))

@section('content')

    @if (session('statusOk'))
        <notification class="is-success">{{ __('profile.' . session('statusOk')) }}</notification>
    @endif

    @if (count($errors->all()))
        <notification class="is-danger">{{ __('app.form-error') }}</notification>
    @endif

    <div class="columns">
        <div class="column">
            <article class="message">
                <div class="message-body">
                    <user-form
                        action="{{ route('updateProfile') }}"
                        login-error="{{ $errors->first('login') }}"
                        password-error="{{ $errors->first('password') }}"
                        date-error="{{ $errors->first('date') }}"
                        file-error="{{ $errors->first('fotoFile') }}"
                        v-bind="{{ json_encode($userForm) }}">
                        {{ csrf_field() }}
                    </user-form>
                </div>
            </article>
        </div>
        <div class="column">
            @include('donors.partials.card', $donor)
        </div>
    </div>

@endsection