<article class="media">
    <div class="media-content">
        <div class="content">
            <p>
                <strong>{{ $created_at }}</strong><br>
                {{ $article }}
            </p>
        </div>
    </div>
</article>