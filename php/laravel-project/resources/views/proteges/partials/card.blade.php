<div class="card">
    <div class="card-content">
        <div class="media">
            <div class="media-left">
                <figure class="image is-64x64">
                    @if($passAway)
                        <img src="/img/kir.png" style="background-image: url('{{ $img }}'); background-size: cover;">
                    @else
                        <img src="{{ $img }}">
                    @endif
                </figure>
            </div>
            <div class="media-content">
                <p class="title">{{ $title }}</p>
                <p class="subtitle">
                    <a href="{{ route('protege', ['slug' => $slug]) }}">{{ '@' . $slug }}</a>
                </p>
            </div>
        </div>
        <div class="content">
            @include('partials.tags4card')

            @if($singleProtegePage)

                <blockquote>
                    @if($passAway)
                        <span class="has-text-weight-bold">Ś.p. {{ $passAwayDate }}</span>
                    @else
                        {{ __('proteges.donors-info') }}<br>
                        <span class="has-text-weight-bold">{{ $title }}, {{ $hospital }}</span>
                    @endif
                </blockquote>

                <p class="has-text-justified">{!! $description !!}</p>
            @endif
        </div>
    </div>

    @if(!$singleProtegePage)
        <footer class="card-footer">
            <a href="{{ route('protege', ['slug' => $slug]) }}" class="card-footer-item">
                <span>{{ __('proteges.more') }}</span>
                <span class="icon"><i class="fa fa-hand-o-right"></i></span>
            </a>
        </footer>
    @endif

</div>
