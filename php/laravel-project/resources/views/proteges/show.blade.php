@extends('layout')

@section('title', __('proteges.title'))

@section('description', __('proteges.description'))

@section('content')

   <div class="columns">
      <div class="column is-4">
         @include('proteges.partials.card', $protege)
      </div>
      <div class="column">
         @if(count($actions))
         <div class="content">
            <h3>{{ __('proteges.last-action-'.$actions[0]['status']) }}</h3>
         </div>
         <figure class="image is-16by9">
            <a href="{{ route('action', ['slug' => $actions[0]['slug']]) }}"><img src="{{ $actions[0]['img'] }}"></a>
         </figure>
         @endif
      </div>
   </div>

   @if(count($news) > 1)
      <h1 class="title">{{ __('proteges.news') }}</h1>
      <div class="content">
         @foreach($news as $article)
            @include('proteges.partials.news', $article)
         @endforeach
      </div>
   @endif

   @if(count($actions) > 1)
      <h1 class="title">{{ __('proteges.actions') }}</h1>

      <div class="columns is-multiline">
         @for($i=1; $i< count($actions); $i++)
            <div class="column is-4">
               @include('actions.partials.card', $actions[$i])
            </div>
         @endfor
      </div>
   @endif

   @if(count($donors))
      <h1 class="title">{{ __('proteges.donors-me') }}</h1>

      <div class="columns is-multiline">
         @foreach($donors as $donor)
            <div class="column is-4">
               @include('donors.partials.card', $donor)
            </div>
         @endforeach
      </div>
   @endif

   <a href="{{ route('proteges') }}" class="button">
      <span class="icon"><i class="fa fa-hand-o-left"></i></span>
      <span>{{ __('proteges.back') }}</span>
   </a>

@endsection