@extends('layout')

@section('title', __('proteges.title'))

@section('description', __('proteges.description'))

@section('content')

   <div class="columns is-multiline">
      @foreach($proteges as $protege)
         <div class="column is-4">
            @include('proteges.partials.card', $protege)
         </div>
      @endforeach
   </div>

@endsection