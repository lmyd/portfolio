@extends('layout')

@section('title', __('main.title'))

@section('description', __('main.description'))

@section('content')

   <payu action="{{ route('payuSend') }}" type="large">{{ csrf_field() }}</payu>

   <div class="columns">
      <div class="column">
         <div class="carousel">
            <div class="carousel-container">
               <div class="carousel-content">{{-- carousel-animate carousel-animate-slide--}}
                  {{--
                  @foreach($actions as $action)
                     <div class="carousel-item" @click="goto('{{ route('action', ['slug' => $action['slug']]) }}')">
                        <img class="is-background" src="{{ $action['img'] }}" >
                        {{- -<div class="title" >{{ $action['title'] }}</div>- -}}
                     </div>
                  @endforeach
                  <div class="carousel-item" @click="goto('{{ route('action', ['slug' => 'pokonujemy-granice-2019']) }}')">
                     <img class="is-background" src="{{ Storage::url( 'actions/pokonujemy-granice-2019.png') }}" >
                  </div>
                  --}}
                  <div class="carousel-item">
                     <img class="is-background" src="/img/about.png" >
                  </div>
               </div>
               <div class="carousel-nav-left">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i>
               </div>
               <div class="carousel-nav-right" ref="actionCarouselNext">
                  <i class="fa fa-chevron-right" aria-hidden="true"></i>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="columns is-multiline">
      @foreach($supporting as $person)
         <div class="column">
            @include('supporting.partials.card', $person)
         </div>
      @endforeach
   </div>

@endsection