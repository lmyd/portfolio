
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia strony z protegowanymi PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Nasi podopieczni',
    'description' => 'Lista podopiecznych dla których zbieramy krew.',
    'back' => 'wróć do listy podopiecznych',
    'more' => 'więcej',
    'donors-info' => 'Oddajac krew, zaznacz dla kogo:',
    'last-action-wait' => 'Najbliższa akcja:',
    'last-action-continue' => 'Trwająca akcja:',
    'last-action-was' => 'Ostatnia akcja:',
    'news' => 'Wiadomości',
    'donors-me' => 'Pomogli:',
    'actions' => 'Akcje:',
];
