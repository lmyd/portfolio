<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Główne tłumaczenia aplikacji PL
    |--------------------------------------------------------------------------
    */

    'name' => 'JUDO - sport walki z rakiem!',
    'motto' => 'Walkę mamy we krwi!',
    'description' => 'Nasza fundacja JUDO - sport walki z rakiem jest organizacją non profit i powstała, aby pomagać małym wojownikom w walce z ich największym życiowym przeciwnikiem - rakiem. Wspieramy ich w zmaganiach organizując zbiórki krwi, która jest niezbędna do leczenia choroby. Wielokrotne przetaczania, dostarczanie organizmowi nowego paliwa do walki - to codzienność dzieci cierpiących na tę nieprzewidywalną i wyniszczającą chorobę.',
    'hero-sponsors-title' => 'Zapraszamy',
    'hero-sponsors-description' => 'na strony sponsorów',
    'hero-promotion' => 'Pomóż rozpromować fundację!',
    'rights-reserved' => 'Wszystkie prawa zastrzeżone.',
    'donation' => 'Możesz wspomóc naszą ideę:',
    'payu' => 'Przekazuję darowiznę z',
    'bank' => 'lub na konto Get In Bank:',
    'gender' => 'Płeć',
    'male' => 'Mężczyzna',
    'female' => 'Kobieta',
    'form-error' => 'Wykryto błędy w formularzu.'
];
