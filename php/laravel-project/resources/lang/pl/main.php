
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia głównej strony aplikacji PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Witamy na stronach fundacji,',
    'description' => 'która jednoczy całą społeczność Judo w walce z największym wrogiem - NOWOTWORAMI.',
];
