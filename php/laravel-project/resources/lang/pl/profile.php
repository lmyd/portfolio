
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia profile PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Edycja profilu',
    'description' => 'Zaaktualizuj swoje dane.',
    'update-user' => 'Edycja profilu',
    'login' => 'Nazwa dawcy',
    'email' => 'Email dawcy',
    'password' => 'Nowe hasło',
    'password-placeholder' => 'Podaj nowe haslo do konta',
    'password2' => 'Powtórz nowe hasło',
    'password2-placeholder' => 'Powtórz nowe hasło do konta',
    'regulations-date' => 'Data akceptacji regulaminu',
    'birthday' => 'Data urodzenia',
    'type' => 'Grupa krwi',
    'foto-file' => 'Zdjęcie dawcy',
    'delete-foto-file' => 'Skasuj zdjęcie z serwera',
    'update-user-btn' => 'Aktualizuj',
    'user-was-updated' => 'Profil został zaaktualizowany.',
    'form-error-login-min' => 'Nazwa dawcy musi mieć przynajmiej 5 znaków.',
    'form-error-password-min' => 'Nowe hasło musi mieć przynajmiej 5 znaków.',
    'form-error-password-confirmed' => 'Błędnie powtórzone nowe hasło.',
    'form-error-date-format' => 'Błędny format daty [RRRR-MM-DD].',
    'form-error-date-before_or_equal' => 'Data powinna spełniać wymóg pełnoletności.',
    'form-error-gender-in' => 'Błędna płeć [Kobieta lub Mężczyzna]',
    'form-error-typeId-required' => 'Grupa krwi jest wymagana.',
    'form-error-typeId-exists' => 'Podana grupa krwi nie istnieje.',
    'form-error-fotoFile-mimes' => 'Plik powinien być typu graficznego [jpeg,jpg,bmp,png,gif]',
];
