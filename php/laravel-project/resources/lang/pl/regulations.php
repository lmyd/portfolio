
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia strony regulaminu aplikacji PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Regulamin serwisu fundacji,',
    'description' => 'Zasady korzystania z aplikacji fundacji.',
    'pls-accept-regulations' => 'Zanim rozpoczniesz korzystanie z naszego serwisu, prosimy o zapoznanie się i akceptację (poniżej treści) regulaminu serwisu.',
    'no-accept' => 'Nie akceptuję',
    'yes-accept' => 'Akceptuję regulamin serwisu',
];
