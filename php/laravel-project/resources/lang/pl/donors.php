
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia strony z dawcami PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Nasi dawcy krwi',
    'description' => 'Lista ludzi dobrego serca bez których nasza fundacja by nie istniała!',
    'anonymous' => 'Dawca anonimowy',
    'many_anonymous' => '{1}Dawca anominowy|[2,*]:COUNT dawców anonimowych',
    'i-join' => 'Dołączam',


    'tag-blood-type' => 'Moja grupa krwi',
    'tag-blood' => 'Oddałem krwi',
    'tag-donors' => 'Wsparłem',
    'tag-actions' => 'Brałem udział'

];
