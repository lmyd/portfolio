<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ttłumaczenia tooltipow tagow PL
    |--------------------------------------------------------------------------
    */

    'blood-type' => 'Najbardziej potrzebna grupa krwi',
    'blood-count' => 'Zebraliśmy krwi',
    'donors-count' => 'Wsparło nas',
    'donors-count-will-be' => 'Wyraziło chęć uczestnictwa',
    'actions-count' => 'Zorganizowaliśmy już'

];
