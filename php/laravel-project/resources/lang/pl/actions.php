
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia strony z akcjami PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Nasze akcje',
    'description' => 'Lista zrealizowanych i nadchodzących akcji zbiórki krwi.',
    'more' => 'więcej',
    'from' => 'od',
    'to' => 'do',
    'back' => 'wróć do listy akcji',

    'support-us' => 'Na akcję zapraszają:',
    'was-support-us' => 'Na akcję zapraszali:',

    'sponsor-us' => 'Wspierają nas:',
    'was-sponsor-us' => 'Wspierali nas:',

    'donors-us' => 'Uczestniczą:',
    'will-donors-us' => 'Chcą uczestniczyć:',
    'was-donors-us' => 'Uczestniczyli:',


];
