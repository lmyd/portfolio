
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia strony z wspierajacymi PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Wspierają nas',
    'description' => 'Sławni ludzi naszej ukochanej dyscypliny, którzy nam pomagają!',
];
