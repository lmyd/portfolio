<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title' => 'Logowanie',
    'description' => 'Zapraszamy do panelu systemu.',
    'email' => 'E-mail',
    'password' => 'Hasło',
    'login' => 'Zaloguj się',
    'forgot' => 'Zapomniałeś lub nie znasz hasla?',

    'failed' => 'Brak podanego maila w naszej bazie.',
    'throttle' => 'Za dużo prób logowania. Proszę spróbować za :seconds sekund.',

];
