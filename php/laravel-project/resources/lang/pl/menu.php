
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia głównego menu aplikacji PL
    |--------------------------------------------------------------------------
    */

    'actions' => 'Akcje',
    'proteges' => 'Podopieczni',
    'donors' => 'Donatorzy',
    'supporting' => 'Wspierający',
    'sponsors' => 'Sponsorzy',
    'login' => 'Logowanie',
    'logout' => 'Wyloguj się',
    'dashboard' => 'Dashboard',
    'regulations' => 'Regulamin',
    'my-profile' => 'Mój profil',
    'edit-profile' => 'Edycja profilu',
    '' => '',
];
