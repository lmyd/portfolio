
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia dashboard PL
    |--------------------------------------------------------------------------
    */

    'title' => 'Dashboard',
    'description' => 'Panel systemowy',
    'my-donations' => 'Moje donacje:',
    'my-actions' => 'Chcę uczestniczyć:',
    'given_day' => 'Data',
    'protege' => 'Podopieczny',
    'action' => 'Akcja',
    'type' => 'Typ',
    'amount' => 'Ilość',
    'blood' => 'Pełna krew',
    'make-donation' => 'Zgłoś donację:',
    'make-donation-btn' => 'Zgłoś',
    'non-action' => 'Bez akcji',
    'donation-was-added' => 'Dziękujemy, za oddanie krwi. Walkę mamy we krwi!',
    'form-error-userId-required' => 'Donator jest wymagany.',
    'form-error-protegeId-required' => 'Protegowany jest wumagany.',
    'form-error-typeId-required' => 'Typ donacji jest wymagany.',
    'form-error-amount-required' => 'Ilość oddanej krwi jest wymagana.',
    'form-error-date-required' => 'Data donacji jest wymagana.',
    'form-error-userId-exists' => 'Brak donatora w bazie.',
    'form-error-actionId-exists' => 'Brak akcji w bazie.',
    'form-error-protegeId-exists' => 'Brak protegowanego w bazie.',
    'form-error-typeId-exists' => 'Brak typu donacji w bazie.',
    'form-error-date-exists' => 'Błędna data donacji.',
    'form-error-amount-integer' => 'Ilość oddanej krwi musi być liczbą całkowitą.',
    'form-error-donation-unique' =>'Informacja o takiej donacji istnieje już w naszej bazie.',
    'form-error-date-before_or_equal' => 'Data donacji nie może być z przyszłości.',
    'user-accepted-regulations' => 'Dziękujemy za zaakceptowanie regulaminu.',
    'best-three' => 'Pierwsza trójka dawców:',
    'place' => 'Miejsce',
    'place-1' => 'Miejsce pierwsze',
    'place-2' => 'Miejsce drugie',
    'place-3' => 'Miejsce trzecie',
    'place-color-1' => 'gold',
    'place-color-2' => 'silver',
    'place-color-3' => 'brown',
];
