
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tłumaczenia głównego menu aplikacji EN
    |--------------------------------------------------------------------------
    */

    'actions' => 'Actions',
    'proteges' => 'Proteges',
    'donors' => 'Donors',
    'supporting' => 'Supporting',
    'sponsors' => 'Sponsors',
    'login' => 'Log in',
    'logout' => 'Log out',
    'dashboard' => 'Dashboard',
    'my-profile' => 'My profile',
    'edit-profile' => 'Profile edition',
    '' => '',
];
