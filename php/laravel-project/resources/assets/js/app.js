
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.axios = require('axios');
window.moment = require('moment');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('notification', require('./components/Notification.vue'));
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('fa-icon', require('./components/FaIcon.vue'));

Vue.component('two-columns', require('./components/TwoColumns.vue'));
Vue.component('countdown', require('./components/Countdown.vue'));

Vue.component('login-form', require('./auth/LoginForm.vue'));
Vue.component('logout-form', require('./auth/LogoutForm.vue'));
Vue.component('register-form', require('./auth/RegisterForm.vue'));
Vue.component('forgot-form', require('./auth/ForgotForm.vue'));
Vue.component('social', require('./auth/Social.vue'));

Vue.component('payu', require('./components/PayU.vue'));


Vue.component('accept-regulations-form', require('./components/forms/AcceptRegulationsForm.vue'));
Vue.component('donation-form', require('./components/forms/DonationForm.vue'));
Vue.component('user-form', require('./components/forms/UserForm.vue'));

Vue.component('join-action', require('./components/JoinAction.vue'));

const app = new Vue({
    el: '#app',
    data: {
        toogleNavbarBurger: false,
    },
    created: function(){

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.11";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    },
    mounted: function(){
        var that = this;

        if(that.$refs.sponsorsCarouselNext){
            that.slideSponsorsCarousel();
        }
        if(that.$refs.actionCarouselNext){
            that.slideActionCarousel();
        }
    },
    methods: {
        slideSponsorsCarousel: function() {
            var that = this;
            setTimeout(function(){
                that.$refs.sponsorsCarouselNext.dispatchEvent(new Event('click'))
                that.slideSponsorsCarousel();
            }, 10000);
        },
        slideActionCarousel: function() {
            var that = this;
            setTimeout(function(){
                that.$refs.actionCarouselNext.dispatchEvent(new Event('click'))
                that.slideActionCarousel();
            }, 10000);
        },
        goto: function (url) {
            document.location=url;
        },
    }
});

