<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{

    const SESSION_KEY = 'locale';

    protected $languages = ['pl','en'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has(self::SESSION_KEY)){
            session()->put(self::SESSION_KEY, $request->getPreferredLanguage($this->languages));
        }

        $locale = session()->get(self::SESSION_KEY);

        app()->setLocale($locale);
        setlocale(LC_TIME, $locale, $locale . '_' . strtolower($locale));

        return $next($request);
    }
}
