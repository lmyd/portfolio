<?php

namespace App\Http\Controllers;

use App\Services\DonorsService;
use Illuminate\Http\Request;

class Donors extends Controller
{
    /** @var DonorsService */
    protected $donorsService;

    public function __construct()
    {
        $this->donorsService = app()->make(DonorsService::class);
    }

    public function index()
    {
        $data = [
            'donors' => $this->donorsService->getListArray(),
        ];

        return view('donors.index', $data);
    }


}
