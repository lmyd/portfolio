<?php

namespace App\Http\Controllers;

use App\Services\SupportingsService;
use Illuminate\Http\Request;

class Supporting extends Controller
{
    public function index(Request $request)
    {
        /** @var SupportingsService $supportingsService */
        $supportingsService = app()->make(SupportingsService::class);

        $data = [
            'supporting' => $supportingsService->getListArray()
        ];

        return view('supporting.index', $data);
    }
}
