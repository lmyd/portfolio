<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserProfile;
use App\Services\DonorsService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Profile extends Controller
{
    /** @var UserService */
    protected $userService;

    /** @var DonorsService */
    protected $donorsService;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->userService = app()->make(UserService::class);
        $this->donorsService = app()->make(DonorsService::class);
    }

    /**
     * Show the user edit form.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data = [
            'donor' => $this->donorsService->getData(Auth::user()),
            'userForm' => $this->userService->getFormData(Auth::user()),
        ];

        return view('profile.edit', $data);
    }

    public function update(UpdateUserProfile $request)
    {
        $this->userService->updateProfile($request->all());
        return redirect(route('editProfile'))->with('statusOk', 'user-was-updated');
    }

    public function accept()
    {
        $this->userService->acceptRegulations(Auth::user());
        return redirect(route('dashboard'))->with('statusOk', 'user-accepted-regulations');
    }
}
