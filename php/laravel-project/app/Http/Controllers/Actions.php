<?php

namespace App\Http\Controllers;

use App\Services\ActionsService;
use App\Services\DonorsService;
use App\Services\SponsorsService;
use App\Services\SupportingsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Actions extends Controller
{
    /** @var ActionsService */
    protected $actionsService;

    /** @var SupportingsService */
    protected $supportingsService;

    /** @var SponsorsService */
    protected $sponsorsService;

    /** @var DonorsService */
    protected $donorsService;

    public function __construct()
    {
        $this->actionsService = app()->make(ActionsService::class);
        $this->supportingsService = app()->make(SupportingsService::class);
        $this->sponsorsService = app()->make(SponsorsService::class);
        $this->donorsService = app()->make(DonorsService::class);
    }

    public function index(Request $request)
    {
        $data = [
            'singleActionPage' => false,
            'actions' => $this->actionsService->getListArray(),
        ];

        return view('actions.index', $data);
    }

    public function show(Request $request, $slug)
    {
        $data = [
            'singleActionPage' => true,
            'action' => $this->actionsService->getOneArray($slug),
            'supportings' => $this->supportingsService->getListForAction($slug),
            'sponsors' => $this->sponsorsService->getListForAction($slug),
            'donors' => $this->donorsService->getListForAction($slug),
        ];

        return view('actions.show', $data);
    }

    public function join(Request $request, $slug)
    {
//        if(!$request->ajax()){
//            return abort(404);
//        }

        return response()->json(
            ['joined' => $this->actionsService->joinAction($slug, Auth::user(), $request->post('change', false))]
        );
    }
}
