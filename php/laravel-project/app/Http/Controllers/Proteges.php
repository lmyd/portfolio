<?php

namespace App\Http\Controllers;

use App\Services\ActionsService;
use App\Services\DonorsService;
use App\Services\ProtegesService;
use Illuminate\Http\Request;

class Proteges extends Controller
{
    /** @var ProtegesService */
    protected $protegesService;

    /** @var ActionsService */
    protected $actionsService;

    /** @var DonorsService */
    protected $donorsService;

    public function __construct()
    {
        $this->protegesService = app()->make(ProtegesService::class);
        $this->actionsService = app()->make(ActionsService::class);
        $this->donorsService = app()->make(DonorsService::class);
    }

    public function index(Request $request)
    {
        $data = [
            'singleProtegePage' => false,
            'proteges' => $this->protegesService->getListArray(),
        ];

        return view('proteges.index', $data);
    }

    public function show(Request $request, $slug)
    {
        $data = [
            'singleProtegePage' => true,
            'singleActionPage' => false,
            'protege' => $this->protegesService->getOneArray($slug),
            'news' => $this->protegesService->getNewsArray($slug),
            'actions' => $this->actionsService->getListForProtege($slug),
            'donors' => $this->donorsService->getListForProtege($slug)
        ];

        return view('proteges.show', $data);
    }
}
