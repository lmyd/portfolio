<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayU;
use App\Models\PayuDonation;
use App\Services\ActionsService;
use App\Services\LocalizationService;
use App\Services\SupportingsService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Main extends Controller
{
    public function index(Request $request, $locale = null)
    {
        /** @var LocalizationService */
        app()->make(LocalizationService::class)->change($locale);

        /** @var ActionsService $actionsService */
        $actionsService = app()->make(ActionsService::class);

        /** @var SupportingsService $supportingsService */
        $supportingsService = app()->make(SupportingsService::class);

        $data = [
            'actions' => $actionsService->getCarouselArray(3),
            'supporting' => $supportingsService->getListArray(4)
        ];

        return view('main.index', $data);
    }

    public function payuSend(PayU $request)
    {
        $donation = PayuDonation::create(['ammount' => $request->get('ammount')]);

        //set Production Environment
        \OpenPayU_Configuration::setEnvironment('secure');

        //set POS ID and Second MD5 Key (from merchant admin panel)
        \OpenPayU_Configuration::setMerchantPosId('575957');
        \OpenPayU_Configuration::setSignatureKey('c152b9ce49ab108be6cde708086e30f4');

        //set Oauth Client Id and Oauth Client Secret (from merchant admin panel)
        \OpenPayU_Configuration::setOauthClientId('575957');
        \OpenPayU_Configuration::setOauthClientSecret('c607156152d37fbfbfce58df074da860');

        $order['continueUrl'] = route('payuRecive', ['donationId' => $donation->id]); //customer will be redirected to this page after successfull payment
        $order['notifyUrl'] = route('main');
        $order['customerIp'] = $_SERVER['REMOTE_ADDR'];
        $order['merchantPosId'] = \OpenPayU_Configuration::getMerchantPosId();
        $order['description'] = 'Nowa darowizna';
        $order['currencyCode'] = 'PLN';
        $order['totalAmount'] = $donation->ammount;
        $order['extOrderId'] = $donation->created_at->timestamp; //must be unique!

        $order['products'][0]['name'] = 'Dotacja';
        $order['products'][0]['unitPrice'] = 1000;
        $order['products'][0]['quantity'] = 1;

        $response = \OpenPayU_Order::create($order);

        return redirect($response->getResponse()->redirectUri); //You must redirect your client to PayU payment summary page.
    }

    public function payuRecive(Request $request, $donationId)
    {
        /** @var PayuDonation $donation */
        $donation = PayuDonation::findOrFail($donationId);
        $donation->confirmed_at = Carbon::now();
        $donation->save();

        return redirect(route('main'))->with(self::STATUS_SUCCESS, 'Dziękujemy za darowiznę. Walkę mamy we krwi!');
    }

}
