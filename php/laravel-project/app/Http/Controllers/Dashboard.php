<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDonation;
use App\Services\ActionsService;
use App\Services\DonationsService;
use App\Services\DonorsService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class Dashboard extends Controller
{
    /** @var UserService */
    protected $userService;

    /** @var ActionsService */
    protected $actionsService;

    /** @var DonationsService */
    protected $donationsService;

    /** @var DonorsService */
    protected $donorsService;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->userService = app()->make(UserService::class);
        $this->actionsService = app()->make(ActionsService::class);
        $this->donationsService = app()->make(DonationsService::class);
        $this->donorsService = app()->make(DonorsService::class);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'singleActionPage' => false,
            'waitingActions' => $this->actionsService->getWaitingListForDonor(Auth::user()),
            'donations' => $this->userService->getDonationsList(),
            'donationForm' => $this->donationsService->getFormData(Auth::user()),
            'userForm' => $this->userService->getFormData(Auth::user()),
            'bestDonors' => $this->donorsService->getBest(3),
        ];

        return view('dashboard.index', $data);
    }

    public function makeDonation(StoreDonation $request)
    {
        $this->donationsService->createDonation($request->all());
        return redirect('dashboard')->with('statusOk', 'donation-was-added');
    }
}
