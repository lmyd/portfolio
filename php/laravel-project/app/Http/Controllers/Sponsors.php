<?php

namespace App\Http\Controllers;

use App\Services\SponsorsService;
use Illuminate\Http\Request;

class Sponsors extends Controller
{
    /** @var SponsorsService */
    protected $sponsorsService;

    public function __construct()
    {
        $this->sponsorsService = app()->make(SponsorsService::class);
    }

    public function index(Request $request)
    {
        $data = [
            'sponsors' => $this->sponsorsService->getListArray(),
        ];

        return view('sponsors.index', $data);
    }
}
