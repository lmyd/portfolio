<?php

namespace App\Http\Requests;

class StoreDonation extends AppFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->prepareUserId();
        return [
            'userHash' => 'required|exists:users,id',
            'actionId' => 'nullable|exists:actions,id|unique:donations,action_id,null,id,user_id,'.$this->get('userHash'),
            'protegeId' => 'required|exists:proteges,id',
            'typeId' => 'required|exists:donation_types,id',
            'amount' => 'required|integer',
            'date' => 'required|date_format:Y-m-d|before_or_equal:'.date('Y-m-d').'|unique:donations,given_day,null,id,user_id,'.$this->get('userHash'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'userId.required' => 'form-error-userId-required',
            'protegeId.required' => 'form-error-protegeId-required',
            'typeId.required' => 'form-error-typeId-required',
            'amount.required' => 'form-error-amount-required',
            'date.required' => 'form-error-date-required',
            'userId.exists' => 'form-error-userId-exists',
            'actionId.exists' => 'form-error-actionId-exists',
            'protegeId.exists' => 'form-error-protegeId-exists',
            'typeId.exists' => 'form-error-typeId-exists',
            'date.date_format' => 'form-error-date-exists',
            'amount.integer' => 'form-error-amount-integer',
            'actionId.unique' =>  'form-error-donation-unique',
            'date.unique' =>  'form-error-donation-unique',
            'date.before_or_equal' => 'form-error-date-before_or_equal'
        ];
    }
}
