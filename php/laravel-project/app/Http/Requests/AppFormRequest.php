<?php

namespace App\Http\Requests;

use App\Services\ConvertService;
use Illuminate\Foundation\Http\FormRequest;

abstract class AppFormRequest extends FormRequest
{
    protected function prepareUserId()
    {
        $all = $this->all();
        $userHash = $this->get('userHash');
        if(!is_null($userHash)){
            /** @var ConvertService $service */
            $service = app()->make(ConvertService::class);
            $all['userHash'] = $service->hexToStr($userHash);
        }
        $this->replace($all);
    }
}
