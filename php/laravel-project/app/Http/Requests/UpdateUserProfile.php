<?php

namespace App\Http\Requests;

use Carbon\Carbon;

class UpdateUserProfile extends AppFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->prepareUserId();
        return [
            'userHash' => 'required|exists:users,id',
            'login' => 'nullable|min:5',
            'password' => 'nullable|min:5|confirmed',
            'date' => 'nullable|date_format:Y-m-d|before_or_equal:'.Carbon::now()->subYear(18)->format('Y-m-d'),
            'gender' => 'nullable|in:male,female',
            'typeId' => 'required|exists:blood_types,id',
            'fotoFile' => 'nullable|mimes:jpeg,jpg,bmp,png,gif',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.min' => 'form-error-login-min',
            'password.min' => 'form-error-password-min',
            'password.confirmed' => 'form-error-password-confirmed',
            'date.date_format' => 'form-error-date-format',
            'date.before_or_equal' => 'form-error-date-before_or_equal',
            'gender.in' => 'form-error-gender-in',
            'typeId.required' => 'form-error-typeId-required',
            'typeId.exists' => 'form-error-typeId-exists',
            'fotoFile.mimes' => 'form-error-fotoFile-mimes',
        ];
    }
}
