<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProtegeNews extends Model
{
    public function protege()
    {
        return $this->belongsTo(Protege::class);
    }
}
