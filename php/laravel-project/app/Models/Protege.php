<?php

namespace App\Models;

use App\Models\Traits\Slug;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Protege extends Model
{
    use Slug;

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }

    public function bloodType()
    {
        return $this->belongsTo(BloodType::class);
    }

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    public function donations()
    {
        return $this->hasMany(Donation::class, 'protege_id');
    }

    public function news()
    {
        return $this->hasMany(ProtegeNews::class);
    }

    public function donorsCount()
    {
        return $this->donations()->select('user_id')->groupBy('user_id')->get()->count();
    }

    public function donors()
    {
        return $this->hasManyThrough(User::class, Donation::class, 'protege_id', 'id', 'id', 'user_id');
    }
}
