<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PayuDonation
 * @package App\Models
 *
 * @param integer $id
 * @param integer $ammount
 * @param Carbon $confirmed_at
 *
 */
class PayuDonation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ammount', 'confirmed_at'];

    protected $dates = ['confirmed_at'];
}
