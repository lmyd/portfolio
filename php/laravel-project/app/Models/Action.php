<?php

namespace App\Models;

use App\Models\Traits\Slug;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use Slug;

    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime'
    ];

    public function promoter()
    {
        return $this->belongsTo(Promoter::class, 'promoter_id');
    }

    public function protege()
    {
        return $this->belongsTo(Protege::class, 'protege_id');
    }

    public function sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'actions_has_sponsors', 'action_id', 'sponsor_id')->withPivot('created_at');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'actions_has_users', 'action_id', 'user_id')->withPivot('created_at');
    }

    public function donations()
    {
        return $this->hasMany(Donation::class, 'action_id');
    }

    public function donors()
    {
        return $this->hasManyThrough(User::class, Donation::class, 'action_id', 'id', 'id', 'user_id');
    }

    public function getStatus()
    {
        $now = Carbon::now();

        if($this->end < $now)
            return config('actions.status.was');
        elseif($this->start > $now)
            return config('actions.status.wait');

        return config('actions.status.continue');
    }

    public function isWaiting()
    {
        return $this->getStatus() === config('actions.status.wait');
    }
}
