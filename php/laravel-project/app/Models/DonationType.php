<?php

namespace App\Models;

use App\Models\Traits\Slug;
use Illuminate\Database\Eloquent\Model;

class DonationType extends Model
{
    use Slug;

    public $timestamps = false;
}
