<?php
namespace App\Models\Traits;

trait Slug
{
    public static function findBySlug($slug)
    {
        return self::where('slug', $slug)->first();
    }

    public static function findBySlugOrFail($slug)
    {
        return self::where('slug', $slug)->firstOrFail();
    }
}
