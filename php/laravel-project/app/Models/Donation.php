<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'action_id', 'type_id', 'protege_id', 'given_day', 'amount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(DonationType::class);
    }

    public function protege()
    {
        return $this->belongsTo(Protege::class);
    }

    public function action()
    {
        return $this->belongsTo(Action::class);
    }
}
