<?php

namespace App\Models;

use App\Models\Traits\Slug;
use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    use Slug;

    public $timestamps = false;
}
