<?php

namespace App\Models;

use App\Models\Traits\Slug;
use Illuminate\Database\Eloquent\Model;

class Promoter extends Model
{
    use Slug;

    public $timestamps = false;

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

}
