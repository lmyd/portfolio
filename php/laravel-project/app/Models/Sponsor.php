<?php

namespace App\Models;

use App\Models\Traits\Slug;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use Slug;

    public function actions()
    {
        return $this->belongsToMany(Action::class, 'actions_has_sponsors', 'sponsor_id', 'action_id')->withPivot('created_at');
    }
}
