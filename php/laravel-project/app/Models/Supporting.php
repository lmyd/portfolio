<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supporting extends Model
{
    public function action()
    {
        return $this->belongsTo(Action::class, 'action_id');
    }
}
