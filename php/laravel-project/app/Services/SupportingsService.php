<?php

namespace App\Services;


use App\Models\Supporting;

class SupportingsService extends Service
{
    protected function getData(Supporting $supporting)
    {
        return [
            'title' => $supporting->title,
            'subtitle' => $supporting->subtitle,
            'description' => $supporting->description,
            'video' => $supporting->video,
        ];
    }

    public function getListArray($limit = null)
    {
        $response = [];

        $supportings = Supporting::orderBy('id', 'desc');

        if(!is_null($limit)){
            $supportings->take($limit);
        }

        $supportings = $supportings->get();

        foreach ($supportings as $supporting){
            $response[] = $this->getData($supporting);
        }

        return $response;
    }

    public function getListForAction($actionSlug)
    {
        $response = [];

        $supportings = Supporting::whereHas('action', function ($query) use ($actionSlug){
            $query->where('slug', $actionSlug);
        })->orderBy('created_at', 'desc')->get();

        foreach ($supportings as $supporting){
            $response[] = $this->getData($supporting);
        }

        return $response;
    }
}
