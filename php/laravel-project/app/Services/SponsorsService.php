<?php

namespace App\Services;

use App\Models\Action;
use App\Models\Sponsor;
use Illuminate\Support\Facades\Storage;

class SponsorsService extends Service
{
    protected function getData(Sponsor $sponsor, $tn = false)
    {
        return [
            'slug' => $sponsor->slug,
            'title' => $sponsor->title,
            'img' => Storage::url( 'sponsors/'. ($tn?'tn-':'') . $sponsor->slug . '.png'),
            'description' => $sponsor->description,
            'www' => $sponsor->www,
        ];
    }

    public function getListArray($limit = null, $tn = false)
    {
        $response = [];

        $sponsors = Sponsor::orderBy('priority', 'asc');

        if(!is_null($limit)){
            $sponsors->take($limit);
        }

        $sponsors = $sponsors->get();

        foreach ($sponsors as $sponsor){
            $response[] = $this->getData($sponsor, $tn);
        }

        return $response;
    }

    public function getListForAction($actionSlug)
    {
        $response = [];

        $sponsors = Action::findBySlugOrFail($actionSlug)->sponsors()->get();

        foreach ($sponsors as $sponsor){
            $response[] = $this->getData($sponsor);
        }

        return $response;
    }
}
