<?php

namespace App\Services;

use App\Models\Protege;
use Illuminate\Support\Facades\Storage;

class ProtegesService extends Service
{
    protected $tagsService;

    protected $donationsService;

    public function __construct(TagsForCardService $tagsForCardService, DonationsService $donationsService)
    {
        $this->tagsService = $tagsForCardService;
        $this->donationsService = $donationsService;
    }

    protected function getData(Protege $protege)
    {
        $response = [
            'slug' => $protege->slug,
            'title' => $protege->title,
            'img' => Storage::url( 'proteges/' . $protege->slug . '.jpg'),
            'hospital' => $protege->hospital->title,
            'description' => $protege->description,
            'passAway' => $protege->deathday !== null,
            'passAwayDate' => $protege->deathday,
            'tags' => $this->tagsService
                ->setBloodType($protege->bloodType->title)
                ->setBloodCount($this->donationsService->sumForProtege($protege))
                ->setDonorsCount($protege->donorsCount())
                ->setActionsCount($protege->actions->count())
                ->toArray()
        ];

        return $response;
    }

    public function getListArray()
    {
        $response = [];

        $proteges = Protege::with(['hospital', 'bloodType'])->orderBy('id', 'desc')->get();

        foreach ($proteges as $protege){
            $response[] = $this->getData($protege);
        }

        return $response;
    }

    public function  getOneArray($slug)
    {
        $protege = Protege::findBySlugOrFail($slug);
        return $this->getData($protege);
    }

    public function getNewsArray($slug)
    {
        $protege = Protege::findBySlugOrFail($slug);
        return $protege->news()->select('article', 'created_at')->orderby('created_at', 'desc')->get()->toArray();
    }
}
