<?php

namespace App\Services;


class TagsForCardService extends Service
{
    const TAG_BLOOD_TYPE = 'blood-type';
    const TAG_BLOOD_COUNT = 'blood-count';
    const TAG_DONORS_COUNT = 'donors-count';
    const TAG_ACTIONS_COUNT = 'actions-count';

    protected $tags = [
        self::TAG_BLOOD_TYPE,
        self::TAG_BLOOD_COUNT,
        self::TAG_DONORS_COUNT,
        self::TAG_ACTIONS_COUNT,
    ];

    protected $tooltips = [
        self::TAG_BLOOD_TYPE => 'tags4card.blood-type',
        self::TAG_BLOOD_COUNT => 'tags4card.blood-count',
        self::TAG_DONORS_COUNT => 'tags4card.donors-count',
        self::TAG_ACTIONS_COUNT => 'tags4card.actions-count',
    ];

    protected $icons = [
        self::TAG_BLOOD_TYPE => 'fa-tint',
        self::TAG_BLOOD_COUNT => 'fa-heartbeat',
        self::TAG_DONORS_COUNT => 'fa-users',
        self::TAG_ACTIONS_COUNT => 'fa-ambulance',
    ];

    protected $bgs = [
        self::TAG_BLOOD_TYPE => 'is-link',
        self::TAG_BLOOD_COUNT => 'is-primary',
        self::TAG_DONORS_COUNT => 'is-warning',
        self::TAG_ACTIONS_COUNT => 'is-info',
    ];

    protected $data = [];

    /** @var DonationsService */
    protected $donationsService;

    public function __construct(DonationsService $donationsService)
    {
        $this->donationsService = $donationsService;
    }

    protected function setTag($tag, $value, $tooltip = null, $icon = null, $bg = null)
    {
        if(in_array($tag, $this->tags)){
            $this->data[$tag]['value'] = $value;
            $this->data[$tag]['tooltip'] = is_null($tooltip) ? $this->tooltips[$tag] : $tooltip;
            $this->data[$tag]['icon'] = is_null($icon) ? $this->icons[$tag] : $icon;
            $this->data[$tag]['bg'] = is_null($bg) ? $this->bgs[$tag] : $bg;
        }
        return $this;
    }

    public function setBloodType($value, $tooltip = null, $icon = null, $bg = null)
    {
        return $this->setTag(self::TAG_BLOOD_TYPE, $value, $tooltip, $icon, $bg);
    }

    public function setBloodCount($value, $tooltip = null, $icon = null, $bg = null)
    {
        $value = $this->donationsService->format($value);
        return $this->setTag(self::TAG_BLOOD_COUNT, $value, $tooltip, $icon, $bg);
    }

    public function setDonorsCount($value, $tooltip = null, $icon = null, $bg = null)
    {
        return $this->setTag(self::TAG_DONORS_COUNT, $value, $tooltip, $icon, $bg);
    }

    public function setActionsCount($value, $tooltip = null, $icon = null, $bg = null)
    {
        return $this->setTag(self::TAG_ACTIONS_COUNT, $value, $tooltip, $icon, $bg);
    }

    public function toArray()
    {
        $result = [];
        foreach ($this->tags as $tag){
            if(array_key_exists($tag, $this->data)){
                $result[$tag] = $this->data[$tag];
            }
        }
        $this->data = [];
        return $result;
    }
}
