<?php

namespace App\Services;


use App\Models\Action;
use App\Models\Protege;
use App\User;

class StatsService extends Service
{
    protected $donationsService;

    public function __construct(DonationsService $donationsService)
    {
        $this->donationsService = $donationsService;
    }

    public function getActionsCount()
    {
        return Action::count();
    }

    public function getProtegesCount()
    {
        return Protege::count();
    }

    public function getDonorsCount()
    {
        return User::count();
    }

    public function getBloodCount()
    {
        return $this->donationsService->sumForAll(true);
    }
}
