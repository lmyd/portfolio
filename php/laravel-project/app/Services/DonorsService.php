<?php

namespace App\Services;


use App\Models\Action;
use App\Models\Protege;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DonorsService extends Service
{
    protected $tagsService;

    protected $donationsService;

    public function __construct(TagsForCardService $tagsForCardService, DonationsService $donationsService)
    {
        $this->tagsService = $tagsForCardService;
        $this->donationsService = $donationsService;
    }

    public function getData(User $donor)
    {
        $hash = $donor->getHash();
        $title = $donor->isAcceptRegulations() && !is_null($donor->name) ? $donor->name : __('donors.anonymous');
        $pseudoSlug = '@' . implode('-', explode(' ', mb_strtolower($title)));

        $data = [
            'hash' => $hash,
            'title' => $title,
            'pseudoSlug' => $pseudoSlug,
            'img' => Storage::exists( 'public/donors/' . $hash . '.jpg') ? Storage::url( 'donors/' . $hash . '.jpg') : '/img/logo-hero.png',
            'acceptRegulations' => $donor->isAcceptRegulations()
        ];

        if($donor->isAcceptRegulations()){
            $tags = $this->tagsService
                ->setBloodCount($this->donationsService->sumForDonor($donor), 'donors.tag-blood')
                ->setDonorsCount($donor->protegesCount(), 'donors.tag-donors')
                ->setActionsCount($donor->actions()->count(), 'donors.tag-actions');

            if($donor->bloodType){
                $tags->setBloodType($donor->bloodType->title, 'donors.tag-blood-type');
            }

            $data['tags'] = $tags->toArray();
        }

        return $data;
    }

    protected function buildArray($donors)
    {
        $me = [];
        $response = [];

        foreach ($donors as $donor){
            if(Auth::check() && Auth::id() == $donor->id){
                $me[$donor->id] = $this->getData($donor);
            }else{
                if(!array_key_exists($donor->id, $response))
                    $response[$donor->id] = $this->getData($donor);
            }
        }

        /**
         * grupowanie dawcow anonimowych
         */
        $i = 0;
        $tmp = array_merge($me, $response);
        $response = [];
        foreach ($tmp as $row){
            if($row['title'] !== __('donors.anonymous')){
                $response[] = $row;
            }else{
                $i++;
            }
        }

        if($i > 0) {
            $response[] = [
                'hash' => '',
                'title' => trans_choice('donors.many_anonymous', $i, ['COUNT' => $i]),
                'pseudoSlug' => '',
                'img' => '/img/logo-hero.png',
            ];
        }

        return $response;
    }

    public function getListArray($limit = null)
    {
        $donors = User::with('bloodType')->orderBy('name', 'asc');

        if(!is_null($limit)){
            $donors->take($limit);
        }

        return $this->buildArray($donors->get());
    }

    public function getListForAction($actionSlug)
    {
        /** @var Action $action */
        $action = Action::findBySlugOrFail($actionSlug);

        if ($action->isWaiting()){
            $donors = $action->users()->get();

            if(Auth::check() && is_null($donors->where('id', Auth::id())->first())){
                $donors->push(Auth::user());
            }
        }else{
            $donors = $action->donors()->orderBy('donations.created_at', 'desc')->get();
        }

        return $this->buildArray($donors);
    }

    public function getListForProtege($protegeSlug)
    {
        /** @var Protege $protege */
        $protege = Protege::findBySlugOrFail($protegeSlug);

        return $this->buildArray($protege->donors);
    }

    public function getBest($limit)
    {
        $donors = collect($this->getListArray())->sortByDesc('tags.blood-count')->values();

        $top = $donors->take($limit);

        if(Auth::check()){
            $logged = $donors->where('hash', Auth::user()->getHash());
            //$logged = $donors->where('hash', app()->make(ConvertService::class)->strToHex(10));
            if($logged->count()){
                $top->put($logged->keys()[0], $logged->first());
            }
        }

        return $top->toArray();
    }
}
