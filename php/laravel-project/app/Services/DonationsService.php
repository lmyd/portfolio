<?php

namespace App\Services;

use App\Models\Action;
use App\Models\Donation;
use App\Models\DonationType;
use App\Models\Protege;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DonationsService extends Service
{
    const TYPE_FULL_BLOOD = 'krew-pelna';
    const TYPE_PLASMA = 'osocze';
    const TYPE_PLATELETS = 'plytki-krwi';

    protected function sum(Collection $donations, $withFormat)
    {
        $summary = 0;
        foreach ($donations as $donation){
            $summary += $this->recalculate($donation->type->slug, $donation->amount);
        }
        return $withFormat ? $this->format($summary) : $summary;
    }

    public function sumForAction(Action $action, $withFormat = false)
    {
        return $this->sum($action->donations()->with('type')->get(), $withFormat);
    }

    public function sumForProtege(Protege $protege, $withFormat = false)
    {
        return $this->sum($protege->donations()->with('type')->get(), $withFormat);
    }

    public function sumForDonor(User $donor, $withFormat = false)
    {
        return $this->sum($donor->donations()->with('type')->get(), $withFormat);
    }

    public function sumForAll($withFormat = false)
    {
        return $this->sum(Donation::with('type')->get(), $withFormat);
    }

    public function recalculate($type, $value, $withFormat = false)
    {
        if($type == self::TYPE_PLASMA) $value = $value / 3;

        if($type == self::TYPE_PLATELETS) $value = $value * 2;

        return $withFormat ? $this->format($value) : $value;
    }

    public function format($value)
    {
        if($value < 1000)
            return  $value . ' ml';

        return $value % 1000 == 0 ? $value/1000 . ' l' : number_format($value/1000, 3, ' l ', ' ') . ' ml';
    }

    public function getFormData(User $user)
    {
        $selected = true;
        $actions = [];
        $actionDb = Action::where('start', '<', date('Y-m-d'))->orderby('start', 'desc')->get();

        $proteges = Protege::select('id', 'title as label', DB::raw('false as selected'))->whereNull('deathday')->orderby('title')->get();

        foreach ($actionDb as $row){
            if($proteges->where('id', $row->protege_id)->count()){
                if($selected) {
                    $actions[] = [
                        'id' => '',
                        'label' => __('dashboard.non-action'),
                        'selected' => false,
                        'protegeId' => $row->protege_id,
                    ];
                }
                $actions[] = [
                    'id' => $row->id,
                    'label' => $row->title,
                    'selected' => $selected,
                    'protegeId' => $row->protege_id,
                    'start' => $row->start->format('Y-m-d'),
                    'end' => $row->end->format('Y-m-d')
                ];
                $selected = false;
            }
        }

        $proteges = $proteges->toArray();

        $types = DonationType::select('id', 'title as label', DB::raw('false as selected'), 'default_amount as default')->orderby('title')->get()->toArray();

        $max = 0;
        foreach($types as $type)
            if($type['default'] > $max)
                $max = $type['default'];

        $amounts = [];

        for($i = 50; $i <= $max; $i = $i+50)
            $amounts[] = [
                'id' => $i,
                'label' => $this->format($i),
                'selected' => false,
            ];

        return [
            'label' => __('dashboard.make-donation-btn'),
            'actions-label' => __('dashboard.action'),
            'proteges-label' => __('dashboard.protege'),
            'types-label' => __('dashboard.type'),
            'amounts-label' => __('dashboard.amount'),
            'date-label' => __('dashboard.given_day'),
            'modal-title' => __('dashboard.make-donation'),
            'user-hash' => $user->getHash(),
            'actions' => $actions,
            'proteges' => $proteges,
            'types' => $types,
            'amounts' => $amounts
        ];
    }

    public function createDonation(array $data)
    {
        $createData = [
            'user_id' => $data['userHash'],
            'type_id' => $data['typeId'],
            'protege_id' => $data['protegeId'],
            'given_day' => $data['date'],
            'amount' => $data['amount'],
        ];
        if(!is_null($data['actionId'])){
            $action = Action::findOrFail($data['actionId']);
            $createData['action_id'] = $action->id;
            $createData['protege_id'] = $action->protege_id;
        }
        return Donation::create($createData);
    }
}
