<?php

namespace App\Services;

use App\Models\Action;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ActionsService extends Service
{
    protected $tagsService;

    protected $donationsService;

    public function __construct(TagsForCardService $tagsForCardService, DonationsService $donationsService)
    {
        $this->tagsService = $tagsForCardService;
        $this->donationsService = $donationsService;
    }

    public function getCarouselArray($limit = 3)
    {
        $response = [];
        //$actions = Action::where('end', '>', date('Y-m-d'))->take($limit)->orderBy('start', 'asc')->get();
        $actions = Action::orderBy('start', 'desc')->get();
        foreach ($actions as $action){
            $response[] = [
                'slug' => $action->slug,
                'title' => $action->title_carousel,
                'img' => Storage::url( 'actions/' . $action->slug . '.png')
            ];
        }

        return $response;
    }

    protected function getData(Action $action)
    {
        $now = Carbon::now();

        $this->tagsService->setBloodType($action->protege->bloodType->title);

        if($action->isWaiting()){
            $this->tagsService->setDonorsCount($action->users()->count(), 'tags4card.donors-count-will-be');
        }else{
            $this->tagsService
                ->setBloodCount($this->donationsService->sumForAction($action))
                ->setDonorsCount($action->donors->count());
        }

        $response = [
            'slug' => $action->slug,
            'status' => $action->getStatus(),
            'title' => $action->title,
            'img' => Storage::url( 'actions/' . $action->slug . '.png'),
            'datetime' => sprintf(
                '%s %s %s %s %s',
                $action->start->formatLocalized('%d %b %Y'),
                __('actions.from'),
                $action->start->formatLocalized('%H:%S'),
                __('actions.to'),
                $action->end->formatLocalized('%H:%S')
            ),
            'promotor' => Storage::url( 'promoters/' . $action->promoter->slug . '.png'),
            'protege' => $action->protege->slug,
            'place' => $action->place,
            'address' => $action->address,
            'city' => $action->post_code . ' ' . $action->city,
            'description' => $action->description,
            'tags' => $this->tagsService->toArray(),
        ];

        if($action->end > $now){
            $response['countdown'] = [
                'current' => $now->format('Y-m-d H:i:s'),
                'start' => $action->start->format('Y-m-d H:i:s'),
                'end' => $action->end->format('Y-m-d H:i:s'),
            ];
        }

        return $response;
    }

    public function getListArray()
    {
        $response = [];

        $actions = Action::with('protege')->orderBy('start', 'desc')->get();

        foreach ($actions as $action){
            $response[] = $this->getData($action);
        }

        return $response;
    }

    public function getOneArray($slug)
    {
        $action = Action::findBySlugOrFail($slug);
        return $this->getData($action);
    }

    public function getListForProtege($protegeSlug)
    {
        $response = [];
        $actions = Action::whereHas('protege', function ($query) use ($protegeSlug){
            $query->where('slug', $protegeSlug);
        })->orderBy('start', 'desc')->get();

        foreach ($actions as $action){
            $response[] = $this->getData($action);
        }

        return $response;
    }

    public function getWaitingListForDonor(User $donor)
    {
        $response = [];

        foreach ($donor->waitingActions as $action){
            $response[] = $this->getData($action);
        }

        return $response;
    }

    public function joinAction($slug, User $user, $withChange = false)
    {
        $action = Action::findBySlugOrFail($slug);
        $relation = $action->users()->where('user_id', $user->id)->get();
        $hasRelation = count($relation) > 0;

        if($withChange){
            if($hasRelation){
                $action->users()->detach($user);
                $hasRelation = false;
            }else{
                $action->users()->attach($user);
                $hasRelation = true;
            }
        }

        return $hasRelation;
    }
}
