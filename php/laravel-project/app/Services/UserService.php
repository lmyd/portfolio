<?php

namespace App\Services;

use App\Models\BloodType;
use App\Models\Donation;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserService extends Service
{
    /** @var DonationsService */
    protected $donationsService;

    public function __construct(DonationsService $donationsService)
    {
        $this->donationsService = $donationsService;
    }

    public function getDonationsList()
    {
        $response = [];

        $donations = Auth::user()->donations()->with(['protege', 'action', 'type'])->orderBy('given_day', 'desc')->get();

        $sumAmount = 0;
        $sumBlood = 0;

        /** @var Donation $donation */
        foreach($donations as $donation) {
            $row = [
                'given_day' => $donation->given_day,
                'protege' => [
                    'slug' => $donation->protege->slug,
                    'title' => $donation->protege->title
                ],
                'type' => $donation->type->title,
                'amount' => $this->donationsService->format($donation->amount),
                'blood' => $this->donationsService->recalculate($donation->type->slug, $donation->amount, true)
            ];

            if($donation->action){
                $row['action'] = [
                    'slug' => $donation->action->slug,
                    'title' => $donation->action->title
                ];
            }

            $response['data'][] = $row;

            $sumAmount += $donation->amount;
            $sumBlood += $this->donationsService->recalculate($donation->type->slug, $donation->amount);
        }

        $response['summary']['amount'] = $this->donationsService->format($sumAmount);
        $response['summary']['blood'] = $this->donationsService->format($sumBlood);

        return $response;
    }

    public function getGenders($selected = null)
    {
        return [
            ['id' => '', 'label' => '', 'selected' => $selected == ''],
            ['id' => 'female', 'label' => __('app.female'), 'selected' => $selected == 'female'],
            ['id' => 'male', 'label' => __('app.male'), 'selected' => $selected == 'male'],
        ];
    }

    public function getFormData(User $user)
    {
        $types = BloodType::select('id', 'title as label')->orderby('id')->get();

        $types->each(function($item) use ($user) {
            $item->selected = $item->id == $user->blood_type_id;
        });

        return [
            'label' => __('profile.update-user-btn'),
            'modal-title' => __('profile.update-user'),
            'user-hash' => $user->getHash(),
            'login' => $user->name,
            'login-label' => __('profile.login'),
            'email' => $user->email,
            'email-label' => __('profile.email'),
            'password-label' => __('profile.password'),
            'password-placeholder' => __('profile.password-placeholder'),
            'password2-label' => __('profile.password2'),
            'password2-placeholder' => __('profile.password2-placeholder'),
            'regulations' => $user->accept_regulations,
            'regulations-label' => __('profile.regulations-date'),
            'date' => $user->birthday,
            'date-label' => __('profile.birthday'),
            'genders' => $this->getGenders($user->gender),
            'genders-label' => __('app.gender'),
            'types-label' => __('profile.type'),
            'types' => $types->toArray(),
            'file-label' => __('profile.foto-file'),
            'delete-file-label' => __('profile.delete-foto-file'),
        ];
    }

    public function updateProfile(array $data)
    {
        $user = User::findOrFail($data['userHash']);
        $user->name = $data['login'];
        $user->birthday = $data['date'];
        $user->gender = $data['gender'];
        $user->blood_type_id = $data['typeId'];
        if($data['password'] !== null)
            $user->password = bcrypt($data['password']);
        $user->save();

        $fotoPaths = 'public/donors/' . $user->getHash() . '.jpg';

        if(array_key_exists('fotoFile', $data)){
            $img = Image::make($data['fotoFile'])->fit(128)->stream('jpg', 80); //->save('/storage/donors/'.$user->getHash().'.jpg');
            Storage::put($fotoPaths, $img, 'public');
        }

        if(array_key_exists('deleteFotoFile', $data)){
            Storage::delete($fotoPaths);
        }

        return $user;
    }

    public function acceptRegulations(User $user)
    {
        $user->accept_regulations = Carbon::now();
        $user->save();
        return $user;
    }
}
