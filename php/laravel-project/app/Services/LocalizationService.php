<?php

namespace App\Services;

use App\Http\Middleware\Localization;

class LocalizationService extends Service
{

    public function change($locale)
    {
        if($locale !== null){
            session()->put(Localization::SESSION_KEY, $locale);
            app()->setLocale($locale);
        }
    }

}
