<?php

namespace App\Services;


class ConvertService extends Service
{
    public function strToHex($string)
    {
        $hex = '';
        $string = base64_encode($string);
        for($i = 0; $i < strlen($string); $i++){
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }

    public function hexToStr($hex)
    {
        $string = '';
        for($i = 0; $i < strlen($hex) - 1; $i += 2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return base64_decode($string);
    }
}
