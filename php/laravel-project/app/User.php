<?php

namespace App;

use App\Models\Action;
use App\Models\BloodType;
use App\Models\Donation;
use App\Models\Protege;
use App\Services\ConvertService;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function bloodType()
    {
        return $this->belongsTo(BloodType::class);
    }

    public function donations()
    {
        return $this->hasMany(Donation::class);
    }

    public function actions()
    {
        return $this->hasManyThrough(Action::class, Donation::class, 'user_id', 'id', 'id', 'action_id');
    }

    public function proteges()
    {
        return $this->hasManyThrough(Protege::class, Donation::class, 'user_id', 'id', 'id', 'protege_id');
    }

    public function protegesCount()
    {
        return $this->donations()->select('protege_id')->groupBy('protege_id')->get()->count();
    }

    public function waitingActions()
    {
        return $this->belongsToMany(Action::class, 'actions_has_users', 'user_id', 'action_id')->withPivot('created_at');
    }

    /**
     * @return boolean
     */
    public function isAcceptRegulations()
    {
        return $this->accept_regulations !== null && $this->accept_regulations <= Carbon::now();
    }

    public function getHash()
    {
        return app()->make(ConvertService::class)->strToHex($this->id);
    }
}
