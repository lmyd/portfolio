<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('protege_id')->unsigned();
            $table->integer('promoter_id')->unsigned();
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('title_carousel');
            $table->text('description');
            $table->datetime('start');
            $table->datetime('end');
            $table->string('place');
            $table->string('address');
            $table->string('post_code');
            $table->string('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
