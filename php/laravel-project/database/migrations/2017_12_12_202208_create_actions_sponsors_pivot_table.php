<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsSponsorsPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions_has_sponsors', function (Blueprint $table) {
            $table->integer('action_id')->unsigned();
            $table->integer('sponsor_id')->unsigned();
            $table->timestamp('created_at');
            $table->primary(['action_id','sponsor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions_has_sponsors');
    }
}
