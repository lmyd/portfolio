<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Models\ProtegeNews::class, function (Faker $faker) {
    return [
        'protege_id' => \App\Models\Protege::inRandomOrder()->first()->id,
        'article' => $faker->text('500'),
        'created_at' => $faker->dateTime(),
    ];
});
