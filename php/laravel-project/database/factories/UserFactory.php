<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    $gender = $faker->randomElement(['male', 'female']);

    return [
        'name' => $faker->randomElement([null, $faker->name($gender)]),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),

        'accept_regulations' => $faker->randomElement([null, $faker->dateTime]),
        'birthday' => $faker->date,
        'gender' => $gender,
        'blood_type_id' => $faker->randomElement([1,2,3,4,5,6,7,8]),
    ];
});
