<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Models\Donation::class, function (Faker $faker) {

    $donationType = \App\Models\DonationType::inRandomOrder()->first();
    $protege = \App\Models\Protege::inRandomOrder()->first();
    if ($donationType->id == 1) {
        $action = \App\Models\Action::where('protege_id', $protege->id)->inRandomOrder()->first();
        $actionId = $action ? $action->id : null;
    }else{
        $actionId = null;
    }

    if(!is_null($actionId)){
        $donationsUsersIds = \App\Models\Donation::where('action_id', $actionId)->get()->pluck('user_id');
        $userId = \App\User::inRandomOrder()->whereNotIn('id', $donationsUsersIds)->first()->id;
    }else{
        $userId = \App\User::inRandomOrder()->first()->id;
    }

    while(true){
        $date = $faker->dateTimeBetween('-3 years');
        $donation = \App\Models\Donation::where('given_day', $date)->where('user_id', $userId)->first();
        if(is_null($donation)){
            break;
        }
    }

    return [
        'user_id' => $userId,
        'type_id' => $donationType->id,
        'protege_id' => $protege->id,
        'action_id' => $actionId,
        'given_day' => $date,
        'amount' => $donationType->default_amount,
    ];
});
