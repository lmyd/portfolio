<?php

use Illuminate\Database\Seeder;

class SupportingsTableSeeder extends Seeder
{
    protected $data = [
        [
            'action' => 'i-liga-wom-funny-judo-2017',
            'title' => 'Tomasz \'Krulik\' Adamiec',
            'subtitle' => 'Olimpijczyk. Medalista mistrzostw Polski i Europy',
            'description' => 'Medalista Mistrzostw Europy , medalista pucharów Świata i Europy, sześciokrotny mistrz Polski oraz jak sam o sobie mowi....serfer:) Zaprasza i zachęca Państwa do szlachetnej walki z Rakiem. Tomek juz wielokrotnie upuszczał swojej "sportowej benzyny" i jak widac nie wplynelo to negatywnie na jego kondycje fizyczna :)',
            'video' => 'https://www.facebook.com/958212147647495/videos/962270030575040/',
        ],[
            'action' => 'i-liga-wom-funny-judo-2017',
            'title' => 'Filip Pacholak',
            'subtitle' => 'Multimedalista zawodów dziecięcych.',
            'description' => 'Dziecku odmówisz? :)',
            'video' => 'https://www.facebook.com/958212147647495/videos/964483923686984/',
        ],[
            'action' => 'judo-chojrak-cup-2017',
            'title' => 'Paweł Grzegorz Zagrodnik',
            'subtitle' => 'Olimpijczyk. Medalista mistrzostw Polski i Europy',
            'description' => 'Paweł zaprasza na naszą najbliższą akcje podczas turnieju organizowanego przez Klub Judo Chojrak juz 18 marca.',
            'video' => 'https://www.facebook.com/958212147647495/videos/976912832444093/',
        ],[
            'action' => 'judo-chojrak-cup-2017',
            'title' => 'Jarosław Furmaniuk',
            'subtitle' => 'Prezes Klub Judo Chojrak',
            'description' => 'Prezes Klub Judo Chojrak zaprasza na Turniej Malego Mistrza , który odbędzie się 18 Marca na ulicy Obozowej 60 w Hali "koło". Zbieramy krew dla Szymonka Wódkiewicza.',
            'video' => 'https://www.facebook.com/958212147647495/videos/990349321100444/',
        ],[
            'action' => 'judo-legia-cup-2017',
            'title' => 'Krzysztof Wiłkomirski',
            'subtitle' => 'Olimpijczyk. Medalista mistrzostw Polski, Europy i Świata',
            'description' => 'Zaproszenie od samego Krzysztofa Wiłkomirskiego Prezesa Judo Legia Warszawa , podwójnego Olimpijczyka, brązowego medalisty Mistrzostw Świata i wielokrotnego Mistrza Polski! Wielcy sportowcy nas wspieraja wiec musi byc dobrze!!! Do zobaczenia jutro!',
            'video' => 'https://www.facebook.com/958212147647495/videos/1001712359964140/',
        ],[
            'action' => 'same-judo-cup-2',
            'title' => 'Arleta Podolak',
            'subtitle' => 'Mistrzyni Świata juniorek',
            'description' => 'Mistrzyni Świata juniorek zaprasza na naszą najbliższa akcje zbiórki krwi w Markach 08.04 ulica Duża 3. Pobijemy rekord 25L 650ml? Zależy od liczebności naszych dawców :) zapraszamy!',
            'video' => 'https://www.facebook.com/958212147647495/videos/1004587086343334/',
        ],[
            'action' => 'judo-kowala-cup-2017',
            'title' => 'Dariusz Bulski',
            'subtitle' => 'Zastępca Wójta Gminy Kowala',
            'description' => 'Zastępca Wójta Gminy Kowala Pan Dariusz Bulski zaprasza na Akcje , która odbędzie sie 16 wrzesnia na Terenie gminy :)',
            'video' => 'https://www.facebook.com/958212147647495/videos/1114750445326997/',
        ],[
            'action' => 'judo-lemur-cup-2017',
            'title' => 'Artur Gorzelak',
            'subtitle' => 'Aktywny sportowiec amator. Od 20 lat trenuje judo (1 DAN).',
            'description' => 'W Japonii zakochał się od pierwszego wejrzenia, dlatego „W judodze na rowerze” postanowił przyjrzeć się jej bardzo dokładnie, przemierzając Kraj Kwitnącej Wiśni na dwóch kółkach i trenując judo w lokalnych klubach oraz promując działanie Fundacji kilkanaście tysięcy kilometrów od Polski. Artur regularnie dołącza do akcji zbiórki krwi organizowanych przez naszą fundację.',
            'video' => 'https://www.facebook.com/958212147647495/videos/1129597110508997/',
        ],[
            'action' => 'judo-lemur-cup-2017',
            'title' => 'Agata Ozdoba-Błach',
            'subtitle' => 'Medalistka Mistrzostw Świata',
            'description' => 'Zaprasza na kolejną akcję Judo - Sport Walki z Rakiem. Agata to świeżo upieczona brązowa medalistka Mistrzostw Świata w judo! Zapraszamy!',
            'video' => 'https://www.facebook.com/958212147647495/videos/1136978596437515/',
        ],[
            'action' => 'judo-tuliszkow-2018',
            'title' => 'Grzegorz Ciesielski i Michał Gaj',
            'subtitle' => 'żródło: KNTV24',
            'description' => 'Zapraszają na VII edycję Ogólnopolskiego Turnieju Judo w Tuliszkowie.',
            'video' => 'https://www.facebook.com/kntv24/videos/172177890102576/UzpfSTk1ODIxMjE0NzY0NzQ5NToxMjMyMzQzNDk2OTAxMDI0',
        ],[
            'action' => 'pokonujemy-granice-2019',
            'title' => 'Patryk Wawrzyczek',
            'subtitle' => 'Mistrz Polski 2018, Medalista Mistrzostw Świata i Europy',
            'description' => 'Zaprasza na VIII Międzynarodowy Turniej JUDO Dla Dzieci i Młodzieży "Pokonujemy Granice".',
            'video' => 'https://www.facebook.com/958212147647495/videos/495143444310720/',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supportings')->truncate();
        foreach($this->data as $row){
            $action = \App\Models\Action::findBySlugOrFail($row['action']);
            $row['action_id'] = $action->id;
            unset($row['action']);
            \App\Models\Supporting::create($row);
        }
    }
}
