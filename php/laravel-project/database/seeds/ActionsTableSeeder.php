<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    protected $anonymousCount = 0;

    protected $data = [
        [
            'protege' => 'lenka-wzorek',
            'promoter' => 'judo-legia-warszawa',
            'slug' => 'judo-legia-cup-2016',
            'title' => 'Judo Legia Cup 2016',
            'title_carousel' => 'Walczymy dla Olafa podczas Judo Legia Cup 2016',
            'description' => 'Walczymy dla Olafa podczas Judo Legia Cup 2016',
            'start' => '2016-04-02 10:00:00',
            'end' => '2016-04-02 14:00:00',
            'place' => 'OSiR Bemowo',
            'address' => 'ul. Obrońcy Tobruku 40',
            'post_code' => '01-494',
            'city' => 'Warszawa',
            'blood'=> 17100,
            'donnors' => [UsersTableSeeder::MAIL_1],
        ],[
            'protege' => 'zosia-galazka',
            'promoter' => 'judo-lemur',
            'slug' => 'judo-lemur-cup-2016',
            'title' => 'Judo Lemur Cup 2016',
            'title_carousel' => 'Walczymy dla Igora podczas III Turnieju Judo o Puchar Burmistrza Białołęki',
            'description' => 'Walczymy dla Igora podczas III Turnieju Judo o Puchar Burmistrza Białołęki, poświęconego pamięci trenera Sławomira Pacholaka. Coroczny turniej organizowany po raz kolejny jest przez Klub Judo Lemur.',
            'start' => '2016-11-05 09:00:00',
            'end' => '2016-11-05 14:00:00',
            'place' => 'Białołęcki Ośrodek Sportu',
            'address' => 'ul. Strumykowa 21',
            'post_code' => '03-138',
            'city' => 'Warszawa',
            'blood'=> 25650,
            'donnors' => [UsersTableSeeder::MAIL_1, UsersTableSeeder::MAIL_2],
        ],[
            'protege' => 'natalia-kopowicz',
            'promoter' => 'wmz-judo',
            'slug' => 'i-liga-wom-funny-judo-2017',
            'title' => 'I Liga WOM Funny Judo 2017',
            'title_carousel' => 'Walczymy dla Natalki podczas I Ligi Warszawskiej Olimpiady Maluchów Funny Judo',
            'description' => 'Walczymy dla Natalki podczas I Ligi Warszawskiej Olimpiady Maluchów Funny Judo, organizowanego przez Warszawsko-Mazowiecki Związek Judo',
            'start' => '2017-02-05 09:00:00',
            'end' => '2017-02-05 14:00:00',
            'place' => 'Centrum Edukacji i Sportu',
            'address' => 'ul. Kwiatowa 28',
            'post_code' => '05-515',
            'city' => 'Warszawa',
            'blood'=> 12150,
            'donnors' => [],
        ],[
            'protege' => 'szymek-wodkiewicz',
            'promoter' => 'judo-chojrak',
            'slug' => 'judo-chojrak-cup-2017',
            'title' => 'Judo Chojrak Cup 2017',
            'title_carousel' => 'Walczymy dla Szymka podczas Turnieju Małego Mistrza',
            'description' => 'Walczymy dla Szymka podczas Turnieju Małego Mistrza, organizowanego przez Klub Judo Chojrak',
            'start' => '2017-03-18 09:00:00',
            'end' => '2017-03-18 14:00:00',
            'place' => 'OSiR Koło',
            'address' => 'ul. Obozowa 60',
            'post_code' => '01-423',
            'city' => 'Warszawa',
            'blood'=> 18000,
            'donnors' => [UsersTableSeeder::MAIL_1],
        ],[
            'protege' => 'olaf-piskorczyk',
            'promoter' => 'judo-legia-warszawa',
            'slug' => 'judo-legia-cup-2017',
            'title' => 'Judo Legia Cup 2017',
            'title_carousel' => 'Walczymy dla Olafa podczas Judo Legia Cup 2017',
            'description' => 'Walczymy dla Olafa podczas Judo Legia Cup 2017',
            'start' => '2017-04-01 09:00:00',
            'end' => '2017-04-01 14:00:00',
            'place' => 'OSiR Bemowo',
            'address' => 'ul. Obrońcy Tobruku 40',
            'post_code' => '01-494',
            'city' => 'Warszawa',
            'blood'=> 20250,
            'donnors' => [],
        ],[
            'protege' => 'wojtek-ryszewski',
            'promoter' => 'same-judo',
            'slug' => 'same-judo-cup-2',
            'title' => 'SameJudo Cup 2',
            'title_carousel' => 'Walczymy dla Wojtka podczas Międzynarodowych Mistrzostw Marek w Judo',
            'description' => 'Walczymy dla Wojtka podczas Międzynarodowych Mistrzostw Marek w Judo, turnieju organizowanego przez Same Judo Klub',
            'start' => '2017-04-08 09:00:00',
            'end' => '2017-04-08 14:00:00',
            'place' => 'Szkoła Podstawowa Nr 4',
            'address' => 'ul. Duża 3',
            'post_code' => '05-260',
            'city' => 'Marki',
            'blood'=> 17100,
            'donnors' => [],
        ],[
            'protege' => 'igor-zadlak',
            'promoter' => 'judo-kowala',
            'slug' => 'judo-kowala-cup-2017',
            'title' => 'Ogólnopolski Turniej Dzieci i Młodzików w Judo',
            'title_carousel' => 'Walczymy dla Igora podczas Ogólnopolskiego Turnieju Dzieci i Młodzików w Judo',
            'description' => 'Walczymy dla Igora podczas Ogólnopolskiego Turnieju Dzieci i Młodzików w Judo pod patronatem Starosty Radomskiego',
            'start' => '2017-09-16 09:00:00',
            'end' => '2017-09-16 14:00:00',
            'place' => 'Publiczna Szkoła Podstawowa im. Janusza Korczaka w Kowali',
            'address' => 'Kowala 103',
            'post_code' => '26-624',
            'city' => 'Kowala',
            'blood'=> 5400,
            'donnors' => [],
        ],[
            'protege' => 'igor-zadlak',
            'promoter' => 'judo-lemur',
            'slug' => 'judo-lemur-cup-2017',
            'title' => 'Judo Lemur Cup 2017',
            'title_carousel' => 'Walczymy dla Igora podczas IV Turnieju Judo o Puchar Burmistrza Białołęki',
            'description' => 'Walczymy dla Igora podczas IV Turnieju Judo o Puchar Burmistrza Białołęki, poświęconego pamięci trenera Sławomira Pacholaka. Coroczny turniej organizowany po raz kolejny jest przez Klub Judo Lemur.',
            'start' => '2017-11-04 10:00:00',
            'end' => '2017-11-04 15:00:00',
            'place' => 'Białołęcki Ośrodek Sportu',
            'address' => 'ul. Strumykowa 21',
            'post_code' => '03-138',
            'city' => 'Warszawa',
            'blood'=> 27450,
            'donnors' => [],
        ],[
            'protege' => 'stefan-kaminski',
            'promoter' => 'ikizama-judo',
            'slug' => 'ikizama-cup-2018',
            'title' => 'Ikizama Cup 2018',
            'title_carousel' => 'Walczymy dla Stefka podczas I Turnieju Judo o Puchar Burmistrza Piaseczna',
            'description' => 'Walczymy dla Stefka podczas I Turnieju Judo o Puchar Burmistrza Piaseczna organizowanego przez Klub Ikizama Judo Piaseczno.',
            'start' => '2018-02-10 09:00:00',
            'end' => '2018-02-10 14:00:00',
            'place' => 'Szkoła Podstawowa Nr 5',
            'address' => 'ul. Szkolna 14',
            'post_code' => '05-500',
            'city' => 'Piaseczno',
            'blood'=> 17100,
            'donnors' => [
                UsersTableSeeder::MAIL_1,
                UsersTableSeeder::MAIL_2,
                UsersTableSeeder::MAIL_3,
                UsersTableSeeder::MAIL_4,
                UsersTableSeeder::MAIL_5,
                UsersTableSeeder::MAIL_6,
                UsersTableSeeder::MAIL_7,
                UsersTableSeeder::MAIL_8,
                UsersTableSeeder::MAIL_9,
                UsersTableSeeder::MAIL_10,
                UsersTableSeeder::MAIL_11,
                UsersTableSeeder::MAIL_12,
                UsersTableSeeder::MAIL_13,
                UsersTableSeeder::MAIL_14,
                UsersTableSeeder::MAIL_15,
                UsersTableSeeder::MAIL_16,
                UsersTableSeeder::MAIL_17,
                UsersTableSeeder::MAIL_18,
                UsersTableSeeder::MAIL_19,
            ],
        ],[
            'protege' => 'dawid-wisniewski',
            'promoter' => 'same-judo',
            'slug' => 'same-judo-cup-3',
            'title' => 'SameJudo Cup 3',
            'title_carousel' => 'Walczymy dla Dawida podczas Międzynarodowych Mistrzostw Marek w Judo',
            'description' => 'Walczymy dla Dawida podczas Międzynarodowych Mistrzostw Marek w Judo, turnieju organizowanego przez Same Judo Klub',
            'start' => '2018-03-25 09:00:00',
            'end' => '2018-03-25 14:00:00',
            'place' => 'Szkoła Podstawowa Nr 4',
            'address' => 'ul. Duża 3',
            'post_code' => '05-260',
            'city' => 'Marki',
            'blood'=> 19350,
            'donnors' => [],
        ],[
            'protege' => 'mikolaj-domagala',
            'promoter' => 'judo-tuliszkow',
            'slug' => 'judo-tuliszkow-2018',
            'title' => 'Ogólnopolski Turniej Judo',
            'title_carousel' => 'Walczymy dla Mikołaja podczas Ogólnopolskiego Turnieju Judo z okazji 100-lecia Odzyskania Niepodległości Polski.',
            'description' => 'Walczymy dla Mikołaja podczas Ogólnopolskiego Turnieju Judo z okazji 100-lecia Odzyskania Niepodległości Polski. Gościem specjalnym imprezy będzie dwukrotny mistrz świata w judo Rafał Kubacki. Pan Rafał to duży człowiek mamy nadzieje, że pozbędzie się 450ml swojej krwi w słusznej sprawie , w końcu krew od Mistrza Świata na pewno szybko postawi Mikiego na nogi.',
            'start' => '2018-04-08 09:00:00',
            'end' => '2018-04-08 14:00:00',
            'place' => 'Hala sportowa',
            'address' => 'ul. Nortowska 1',
            'post_code' => '62-740',
            'city' => 'Tuliszków',
            'blood'=> 13050,
            'donnors' => [],
        ],[
            'protege' => 'amelia-stawska',
            'promoter' => 'judo-lemur',
            'slug' => 'judo-lemur-cup-2018',
            'title' => 'Judo Lemur Cup 2010',
            'title_carousel' => 'Walczymy dla Amelki podczas V Turnieju Judo o Puchar Burmistrza Białołęki',
            'description' => 'Walczymy dla Amelki podczas V Turnieju Judo o Puchar Burmistrza Białołęki, poświęconego pamięci trenera Sławomira Pacholaka. Coroczny turniej organizowany po raz kolejny jest przez Klub Judo Lemur.<br><br>Podczas turnieju odbędzie się kwesta, z której dochód przeznaczony zostanie na leczenie Amelki <strong>nierefundowaną w Polsce immunoterapią</strong>.',
            'start' => '2018-11-03 11:00:00',
            'end' => '2018-11-03 16:00:00',
            'place' => 'Białołęcki Ośrodek Sportu',
            'address' => 'ul. Strumykowa 21',
            'post_code' => '03-138',
            'city' => 'Warszawa',
            'blood'=> 16650,
            'donnors' => [UsersTableSeeder::MAIL_2],
        ],[
            'protege' => 'alan-mikolajczyk',
            'promoter' => 'pts-janosik',
            'slug' => 'pokonujemy-granice-2019',
            'title' => 'VIII Turniej "Pokonujemy Granice"',
            'title_carousel' => 'Walczymy dla Alana podczas VIII Międzynarodowego Turnieju JUDO Dla Dzieci i Młodzieży "Pokonujemy Granice"',
            'description' => 'Walczymy dla Alana podczas VIII Międzynarodowego Turnieju JUDO Dla Dzieci i Młodzieży "Pokonujemy Granice". Coroczny turniej organizowany po raz kolejny jest przez Klub PTS Janosik.<br><br>Podczas turnieju odbędzie się kwesta, z której dochód przeznaczony zostanie na <strong>leczenie Alana</strong>.',
            'start' => '2019-01-12 09:00:00',
            'end' => '2019-01-12 14:00:00',
            'place' => 'Hala widowiskowo-sportowa pod Dębowcem',
            'address' => 'ul. Karbowa 26',
            'post_code' => '43-300',
            'city' => 'Bielsko-Biała',
            'blood'=> 17100,
            'donnors' => [UsersTableSeeder::MAIL_2],
        ],[
            'protege' => 'kacper-gajewski',
            'promoter' => 'same-judo',
            'slug' => 'same-judo-cup-4',
            'title' => 'SameJudo Cup 4',
            'title_carousel' => 'Walczymy dla Kacpra podczas Międzynarodowych Mistrzostw Marek w Judo',
            'description' => 'Walczymy dla Kacpra podczas Międzynarodowych Mistrzostw Marek w Judo, turnieju organizowanego przez Same Judo Klub',
            'start' => '2019-03-23 10:00:00',
            'end' => '2019-03-23 15:00:00',
            'place' => 'Szkoła Podstawowa Nr 4',
            'address' => 'ul. Duża 3',
            'post_code' => '05-260',
            'city' => 'Marki',
            'blood'=> 16650,
            'donnors' => [],
        ],[
            'protege' => 'jas-karwowski',
            'promoter' => 'judo-legia-warszawa',
            'slug' => 'judo-legia-cup-2019',
            'title' => 'Judo Legia Cup 2019',
            'title_carousel' => 'Walczymy dla Jasia podczas Judo Legia Cup 2019',
            'description' => 'Walczymy dla Jasia podczas Judo Legia Cup 2019',
            'start' => '2019-04-06 10:00:00',
            'end' => '2019-04-06 14:00:00',
            'place' => 'OSiR Bemowo',
            'address' => 'ul. Obrońcy Tobruku 40',
            'post_code' => '01-494',
            'city' => 'Warszawa',
            'blood'=> 17550,
            'donnors' => [],
        ],[
            'protege' => 'alan-hibental',
            'promoter' => 'uks-feniks-bytom',
            'slug' => 'ijl-szansa-dla-kazdego-bytom-2019',
            'title' => 'International Judo League "Szansą dla Każdego" - Bytom 2019',
            'title_carousel' => 'Walczymy dla Alana podczas zawodów IJL Szansą dla Każdego Bytom 2019',
            'description' => 'Walczymy dla Alana podczas zawodów IJL Szansą dla Każdego Bytom 2019',
            'start' => '2019-04-13 10:00:00',
            'end' => '2019-04-13 14:00:00',
            'place' => 'Hala na skarpie',
            'address' => 'ul. Frycza-Modrzejewskiego 5',
            'post_code' => '41-907',
            'city' => 'Bytom',
            'blood'=> 0,
            'donnors' => [],
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->truncate();
        DB::table('donations')->truncate();
        foreach($this->data as $row){
            $protege = \App\Models\Protege::findBySlugOrFail($row['protege']);
            $row['protege_id'] = $protege->id;
            unset($row['protege']);
            $promoter = \App\Models\Promoter::findBySlugOrFail($row['promoter']);
            $row['promoter_id'] = $promoter->id;
            unset($row['promoter']);
            $blood = $row['blood'];
            unset($row['blood']);
            $donnors = $row['donnors'];
            unset($row['donnors']);
            $action = \App\Models\Action::create($row);
            $this->donations($action, $blood, $donnors);
        }
    }

    protected function donations(\App\Models\Action $action, $blood, array $donnors)
    {
        if($blood <= 0) return;

        $donationType = \App\Models\DonationType::findBySlugOrFail('krew-pelna');

        $donationsCount = $blood / $donationType->default_amount;

        for($i=0; $i<$donationsCount; $i++){
            if($i < count($donnors)){
                $user = \App\User::where('email', $donnors[$i])->firstOrFail();
            }else{
                $this->anonymousCount++;
                $user = \App\User::create(['email' => sprintf('anonymous%s@judosportwalkizrakiem.pl', $this->anonymousCount), 'password' => bcrypt(microtime())]);
            }
            \App\Models\Donation::create([
                'user_id' => $user->id,
                'type_id' => $donationType->id,
                'protege_id' => $action->protege_id,
                'action_id' => $action->id,
                'given_day' => $action->start->format('Y-m-d'),
                'amount' => $donationType->default_amount
            ]);
        }

    }
}
