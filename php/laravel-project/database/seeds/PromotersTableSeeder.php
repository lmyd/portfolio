<?php

use Illuminate\Database\Seeder;

class PromotersTableSeeder extends Seeder
{
    protected $data = [
        ['slug' => 'fundacja-judo-sport-walki-z-rakiem', 'title' => 'Fundacja JUDO - Sport walki z rakiem'],
        ['slug' => 'wmz-judo', 'title' => 'Warszawsko Mazowiecki Związek Judo', 'www' => 'http://www.wmzjudo.pl'],
        ['slug' => 'judo-lemur', 'title' => 'UKS Judo Lemur', 'www' => 'https://www.judo-lemur.pl'],
        ['slug' => 'judo-legia-warszawa', 'title' => 'Judo Legia Warszawa', 'www' => 'https://judo.legia.com'],
        ['slug' => 'same-judo', 'title' => 'UKS Same Judo', 'www' => 'http://www.samejudo.pl'],
        ['slug' => 'judo-chojrak', 'title' => 'UKS Judo Chojrak', 'www' => 'http://judo-chojrak.pl'],
        ['slug' => 'judo-kowala', 'title' => 'ULKS Judo Kowala', 'www' => 'https://pl-pl.facebook.com/ulksjudokowala'],
        ['slug' => 'ikizama-judo', 'title' => 'Ikizama Judo Klub', 'www' => 'http://judopiaseczno.pl'],
        ['slug' => 'judo-tuliszkow', 'title' => 'UKS Judo Tuliszków', 'www' => 'http://www.judotuliszkow.pl'],
        ['slug' => 'pts-janosik', 'title' => 'PTS Janosik', 'www' => 'http://ptsjanosik.pl'],
        ['slug' => 'uks-feniks-bytom', 'title' => 'UKS Feniks Bytom', 'www' => 'https://feniks.bytom.pl'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promoters')->truncate();
        foreach($this->data as $row){
            \App\Models\Promoter::create($row);
        }
    }
}
