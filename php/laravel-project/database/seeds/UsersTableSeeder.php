<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    const MAIL_1 = 'pacholakmateusz@gmail.com';
    const MAIL_2 = 'lmydlarski@gmail.com';
    const MAIL_3 = 'andrzej.dudek.pl@gmail.com';
    const MAIL_4 = 'apilipczuk@wp.pl';
    const MAIL_5 = 'leszek.nowak.77@onet.pl';
    const MAIL_6 = 'karolkw@gmail.com';
    const MAIL_7 = 'marek.dryjanski@gmail.com';
    const MAIL_8 = 'zbigniew.jasiak@poczta.fm';
    const MAIL_9 = 'tobiasz_baranowski@interia.pl';
    const MAIL_10 = 'monikatomaszek1@op.pl';
    const MAIL_11 = 'pawelgrosss@gmail.com';
    const MAIL_12 = 'cherry113@interia.pl';
    const MAIL_13 = 'ttomaszek@icloud.com';
    const MAIL_14 = 'matgruszczynski@gmail.com';
    const MAIL_15 = 'wieslaw113@op.pl';
    const MAIL_16 = 'msoliwoda@gmail.com';
    const MAIL_17 = 'pawelswidwa@poczta.onet.pl';
    const MAIL_18 = 'wisniewb1977@gmail.com';
    const MAIL_19 = 'tomadamiec@gmail.com';

    protected $data = [
        [
            'name' => 'Administrator',
            'email' => 'admin@judosportwalkizrakiem.pl',
            'password' => 'judo2018',
        ],[
            'name' => 'Mateusz Pacholak',
            'email' => self::MAIL_1,
            'password' => 'judo2018',
            'accept_regulations' => '2018-02-15 09:23:45',
            'birthday' => '1987-11-12',
            'gender' => 'male',
            'blood' => 'abrh+',
        ],[
            'name' => 'Łukasz Mydlarski',
            'email' => self::MAIL_2,
            'password' => 'judo2018',
            'accept_regulations' => '2017-12-01 09:23:45',
            'birthday' => '1980-11-10',
            'gender' => 'male',
            'blood' => '0rh+',
        ],[
            'name' => 'Tomasz Adamiec',
            'email' => self::MAIL_19,
            'password' => 'judo2018',
            'accept_regulations' => '2018-02-15 09:23:45',
//            'birthday' => '1980-11-10',
//            'gender' => 'male',
//            'blood' => '0rh+',
        ],[
            'email' => self::MAIL_3,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_4,
            'gender' => 'female',
        ],[
            'email' => self::MAIL_5,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_6,
        ],[
            'email' => self::MAIL_7,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_8,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_9,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_10,
            'gender' => 'female',
        ],[
            'email' => self::MAIL_11,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_12,
        ],[
            'email' => self::MAIL_13,
        ],[
            'email' => self::MAIL_14,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_15,
        ],[
            'email' => self::MAIL_16,
        ],[
            'email' => self::MAIL_17,
            'gender' => 'male',
        ],[
            'email' => self::MAIL_18,
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        foreach($this->data as $row){
            if(array_key_exists('blood', $row)){
                $blood = \App\Models\BloodType::findBySlugOrFail($row['blood']);
                $row['blood_type_id'] = $blood->id;
                unset($row['blood']);
            }
            $row['password'] = isset($row['password']) ? bcrypt($row['password']) : bcrypt('gdybykozkanieksakalatobymialanogi');
            App\User::create($row);
        }

        //factory(App\User::class, 50)->create();
    }
}
