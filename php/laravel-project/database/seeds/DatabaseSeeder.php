<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BloodTypesTableSeeder::class);
        $this->call(DonationTypesTableSeeder::class);
        $this->call(HospitalsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PromotersTableSeeder::class);
        $this->call(ProtegesTableSeeder::class);
        $this->call(ActionsTableSeeder::class);
        $this->call(SponsorsTableSeeder::class);
        $this->call(ActionsHasSponsorsTableSeeder::class);
        $this->call(SupportingsTableSeeder::class);
        $this->call(DonationsTableSeeder::class);
        $this->call(ProtegeNewsTableSeeder::class);
        $this->call(ActionsHasUsersTableSeeder::class);
    }
}
