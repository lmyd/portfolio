<?php

use Illuminate\Database\Seeder;

class SponsorsTableSeeder extends Seeder
{
    protected $data = [
        [
            'slug' => 'gross-catering',
            'title' => 'Gross Catering',
            'description' => 'Atutem naszej firmy jest paroletnia współpraca z głównie firmami budowlanymi, warsztatami.<br><br>Zajmujemy się głównie dostarczaniem posiłków dla ludzi fizycznie pracujących, także w różnych warunkach atmosferycznych.<br><br>W naszej ofercie możemy zaproponować smaczne, świeże posiłki zawierające dużą zawartość energetyczną i regenerującą.<br>SPROSTAMY KAŻDEMU WYZWANIU!<br><br>Na potrzeby naszych klientów możemy zorganizować wolno stojące stołówki, wykonujemy szeroki zakres usług cateringowych w najlepszej cenie na rynku, możemy konkurować z każdą ofertą.',
            'www' => 'https://www.facebook.com/gross.catering',
            'priority' => 60,
        ],[
            'slug' => 'studio306',
            'title' => 'Studio 306',
            'description' => 'Firma Studio 306 zajmuje się wysokojakościowym drukiem wielkoformatowym, offsetowym i cyfrowym. Od początku naszej działalności stawiamy na relacje międzyludzkie i chęć pomocy osobom, które są naszymi klientami.<br><br>Dla nas liczy się przede wszystkim człowiek, jego potrzeby, a nie tylko chęć zysku. Uważamy, że dobro powraca. Mamy ogromną prośbę do wszystkich: <strong>wspierajcie Fundację Judo Sport Walki z Rakiem!</strong> Walczmy wszyscy dla dobra tych, którzy tego potrzebują.<br><br>Dajmy siłę dzieciom :-)!, a one dadzą ją nam...',
            'www' => 'http://www.studio306.pl',
            'priority' => 20,
        ],[
            'slug' => 'artur-gorzelak-w-judodze-na-rowerze',
            'title' => 'Artur Gorzelak - W judodze na rowerze',
            'description' => 'Aktywny sportowiec amator, posiadacz niekończących się pokładów energii. Od 20 lat trenuje judo (stopień mistrzowski – 1 DAN). Maratończyk i ultramaratończyk, poszukiwacz przygód i wyzwań na całym świecie.<br><br>Projekt Artura "W judodze na rowerze" łączy ze sobą pasję uprawniania sportu, odwiedzania nieznanych zakątków świata, a także stawienie sobie nowych wyzwań.<br><br>Artur jest także autorem książki o swojej podróży rowerowej przez Japonię i Koreę, w której promuje działanie Fundacji Judo - Sport Walki z Rakiem.<br><strong>10% zysków z książki zostanie przekazane fundacji na cele statutowe.</strong>',
            'www' => 'https://www.facebook.com/wjudodzenarowerze',
            'priority' => 30,
        ],[
            'slug' => 'la-koguta',
            'title' => 'La KOGUTA',
            'description' => 'Creative Catering, a w konsekwencji przetwory La Koguta, są owocem życia pełnego podróży i przygód międzykulturowych Sophie, jej rodzinnego domu i kilkunastu lat doświadczenia w przemyśle cateringowym.<br><br>Profesjonalne podejście do gastronomii zaczęło się od dnia kiedy Sophie wróciła do Wielkiej Brytanii i wzięła czynny udział w transformacji angielskiego rynku cateringowego. Zaczęła prowadzić i współpracować z restauracjami, klubami i pubami.<br><br>Od czasu kiedy Sophie żyje w Warszawie miała przyjemność organizować imprezy oraz catering z wielu okazji , jak np.: kameralne obiady, przełomowe ceremonie na terenach budowy, a także ekskluzywne bankiety, czy uroczystości w muzeach i galeriach.',
            'www' => 'http://www.lakoguta.pl',
            'priority' => 40,
        ],[
            'slug' => 'kancelaria-jpdi',
            'title' => 'Kancelaria JPDI',
            'description' => 'JPDI działa na rynku usług prawniczych od 2006 roku.<br><br>JPDI tworzą wykwalifikowani, doświadczeni specjaliści różnych dziedzin prawa – ludzie pełni pasji, pomysłów i zaangażowania.<br><br>JPDI doradza podmiotom krajowym oraz międzynarodowym, oferując usługi na najwyższym poziomie merytorycznym.<br><br>JPDI uzyskała rekomendacje w rankingu kancelarii Rzeczpospolitej: w 2016 r. w dziedzinie prawa energetycznego, w 2015 w dziedzinie prawa energetycznego, w 2014 r. w dziedzinie prawa energetycznego, nieruchomości i własności intelektualnej oraz prawa autorskiego, w 2013 prawa energetycznego i własności intelektualnej oraz prawa autorskiego, w 2012 prawa energetycznego i nieruchomości.',
            'www' => 'http://jpdi.pl',
            'priority' => 50,
//        ],[
//            'slug' => 'dogs-and-wellness',
//            'title' => 'Dogs & Wellness - Salon Pielęgnacji Psów',
//            'description' => 'W Salonie Dogs & Wellness zajmujemy się profesjonalną pielęgnacją małych zwierząt.<br><br>Zapraszamy wszystkie pieski, kotki oraz gryzonie.<br><br>Świadczymy usługi w zakresie pielęgnacji dobranej do typu szaty pupila jak i na życzenie klienta.<br>Każda usługa strzyżenia pieska łączy się z poprzedzającą je kąpielą oraz pełnym pakietem higienicznym (tj. obcięcie i jeśli istnieje taka potrzeba także szlifowanie pazurków, higiena oczu i uszu oraz opróżnianie gruczołów okołoodbytowych).<br><br>Ze względu na profesjonalne podejście do wykonywanej przez nas pracy NIE STRZYŻEMY PIESKÓW BEZ WCZEŚNIEJSZEJ KĄPIELI. Jest to nieodłączna część pielęgnacji i bez niej usługa nie może być wykonana w pełni dokładnie.',
//            'www' => 'https://www.facebook.com/dogsandwellness',
//            'priority' => 50,
        ],[
            'slug' => 'flo-pol-catering',
            'title' => 'FLO-POL Catering',
            'description' => 'Rodzinna firma gastronomiczna FLO – POL powstała w 1993 roku.<br><br>Naszym priorytetem jest żywienie dzieci i młodzieży zgodnie z normami, przygotowywanie smacznych, zbilansowanych posiłków według oryginalnych przepisów i receptur. Każdy posiłek przygotowujemy ze świeżych produktów dostarczanych przez zaufanych dostawców. W naszej pracy najbardziej cieszy uśmiech każdego najedzonego dziecka.',
            'www' => 'http://flo-pol.pl/',
            'priority' => 10,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sponsors')->truncate();
        foreach($this->data as $row){
            \App\Models\Sponsor::create($row);
        }
    }
}
