<?php

use Illuminate\Database\Seeder;

class BloodTypesTableSeeder extends Seeder
{
    protected $data = [
        ['slug' => '0rh-', 'title' => '0 Rh−'],
        ['slug' => '0rh+', 'title' => '0 Rh+'],
        ['slug' => 'brh-', 'title' => 'B Rh−'],
        ['slug' => 'brh+', 'title' => 'B Rh+'],
        ['slug' => 'arh-', 'title' => 'A Rh−'],
        ['slug' => 'arh+', 'title' => 'A Rh+'],
        ['slug' => 'abrh-', 'title' => 'AB Rh−'],
        ['slug' => 'abrh+', 'title' => 'AB Rh+'],
        ['slug' => 'arhd+', 'title' => 'A RhD+'],
        ['slug' => 'arhd-', 'title' => 'A RhD-'],
        ['slug' => 'abrhd+', 'title' => 'AB RhD+'],
        ['slug' => 'abrhd-', 'title' => 'AB RhD-'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blood_types')->truncate();
        foreach($this->data as $row){
            \App\Models\BloodType::create($row);
        }
    }
}
