<?php

use Illuminate\Database\Seeder;

class DonationTypesTableSeeder extends Seeder
{
    protected $data = [
        ['slug' => 'krew-pelna', 'title' => 'Krew pełna', 'default_amount' => 450],
        ['slug' => 'osocze', 'title' => 'Osocze', 'default_amount' => 600],
        ['slug' => 'plytki-krwi', 'title' => 'Płytki krwi', 'default_amount' => 250],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('donation_types')->truncate();
        foreach($this->data as $row){
            \App\Models\DonationType::create($row);
        }
    }
}
