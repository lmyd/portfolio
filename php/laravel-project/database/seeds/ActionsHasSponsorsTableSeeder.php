<?php

use Illuminate\Database\Seeder;

class ActionsHasSponsorsTableSeeder extends Seeder
{
    protected $data = [
        'judo-legia-cup-2016' => [],
        'judo-lemur-cup-2016' => [
            'gross-catering',
            'la-koguta',
        ],
        'i-liga-wom-funny-judo-2017' => [
            'gross-catering',
            'studio306',
        ],
        'judo-chojrak-cup-2017' => [
            'gross-catering',
            'studio306',
        ],
        'judo-legia-cup-2017' => [
            'gross-catering',
            'studio306',
        ],
        'same-judo-cup-2' => [
            'gross-catering',
            'studio306',
        ],
        'judo-kowala-cup-2017' => [
            'studio306',
        ],
        'judo-lemur-cup-2017' => [
            'gross-catering',
            'studio306',
            'la-koguta',
        ],
        'ikizama-cup-2018' => [
            'gross-catering',
            'studio306',
        ],
        'same-judo-cup-3' => [
            'gross-catering',
            'studio306',
            'artur-gorzelak-w-judodze-na-rowerze'
        ],
        'judo-tuliszkow-2018' => [
            'gross-catering',
            'studio306',
        ],
        'judo-lemur-cup-2018' => [
            'gross-catering',
            'studio306',
        ],
        'pokonujemy-granice-2019' => [
            'gross-catering',
            'studio306',
            'flo-pol-catering',
        ],
        'same-judo-cup-4' => [
            'studio306',
            'flo-pol-catering',
        ],
        'judo-legia-cup-2019' => [
            'studio306',
            'flo-pol-catering',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions_has_sponsors')->truncate();
        foreach($this->data as $actionSlug => $sponsors){
            $action = \App\Models\Action::findBySlugOrFail($actionSlug);
            foreach ($sponsors as $sponsorSlug){
                $sponsor = \App\Models\Sponsor::findBySlugOrFail($sponsorSlug);
                $action->sponsors()->attach($sponsor);
            }
        }
    }
}

