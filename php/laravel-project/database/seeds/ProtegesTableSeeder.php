<?php

use Illuminate\Database\Seeder;

class ProtegesTableSeeder extends Seeder
{
    protected $data = [
        [
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => '0rh+',
            'slug' => 'lenka-wzorek',
            'title' => 'Lenka Wzorek',
            'description' => 'Nazywam się Lenka Wzorek i mam 5 lat! Zachorowałam na białaczkę. Muszę mieć wielokrotnie przetaczaną krew, by wygrać z rakiem. przyda się każda grupa krwi! Potrzebuję pomocy!',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => '0rh+',
            'slug' => 'zosia-galazka',
            'title' => 'Zosia Gałązka',
            'description' => 'Od lipca 2016 walczy z nowotworem złośliwym Chłoniak Burkitta III stopnia. 6 grudnia 2016 zakończyła krótkie 4 cyklowe, ale bardzo agresywne leczenie chemioterapią. Obecnie jest w stanie remisji całkowitej i regeneruje swój organizm . Zosia miała przytoczone 2600 ml masy erytrocytalnej i 950 ml masy płytkowej.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'abrh+',
            'slug' => 'natalia-kopowicz',
            'title' => 'Natalia Kopowicz',
            'description' => 'Nazywam się Natalia Kopowicz i mam 7 lat. mieszkam z rodzicami i braciszkiem w Miączynie Dużym. W minione wakacje zdiagnozowano u mnie nowotwór ganglioblastoma. Przeszłam już kilka chemioterapii, które wyniszczyły mój organizm, dlatego pilnie potrzebuję krwi. Obecnie jestem w trakcie procesu pobierania komórek macierzystych do autoprzeszczepu.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'brh+',
            'slug' => 'szymek-wodkiewicz',
            'title' => 'Szymek Wódkiewicz',
            'description' => 'Nazywam się Szymek Wódkiewicz i mam 2 lata. W wakacje zdiagnozowano u mnie bardzo złośliwy owotwór z przerzutami do kości i szpiku – Neuroblastoma IV stopnia. Jestem bardzo radosnym dzieckiem i dzielnie walczę z chorobą. Niestety, intensywna chemioterapia i radioterapia wyniszczyły mój organizm, dlatego często potrzebuję krwi.',
            'deathday' => '2017-06-13',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'arh-',
            'slug' => 'olaf-piskorczyk',
            'title' => 'Olaf Piskorczyk',
            'description' => 'Od 16 miesięcy miesięcy leczy się onkologicznie w Centrum Zdrowia Dziecka w Warszawie. Przeszedł dwie bardzo poważne operacje wycięcia złośliwego guza mózgu (Medulloblastoma). Obecnie kontynuuje leczenie onkologiczne, które planowo potrwa jeszcze do końca kwietnia.  Toczony w przybliżeniu około 16 - 18 razy. Każde toczenie to 600 ml. Razem dostał około 7 litrów krwi.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'brh+',
            'slug' => 'wojtek-ryszewski',
            'title' => 'Wojtek Ryszewski',
            'description' => 'Od 2 roku życia choruję na cukrzycę typu 1 (insulinozależną). W lutym 2016 r zdiagnozowano u niego złośliwego guza mózgu - Medulloblastoma IV stopnia z licznymi przerzutami do rdzenia kręgowego. Przez rok przeszedł operacje ratującą życie, wiele chemioterapii, 6 tyg radioterapii. W październiku skończył leczenie ale kontrolny rezonans 17 grudnia pokazał coś niepokojącego...  wznowa. Niestety Wojtek musi znowu rozpocząć walkę. Ogólnie Wojtek był toczony 33 razy.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'arh+',
            'slug' => 'igor-zadlak',
            'title' => 'Igor Żądlak',
            'description' => 'Nowotwór złośliwy móżdżku, medulloblastomę typ anaplastyczny. Igor od początku  leczenia czyli października 2016 miał 22 razy przetaczaną krew. Przeszedł operację ratującą życie, wszczepienie zastawki komorowo-otrzewnowej jak i ma założone dojście centralne Broviak. Po zabiegu pozostał mu lewostronny niedowład, który dzięki rehabilitacji jest mało zauważalny. Odbył cztery chemie przed radioterapią, sześciotygodniową radioterapię i obecnie jest na szóstej z ośmiu chemii podtrzymującej. W rezonansie  głowy i kręgosłupa czysto. Przewidywalny czas w którym zakończy chemię to kwiecień. Na dzień dzisiejszy już wiemy , że Igor ma uszkodzony słuch przez leczenie. W lutym obiektywne badanie słuchu i niestety będziemy musieli zakupić aparaty słuchowe tak nam powiedziała pani audiolog.',
        ],[
            'hospital' => 'samodzielny-publiczny-dziecięcy-szpital-kliniczny',
            'blood' => 'abrh+',
            'slug' => 'stefan-kaminski',
            'title' => 'Stefan Kamiński',
            'description' => 'Stefek ma 2 i pół roku. W grudniu 2017 zdiagnozowano u niego ostrą białaczkę limfoblastyczną. Jego leczenie polega na przejściu wieloetapowej chemioterapii. Ze względu na fakt że, białaczka to nowotwór krwi, właśnie krew oraz płytki krwi są niezbędne do przejścia całego leczenia. Jako rodzice tego dzielnego chłopca, zwracamy się z prośbą o oddanie krwi i płytek krwi.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'arhd+',
            'slug' => 'dawid-wisniewski',
            'title' => 'Dawid Wiśniewski',
            'description' => 'Dawid Wiśniewski ma 10 lat. Od wielu lat trenuje karate tradycyjne. Zawsze był radosnym, życzliwym i pełnym energii dzieckiem. W sierpniu wykryto u niego  guza móżdżka i komory IV. Medulloblastoma anaplastyczna IV stopień złośliwości z ostrym wodogłowiem wewnętrznym. Dawid przeszedł ciężką i skomplikowaną operację usunięcia guza. W wrześniu rozpoczął leczenie chemioterapią i radioterapią. Każda dawka chemii to męczące i wyczerpujące powikłania oraz omdlenia. Dawid ma częste przetaczanie krwi oraz płytek. Oddając krew ratujesz mu życie!!!',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'arh+',
            'slug' => 'mikolaj-domagala',
            'title' => 'Mikołaj Domagała',
            'description' => 'Mikołaj Domagała ma 5 lat. Do zeszłego roku był zdrowym i wesołym chłopcem, który właśnie miał iść do przedszkola. W czerwcu 2017 roku po wykonaniu szeregu badań padła diagnoza: <strong>nowotwór złośliwy mózgu "wyściółczak anaplastyczny" III stopnia</strong> Mikołaj przeszedł ciężką operację usunięcia guza, jednak nie udało się go usunąć w całości. Mikołaj przeszedł radioterapię i jest w trakcie leczenia chemioterapia. Rezonans magnetyczny wykazał, że guz niestety się powiększył. Konieczne jest przetaczanie krwi. Oddając krew ratujesz mu życie!!!',
        ],[
            'hospital' => 'oddzial-onkologii-hematologii-uniwersytecki-szpital-dzieciecy',
            'blood' => '0rh+',
            'slug' => 'amelia-stawska',
            'title' => 'Amelia Stawska',
            'description' => 'Amelia Stawska to wyjątkowa wojowniczka. Wygląda jak aniołek i takie ma serduszko, ale drzemie w niej ogromna siła. W wieku zaledwie 3 lat zachorowała na <strong>neuroblastomę IV stopnia (tzw. nerwiaka zarodkowego)</strong>. Niestety dotychczasowe szkolenie nie przyniosło sukcesu, na który Amelka i jej rodzice tak bardzo czekali. Usłyszeli najgorsze z możliwych słów: WZNOWA. Jej organizm jest mocno wyniszczony chemią i radioterapią, a przed Nią ogromna bitwa. Jeśli chcesz jej pomóc, możesz oddać krew, której tak bardzo potrzebują.',
        ],[
            'hospital' => 'instytut-matki-i-dziecka',
            'blood' => '0rh+',
            'slug' => 'alan-mikolajczyk',
            'title' => 'Alan Mikołajczyk',
            'description' => 'Alan Mikołajczyk ma 12 lat i jest niesamowitym chłopakiem. Chciałby być kucharzem! Niestety radosne dzieciństwo zostało zmącone. U Alana zdiagnozowano złośliwy rak kości - <strong>mięsak Ewinga</strong>. Przeszedł trzy operacje usunięcia guza z miednicy z endoprotezoplastyką oraz usunięcie przerzutów w płucach. Za nim także radioterapia na biodro i płuco. W tym czasie, Alan przyjął bardzo dużo chemii. Możemy pomóc Alanowi oddając krew, która pomoże mu w szybszej odbudowie sił i zdrowia.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => 'brh+',
            'slug' => 'kacper-gajewski',
            'title' => 'Kacper Gajewski',
            'description' => 'Cześć, jestem Kacper i mam 12 lat. Na codzień jestem bystrym i rezolutnym chłopcem. Uwielbiam grać w piłkę i marzę o tym żeby w przyszłości zostać bramkarzem. Jednak od stycznia, rak mózgu (<strong>MEDULLOBLASTOMA CLASIK</strong>) próbuje zepsuć moje plany. Mimo to nie zamierzam sie poddawać. Wierzę, że uda mi się wygrać z chorobą i spełnić swoje marzenia. Za wszelką pomoc bardzo dziękuję.',
        ],[
            'hospital' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'blood' => '0rh+',
            'slug' => 'jas-karwowski',
            'title' => 'Jaś Karwowski',
            'description' => 'Cześć, mam na imię Jaś i mam 6 lat. Od ponad 4 lat walczę z chorobą nowotworową jaka jest <strong>RETINOBLASTOMA</strong> czyli złośliwy nowotwór gałki ocznej. Mimo to, iż straciłem już prawe oczko, jestem wciąż radosny. Lubię malować i lepić z plasteliny. W tej chwili choroba zaatakowała po raz trzeci. Muszę przyjmować bardzo mocną chemioterapię, która wyniszcza mój organizm. Jeśli chcesz mi pomóc, możesz oddać krew, której ostatnio bardzo potrzebuję.',
        ],[
            'hospital' => 'szpital-kliniczny-przemienienia-panskiego',
            'blood' => 'abrhd-',
            'slug' => 'alan-hibental',
            'title' => 'Alan Hibental',
            'description' => 'Hej, nazywam się Alan Hibental. Bardzo lubię zabawę z braćmi, układanie klocków i oczywiście oglądanie bajek. Niestety teraz większość czasu spędzam w szpitalu. W grudniu 2018 roku, lekarze zdiagnozowali u mnie nowotwór złośliwy - <strong>CHŁONIAK HODGKINA</strong>. Aby móc wyzdrowieć, muszę przyjmować dużo leków, które pomagają ale także niszczą mój organizm. Jeśli chcesz mi pomóc - możesz oddać krew. Dziękuję za pomoc w walce o życie.',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proteges')->truncate();
        foreach($this->data as $row){

            $hospital = \App\Models\Hospital::findBySlugOrFail($row['hospital']);
            $row['hospital_id'] = $hospital->id;
            unset($row['hospital']);
            $blood = \App\Models\BloodType::findBySlugOrFail($row['blood']);
            $row['blood_type_id'] = $blood->id;
            unset($row['blood']);

            \App\Models\Protege::create($row);
        }
    }
}
