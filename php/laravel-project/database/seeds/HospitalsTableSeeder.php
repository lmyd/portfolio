<?php

use Illuminate\Database\Seeder;

class HospitalsTableSeeder extends Seeder
{
    protected $data = [
        [
            'slug' => 'klinika-onkologii-instytut-pomnik-centrum-zdrowia-dziecka',
            'title' => 'Klinika onkologii, Instytut „Pomnik – Centrum Zdrowia Dziecka”',
            'address' => 'Al. Dzieci Polskich 20',
            'post_code' => '04-730',
            'city' => 'Warszawa',
            'www' => 'http://www.czd.pl/index.php?option=com_content&view=article&id=227&Itemid=233'
        ],
        [
            'slug' => 'samodzielny-publiczny-dziecięcy-szpital-kliniczny',
            'title' => 'Oddział kliniczny Onkologii i Pediatrii, Samodzielny Publiczny Dziecięcy Szpital Kliniczny w Warszawie',
            'address' => 'ul. Żwirki i Wigury 63A',
            'post_code' => '02-091',
            'city' => 'Warszawa',
            'www' => 'http://spdsk.edu.pl'
        ],
        [
            'slug' => 'oddzial-onkologii-hematologii-uniwersytecki-szpital-dzieciecy',
            'title' => 'Oddział Onkologii i Hematologii, Uniwersytecki Szpital Dziecięcy w Krakowie',
            'address' => 'ul. Wielicka 265',
            'post_code' => '30-663',
            'city' => 'Kraków',
            'www' => 'https://www.szpitalzdrowia.pl'
        ],
        [
            'slug' => 'instytut-matki-i-dziecka',
            'title' => 'Instytut Matki i Dziecka w Warszawie',
            'address' => 'ul. Kasprzaka 17a',
            'post_code' => '01-211',
            'city' => 'Warszawie',
            'www' => 'http://www.imid.med.pl'
        ],
        [
            'slug' => 'szpital-kliniczny-przemienienia-panskiego',
            'title' => 'Szpital Kliniczny Przemienienia Pańskiego, Uniwersytetu Medycznego im. Karola Marcinkowskiego w Poznaniu',
            'address' => 'ul. Szamarzewskiego 82/84',
            'post_code' => '61-569',
            'city' => 'Poznań',
            'www' => 'http://www.skpp.edu.pl'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hospitals')->truncate();
        foreach($this->data as $row){
            \App\Models\Hospital::create($row);
        }
    }
}
