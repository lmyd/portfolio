<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::get('/{locale?}', 'Main@index')->name('main')->where(['locale'=>'pl|en'])->middleware('regulations');

Route::get('/regulations', function(){
    return view('regulations.index');
})->name('regulations');

Route::get('/actions', 'Actions@index')->middleware('regulations')->name('actions');
Route::get('/actions/{slug}', 'Actions@show')->middleware('regulations')->name('action');
Route::post('/actions/{slug}/join', 'Actions@join')->middleware('auth', 'regulations')->name('join2action');

Route::get('/proteges', 'Proteges@index')->middleware('regulations')->name('proteges');
Route::get('/proteges/{slug}', 'Proteges@show')->middleware('regulations')->name('protege');

Route::get('/donors', 'Donors@index')->middleware('regulations')->name('donors');
Route::get('/supporting', 'Supporting@index')->middleware('regulations')->name('supporting');
Route::get('/sponsors', 'Sponsors@index')->middleware('regulations')->name('sponsors');

Route::get('/dashboard', 'Dashboard@index')->middleware('auth', 'regulations')->name('dashboard');
Route::post('/make-donation', 'Dashboard@makeDonation')->middleware('auth', 'regulations')->name('makeDonation');


Route::get('/profile/edit', 'Profile@edit')->middleware('auth', 'regulations')->name('editProfile');
Route::post('/profile/update', 'Profile@update')->middleware('auth', 'regulations')->name('updateProfile');
Route::post('/profile/accept', 'Profile@accept')->middleware('auth')->name('acceptRegulations');



Route::get('/payu/{donationId}', 'Main@payuRecive')->name('payuRecive');
Route::post('/payu', 'Main@payuSend')->name('payuSend');

