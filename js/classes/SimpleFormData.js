/**
 * Klasa wspierajaca komponent simple-form
 */
export default class SimpleFormData {

    /**
     * Konstruktor
     *
     * @param data Json z serwera
     */
    constructor(data) {
        this.el = data.elements;
        this.msg = data.messages;
        this.btn = data.actions || {};
    }

    /**
     * Ustawia wartosc lub null na elemencie
     *
     * @param {string} name
     * @param {mixed}  value
     *
     * @return {SimpleFormData}
     */
    setValue(name, value){
        this.el[name].value = value || null;
        return this;
    }

    /**
     * Zwraca wartosc elementu
     *
     * @param {string} name
     *
     * @return {*}
     */
    getValue(name){
        return this.el[name].value;
    }

    /**
     * Włącza/Wyłącza podświetlanie pola
     *
     * @param {string}  name
     * @param {boolean} value
     *
     * @return {SimpleFormData}
     */
    setError(name, value = true) {
        this.el[name].error = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element posiada błąd
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isError(name){
        return this.el[name].error;
    }

    /**
     * Czysci wszystkie errory przypisane do pol
     *
     * @return {SimpleFormData}
     */
    clearErrors() {
        for (let key in this.el) {
            this.el[key].error = false;
        }
        return this;
    }

    /**
     * Dodaje komunikat bledu
     *
     * @param {string} message
     *
     * @return {SimpleFormData}
     */
    addMessage(message) {
        this.msg.push(message);
        return this;
    }

    /**
     * Czysci tablice komunikatow bledu
     *
     * @return {SimpleFormData}
     */
    clearMessages() {
        this.msg = [];
        return this;
    }

    /**
     * Włącza/Wyłącza blokowanie pola
     *
     * @param {string}  name
     * @param {boolean} value
     *
     * @return {SimpleFormData}
     */
    setReadonly(name, value = true) {
        this.el[name].readonly = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element jest tylko do odczytu
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isReadonly(name) {
        return this.el[name].readonly;
    }

    /**
     * Włącza/Wyłącza wymagalność na polu
     *
     * @param {string} name
     * @param {boolean} value
     *
     * @return {SimpleFormData}
     */
    setRequired(name, value = true) {
        this.el[name].required = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element jest wymagany
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isRequired(name) {
        return this.el[name].required;
    }

    /**
     * Dodaje element button do formularza
     *
     * @param {string}  name
     * @param {string}  label
     * @param {string}  classCss
     * @param {boolean} disabled
     * @param {boolean} hidden
     * @param {boolean} withValidation
     *
     * @return {SimpleFormData}
     */
    addBtn(name, label, classCss, disabled, hidden, withValidation){
        this.btn[name] = {
            label: label,
            type: 'button',
            classCss: classCss,
            disabled: disabled,
            hidden: hidden,
            withValidation: withValidation
        };
        return this;
    }

    /**
     * Dodaje element submit do formularza
     *
     * @param {string}  name
     * @param {string}  label
     * @param {string}  classCss
     * @param {boolean} disabled
     * @param {boolean} hidden
     *
     * @return {SimpleFormData}
     */
    addSubmitBtn(name, label, classCss, disabled, hidden){
        this.btn[name] = {
            label: label || '',
            type: 'submit',
            classCss: classCss || '',
            disabled: disabled || false,
            hidden: hidden || false
        };
        return this;
    }

    /**
     * Odblokowuje przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormData}
     */
    enableBtn(name) {
        this.btn[name].disabled = false;
        return this;
    }

    /**
     * Blokuje przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormData}
     */
    disableBtn(name) {
        this.btn[name].disabled = true;
        return this;
    }

    /**
     * Ukrywa przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormData}
     */
    hideBtn(name) {
        this.btn[name].hidden = true;
        return this;
    }

    /**
     * Odsłania przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormData}
     */
    showBtn(name) {
        this.btn[name].hidden = false;
        return this;
    }
}
