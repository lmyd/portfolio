/**
 * Klasa wspierajaca komponent form-amount
 */
export default class FormatAmount {

    /**
     * Zwraca niesformatowany ciąg liczbowy
     *
     * @param <String> value
     *
     * @return <String>
     */
    static toNumber(value) {
        return value.replace(new RegExp(/[^\d,\-]*/g), '');
    }

    /**
     * Zwraca sformatowany ciąg liczbowy bez zaokraglania
     *
     * @param <String> value
     *
     * @return <String>
     */
    static toString(value, precision = 2, decimal = ",", thousands = " ") {
        try {
            if (value === '') {
                return ''
            }
            value = value.replace(',', '.');
            let absValue = Math.abs(Number(value));

            const negativeSign = value < 0 ? "-" : "";
            let i = parseInt(absValue).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;
            precision = Math.abs(precision);
            precision = isNaN(precision) ? 2 : precision;
            let decimals = "";
            if (precision) {
                decimals = String(absValue);
                let start = decimals.indexOf(".");
                if (start >= 0) {
                    start++;
                    decimals = decimals.slice(start, start + precision);
                } else {
                    decimals = "0";
                }
                while (decimals.length < precision) {
                    decimals += "0"
                }
            }

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (precision ? decimal + decimals : "");
        } catch (e) {
            console.log(e)
        }
    }

}
