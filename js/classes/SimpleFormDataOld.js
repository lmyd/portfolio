/**
 * Klasa wspierajaca komponent simple-form
 * @deprecated
 */
export default class SimpleFormDataOld {

    /**
     * Konstruktor
     *
     * @param data Json z serwera
     */
    constructor(data) {
        this.$el = data.elements;
        this.$msg = data.messages;
        this.$btn = data.actions;
    }

    /**
     * Zwraca obiekt elementu
     *
     * @param {string} name
     *
     * @return {*}
     */
    el(name) {
        return this.$el[name];
    }

    /**
     * Ustawia wartosc lub null na elemencie
     *
     * @param {string} name
     * @param {mixed}  value
     *
     * @return {SimpleFormDataOld}
     */
    setValue(name, value){
        this.$el[name].value = value || null;
        return this;
    }

    /**
     * Zwraca wartosc elementu
     *
     * @param {string} name
     *
     * @return {*}
     */
    getValue(name){
        return this.$el[name].value;
    }

    /**
     * Włącza/Wyłącza podświetlanie pola
     *
     * @param {string}  name
     * @param {boolean} value
     *
     * @return {SimpleFormDataOld}
     */
    setError(name, value = true) {
        this.$el[name].error = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element posiada błąð
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isError(name){
        return this.$el[name].error;
    }

    /**
     * Czysci wszystkie errory przypisane do pol
     *
     * @return {SimpleFormDataOld}
     */
    clearErrors() {
        for (let key in this.$el) {
            this.$el[key].error = false;
        }
        return this;
    }

    /**
     * Dodaje komunikat bledu
     *
     * @param {string} message
     *
     * @return {SimpleFormDataOld}
     */
    addMessage(message) {
        this.$msg.push(message);
        return this;
    }

    /**
     * Czysci tablice komunikatow bledu
     *
     * @return {SimpleFormDataOld}
     */
    clearMessages() {
        this.$msg = [];
        return this;
    }

    /**
     * Włącza/Wyłącza blokowanie pola
     *
     * @param {string}  name
     * @param {boolean} value
     *
     * @return {SimpleFormDataOld}
     */
    setReadonly(name, value = true) {
        this.$el[name].readonly = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element jest tylko do odczytu
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isReadonly(name) {
        return this.$el[name].readonly;
    }

    /**
     * Włącza/Wyłącza wymagalność na polu
     *
     * @param {string} name
     * @param {boolean} value
     *
     * @return {SimpleFormDataOld}
     */
    setRequired(name, value = true) {
        this.$el[name].required = _.isBoolean(value) ? value : true;
        return this;
    }

    /**
     * Czy element jest wymagany
     *
     * @param {string} name
     *
     * @return {boolean}
     */
    isRequired(name) {
        return this.$el[name].required;
    }

    /**
     * Odblokowuje przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormDataOld}
     */
    enable(name) {
        this.$btn[name].disabled = false;
        return this;
    }

    /**
     * Blokuje przycisk
     *
     * @param {string} name
     *
     * @return {SimpleFormDataOld}
     */
    disable(name) {
        this.$btn[name].disabled = true;
        return this;
    }
}
