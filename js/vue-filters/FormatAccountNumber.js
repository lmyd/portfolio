/**
 * FormatAccountNumber - formatowanie stringa na postac NRB
 *
 * @param {string} value Niesformatowany string
 *
 * @return {string}
 */
export default function (value) {
    if (!value) {
        return '';
    }

    let formattedAccountNumber = '';
    let parts = {0: 2, 2: 4, 6: 4, 10: 4, 14: 4, 18: 4, 22: 4};
    for (let i in parts) {
        formattedAccountNumber += value.substr(i, parts[i]) + ' ';
    }

    return formattedAccountNumber.trim();
}
