const app = require('express')();
const http = require('http').createServer(app)
const bodyParser = require('body-parser');
const cors = require('cors');
const mongodb = require('mongodb');
const io = require('socket.io')(http);

// Middleware 
app.use(bodyParser.json());
app.use(cors());

// Routes
const posts = require('./routes/posts')
app.use('/posts', posts);

//Sockets
io.on('connection', (socket) => {
    console.log('start listning mongodb changeStream');
    (async () => {
        const client  = await mongodb.MongoClient.connect('mongodb://localhost:27017/mongo_node_vue', { 
            useNewUrlParser: true, 
            readPreference: mongodb.ReadPreference.PRIMARY
        });
        io.emit('posts', await client.db('mongo_node_vue').collection('posts').find({}).sort({createdAt: -1}).toArray());

        const changeStream = client.db('mongo_node_vue').collection('posts').watch([], {fullDocument: 'updateLookup'});
        changeStream.on('change', (event) => {
            console.log(event);
            (async () => {
                io.emit('posts', await client.db('mongo_node_vue').collection('posts').find({}).sort({createdAt: -1}).toArray());
            })();
        });
    })();
});


const port = process.env.port || 5000;

http.listen(port, () => console.log(`Server start at port ${port}`));
